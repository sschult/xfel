using LinearAlgebra, Statistics, Rotations, ProgressMeter, Random, JLD2, StaticArrays, Dates, FileIO, DelimitedFiles, ArgParse, Glob, StatsBase, Printf, Distributions, Interpolations

import NPhotons as nph

const s = ArgParseSettings()
@add_arg_table! s begin
    "--nobject", "-o"
        arg_type = Float64
        default = 1.0
    "--nbackground", "-b"
        default = 0.0
        arg_type = Float64
    "--nuniform", "-u"
        default = 0.0
        arg_type = Float64
    "--nphotons", "-I"
        default = 15.0
        arg_type = Float64
    "--nimages", "-N"
        default = 1000000
        arg_type = Int
    "--nfiles", "-f"
        default = 1
        arg_type = Int
    "--wavelength", "-w"
        default = 2.5
        arg_type = Float64
    "--qmax", "-q"
        default = Inf
        arg_type = Float64
    "--detectorscale", "-s"
        default = 1.0
        arg_type = Float64
    "--polarization", "-p"
        action = :store_true
    "--gamma", "-g"
        action = :store_true
    "--detector", "-d"
        action = :store_true
    "--saveseparate"
        action = :store_true
    "--pdb"
        default = "1EJG"
        arg_type = String
    "path"
        required = true
        arg_type = String
end

const A = (; parse_args(s, as_symbols = true)...)

mkpath(A.path)

function main()
    @load "pixelpositions.jld2" pixelpositions;
    pixelpositions ./= maximum(getindex.(pixelpositions, 2));
    pixelpositions .-= Ref(mean(pixelpositions));
    pixelpositions .*= A.detectorscale

    xs = range(-A.detectorscale, A.detectorscale, length = 201)
    grid = vcat.(nph.grid(xs, dims = 2), Ref(SA[0.0]))
    ys = zeros(size(grid)...)
    inds = nph.roundImages(pixelpositions[:], [grid[:]])[1]
    for i in 1:length(ys)
        ys[i] = norm(pixelpositions[inds[i]] - grid[i], Inf) < 0.003
    end
    itpd = constant_interpolation((xs, xs), ys, extrapolation_bc = 0);

    pd(k) = A.detector ? itpd((nph.e2d(k; λ = A.wavelength)[1:2])...) : 1.0;
    fp(k) = A.polarization ? (1 - k[2]^2 * A.wavelength / 2pi) : 1.0;

    qmax = A.detector ? ceil(norm(nph.d2e(argmax(norm, pixelpositions))), digits = 2) : min(A.qmax, 4pi / A.wavelength)

    o = nph.normalizeScattering(nph.loadPDB(A.pdb), qmax = qmax, nphotons = A.nobject)
    n = nph.normalizeScattering(nph.AtomVolume(1, 2.0, 1), qmax = qmax, nphotons = A.nbackground)
    u = nph.normalizeScattering(nph.AtomVolume(1, 1/100, 1), qmax = qmax, nphotons = A.nuniform)

    oitp = nph.itp(o, qmax)

    for it in 1:A.nfiles
        I0s = A.gamma ? rand(Gamma(4, 1/4), A.nimages) : fill(1.0, A.nimages)
        oimages = nph.generateImagesThreaded(A.nimages, A.nphotons, o, oitp, maxr = qmax, pd = k -> pd(k) * fp(k), I0s = I0s)
        nimages = nph.generateImagesThreaded(A.nimages, A.nphotons, n, maxr = qmax, pd = k -> pd(k) * fp(k), I0s = I0s)
        uimages = nph.generateImagesThreaded(A.nimages, A.nphotons, u, maxr = qmax, pd = k -> pd(k), I0s = I0s)
        
        T = Vector{SVector{3, Float32}}
        oimages = T.(oimages); nimages = T.(nimages); uimages = T.(uimages)
        
        images = shuffle!.(vcat.(oimages, nimages, uimages))
        nph.saveImages(
            joinpath(A.path, "merged$(A.pdb)_$(it).jld2"), images, 
            metadata = (; I0s, nphotons=A.nphotons, o, n, u, qmax, itpd, args = A)
        )

        if A.saveseparate
            oimages = nph.serializeImages(oimages)
            nimages = nph.serializeImages(nimages)
            uimages = nph.serializeImages(uimages)
            @save joinpath(A.path, "images$(A.pdb)_$(it).jld2") oimages nimages uimages I0s nphotons=A.nphotons o n u qmax itpd args=A
        end
    end
end

main()
