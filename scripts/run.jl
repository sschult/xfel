using Distributed, ClusterManagers, LinearAlgebra, Statistics, Rotations, ProgressMeter, Random, JLD2, StaticArrays, Dates, FileIO, DelimitedFiles, ArgParse, Glob, StatsBase, Printf, Distributions, Interpolations

# using Telegram, Telegram.API
# tg = TelegramClient("1474468146:AAF-1cLZGsQvSlZ9igMcBIOdW5f7SjetLKA", chat_id = "79637328");

const starttime = now()

function printlog(x...)
    println(rpad(string(now()), 23, " "), "  ", join(x, " ")...)
    flush(stdout)
end

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table! s begin
        "--path", "-p"
            default = ""
        "--glob", "-g"
            default = "*"
        "--distributed", "-d"
            action = :store_true
        "--dobenchmark", "-b"
            action = :store_true
        "--continuefinished", "-c"
            action = :store_true
        "--saveinterval", "-s"
            arg_type = Int
            default = 60
        "--loginterval", "-l"
            arg_type = Int
            default = 60
    end

    return parse_args(s)
end

const cmdargs = parse_commandline()
const path = cmdargs["path"]
const outpath = joinpath(path, "out")
const distributed = cmdargs["distributed"]
const dobenchmark = cmdargs["dobenchmark"]
const continuefinished = cmdargs["continuefinished"]
const saveinterval = cmdargs["saveinterval"]
const loginterval = cmdargs["loginterval"]

mkpath(outpath)

files = []
for f in glob(joinpath(path, cmdargs["glob"]))
    if occursin(".jld2", f)
        outfile = joinpath(outpath, splitdir(f)[2])
        if continuefinished || !(isfile(outfile) && load(outfile, "finished"))
            push!(files, f)
        end
    end
end

const infiles = [f for f in files]
const outfiles = [joinpath(outpath, splitdir(f)[2]) for f in files]

printlog("Found $(length(infiles)) unfinished input $(length(infiles) == 1 ? "file" : "files")")
length(infiles) == 0 && exit()


using CUDA

if distributed
    if haskey(ENV, "SLURM_NTASKS")
        addprocs_slurm(parse(Int, ENV["SLURM_NTASKS"]) - nprocs() + 1, gpus_per_task = 1, exeflags = "--project=$(Base.active_project())", job_file_loc = "joboutput", retry_delays = ExponentialBackOff(n=10, first_delay=1, max_delay=10, factor=2));
    else
        addprocs(length(devices()), exeflags = "--project=$(Base.active_project())")
    end
end

@everywhere begin
    using CUDA, LinearAlgebra
    import NPhotons as nph
end

if !haskey(ENV, "SLURM_NTASKS")
    asyncmap(workers()) do pid
        remotecall_wait(pid) do
            device!(myid() % length(devices()))
        end
    end
end

printlog("Main process on $(gethostname())")
printlog.(fetch.([@spawnat w "$(gethostname()): $(name(device())) $(device().handle)" for w in workers()]))
printlog.(fetch.([@spawnat w Base.active_project() for w in workers()]))

const tperworker = zeros(nworkers())

@everywhere function setlogp(args...; kwargs...)
    global logp = nph.logpClosure(args...; kwargs...)
    return true
end

const benchmarks = Dict(
    "NVIDIA GeForce RTX 3090" => 3.9, 
    "NVIDIA GeForce GTX 1070" => 0.4, 
    "NVIDIA GeForce RTX 4070 Ti" => 4.3, 
    "NVIDIA GeForce RTX 4090" => 6.7, 
    "NVIDIA GeForce GTX 1080 Ti" => 0.6, 
    "NVIDIA GeForce GTX 1080" => 0.5, 
    "NVIDIA GeForce RTX 2080 Ti" => 1.4, 
    "NVIDIA GeForce RTX 2080" => 0.85, 
    "NVIDIA RTX A4000" => 1.8, 
    "NVIDIA GeForce RTX 4070" => 3.5
)

function assignJobs(N, weights)
    assignments = [Int[] for b in weights]
    for i in 1:N
        a = argmin(length.(assignments) ./ weights)
        push!(assignments[a], i)
    end
    assignments
end

function setup(images, args...; kwargs...)
    if distributed
        images = if images isa Vector{<:SVector{3, <:Real}}
            nph.deserializeImages(images)
        else
            images
        end

        hostnames = fetch.([@spawnat w gethostname() for w in workers()])
        devicenames = fetch.([@spawnat w name(device()) for w in workers()])

        weights = if dobenchmark
            printlog("Benchmarking")
            testf = nph.randomAtomVolume(20, 1.0, 10)
            selected = rand(images, length(images) ÷ nworkers())
            @sync for worker in workers()
                @spawnat worker setlogp(selected, args...; kwargs...)
            end

            fetch.([@async @fetchfrom worker (@elapsed logp(testf)) for worker in workers()])
            times = mean(1:20) do _
                fetch.([@async @fetchfrom worker (@elapsed logp(testf)) for worker in workers()])
            end
            1 ./ times
        else
            get.(Ref(benchmarks), devicenames, 1.0)
        end
        
        printlog("Assigning images to workers")
        
        assignments = assignJobs(length(images), weights)
        for (hn, dn, w, l) in zip(hostnames, devicenames, weights, length.(assignments))
            printlog("$hn $dn: $l images, weight $(round(w, sigdigits = 2))")
        end

        @sync for worker in workers()
            selected = images[assignments[worker-1]]
            @spawnat worker setlogp(selected, args...; kwargs...)
        end

        function logpDist(f; kwargs...)
            futures = [@async @fetchfrom worker (t = time(); p = logp(f; kwargs...); (p, time() - t)) for worker in workers()]
            vals = fetch.(futures)

            tperworker .+= last.(vals)

            sum(first, vals)
        end
    else
        nph.logpClosure(images, args...; kwargs...)
    end
end

function gc()
    @sync for worker in workers()
        @spawnat worker (GC.gc(); CUDA.reclaim())
    end
end



function loadinputs(infile, outfile)
    @load infile state args kwargs
    object = jldopen(f -> get(f, "object", nothing), infile)
    noise = jldopen(f -> get(f, "noise", nothing), infile)
    uniform = jldopen(f -> get(f, "uniform", nothing), infile)

    if isfile(outfile)
        printlog("Resuming previous run from $(outfile)")
        @load outfile state
    end

    logp = setup(args...; kwargs...)

    logpobject = if !isnothing(object)
        if isnothing(noise)
            logp(object)
        else
            logp(nph.MixtureVolume(object, noise); b = uniform)
        end
    else
        0.0
    end

    function efunc(f)
        if isnothing(noise)
            logpobject - logp(f)
        else
            logpobject - logp(nph.MixtureVolume(f, noise), b = uniform)
        end
    end

    targetTemp::Int = jldopen(f -> get(f, "targetTemp", 1.0), infile)
    remaining::Int = nph.remainingSteps(state, targetTemp = targetTemp)
    totalsteps::Int = jldopen(f -> get(f, "totalsteps", state.nsteps + remaining), infile)
    if continuefinished totalsteps += 10000000 end
    nsteps::Int = totalsteps - state.nsteps

    state, efunc, targetTemp, totalsteps, nsteps
end

function process!(state, efunc, targetTemp, totalsteps, nsteps, outfile)
    tperworker .= 0

    tsave = now()
    tlog = now()
    nlog = state.nsteps

    function output(state)
        if now() - tlog > Second(loginterval)
            steps = (state.nsteps - nlog)
            trem = Millisecond(round(Int, (totalsteps - state.nsteps) / steps * (now() - tlog).value))
            trem = Dates.CompoundPeriod(Dates.canonicalize(trem).periods[1:min(end,2)])
            if !isempty(trem.periods) && trem.periods[end] isa Millisecond
                trem = Dates.CompoundPeriod(trem.periods[1:max(1, end - 1)])
            end
            tperstep = round(Int, (now() - tlog).value / steps)
            printlog("$(state.nsteps) / $(totalsteps)  E = $(@sprintf("%.1e", state.energies[end]))  $(tperstep)ms / step  ETA: $(trem)")

            if distributed 
                printlog(join(string.(round.(Int, 1000 .* tperworker ./ steps)), ", ") * " [ms]")
                tperworker .= 0
                GC.gc(); gc()
            end

            tlog = now()
            nlog = state.nsteps
        end
        
        if now() - tsave > Minute(saveinterval)
            t = @elapsed jldsave(outfile; state = state, finished = false)
            printlog("Saved to $(outfile) in $(round(t, sigdigits = 3)) seconds.")
            tsave = now()
        end

        if now() - starttime > Minute(24 * 60 - 15)
            return false
        end
        return true
    end

    nph.anneal!(state, efunc, output = output, minTemp = targetTemp, nsteps = nsteps, showprogress = false)
end


function main()
    for (infile, outfile) in zip(infiles, outfiles)
        printlog("Loading inputs from $(infile)")
        
        state, efunc, targetTemp, totalsteps, nsteps = loadinputs(infile, outfile)

        printlog("$(nsteps) steps remaining")
        
        finished = process!(state, efunc, targetTemp, totalsteps, nsteps, outfile)

        jldsave(outfile, state = state, finished = finished)

        if finished 
            printlog("Finished processing $(infile)")
        else
            printlog("Pausing while processing $(infile)")
        end

        if now() - starttime > Minute(24 * 60 - 15)
            break
        end
    end
end

main()
