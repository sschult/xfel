using LinearAlgebra, JLD2, Dates, FileIO, ArgParse, Glob, StatsBase, Printf, Accessors, BasicInterpolators

const endtime = if haskey(ENV, "SLURM_JOB_END_TIME")
    unix2datetime(parse(Int, ENV["SLURM_JOB_END_TIME"]))
else
    now(UTC) + Day(1)
end

function printlog(x...)
    println(rpad(string(now()), 23, " "), "  ", join(x, " ")...)
    flush(stdout)
end

const s = ArgParseSettings()
@add_arg_table! s begin
    "--path", "-p"
        default = ""
    "--glob", "-g"
        default = "*"
    "--saveinterval", "-s"
        arg_type = Int
        default = 60^2
    "--loginterval", "-l"
        arg_type = Int
        default = 60
end
const cmdargs = parse_args(s)

const path = cmdargs["path"]
const outpath = joinpath(path, "out")
const saveinterval = cmdargs["saveinterval"]
const loginterval = cmdargs["loginterval"]

mkpath(outpath)

const infiles = String[]
const outfiles = String[]
for f in glob(joinpath(path, cmdargs["glob"]))
    if occursin(".jld2", f)
        outfile = splitext(joinpath(outpath, splitdir(f)[2]))[1] * ".arrow"
        push!(infiles, f)
        push!(outfiles, outfile)
    end
end

printlog("Found $(length(infiles)) input $(length(infiles) == 1 ? "file" : "files")")
length(infiles) == 0 && exit()

using CUDA
using NPhotons; import NPhotons as nph

devnames = [name(d) for d in devices()]
devstrings = ["$(sum(devnames .== n)) × $n" for n in unique(devnames)]
printlog("Running on $(gethostname()) with $(join(devstrings, ", "))")

function setup(forwardmodel, conf)
    confr = @set conf.batchsize = 0
    logpr = MultiComputeAllocation(forwardmodel, confr, storeall = false);
    logp = MultiComputeAllocation(forwardmodel, conf, storeall = true);
    grad = MultiGradientAllocation(logp);
    logpr, grad
end

function loadinputs(infile, outfile)
    @load infile optimizer forwardmodel conf
    reference = jldopen(f -> get(f, "reference", nothing), infile)

    logpr, grad = setup(forwardmodel, conf)
    chain = if isfile(outfile)
        printlog("Resuming previous run from $(outfile)")
        [nph.loadChain(outfile)[end]]
    else
        [nph.init(optimizer)]
    end

    logpr, grad, optimizer, chain, reference
end

function process!(logpr, grad, optimizer, chain, reference, outfile)
    tsave = now()
    tlog = now()
    t0 = chain[end].t

    nph.optimize!(optimizer, chain, grad, logpr, reference; showprogress = false) do chain
        c = chain[end[]]

        if now() - tlog > Second(loginterval) && c.t > t0
            trem = Millisecond(round(Int, (now() - tlog).value / (c.t - t0) * (1 - c.t)))
            trem = Dates.CompoundPeriod(Dates.canonicalize(trem).periods[1:min(end,2)])
            if !isempty(trem.periods) && trem.periods[end] isa Millisecond
                trem = Dates.CompoundPeriod(trem.periods[1:max(1, end - 1)])
            end
            tperstep = round(Int, 1000 * (c.time - chain[max(2, end - 100)].time) / min(100, length(chain)))
            printlog("$(c.nsteps) / $(optimizer.nsteps)  $(tperstep)ms / step  t = $(rpad(round(c.t, digits = 5), 7, "0"))  ETA: $(trem)")

            t0 = c.t
            tlog = now()
        end
        
        if now() - tsave > Second(saveinterval)
            t = @elapsed nph.saveChain(outfile, chain)
            printlog("Saved to $(outfile) in $(round(t, sigdigits = 3)) seconds.")
            tsave = now()
        end

        endtime - now(UTC) > Minute(5)
    end

    chain[end].t == 1
end


function main()
    for (infile, outfile) in zip(infiles, outfiles)
        printlog("Loading inputs from $(infile)")
        logpr, grad, optimizer, chain, reference = loadinputs(infile, outfile)
        printlog("Processing $(infile)")
        finished = process!(logpr, grad, optimizer, chain, reference, outfile)
        nph.saveChain(outfile, chain)
        printlog("$(finished ? "Finished" : "Pausing while")  processing $(infile)")
        endtime - now(UTC) < Minute(5) && break
    end
end

main()
