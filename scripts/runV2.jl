using Distributed, SlurmClusterManager, LinearAlgebra, JLD2, Dates, FileIO, ArgParse, Glob, StatsBase, Printf, Accessors

const starttime = now()
const endtime = if haskey(ENV, "SLURM_JOB_END_TIME")
    unix2datetime(parse(Int, ENV["SLURM_JOB_END_TIME"]))
else
    now(UTC) + Day(1)
end

function printlog(x...)
    println(rpad(string(now()), 23, " "), "  ", join(x, " ")...)
    flush(stdout)
end

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table! s begin
        "--path", "-p"
            default = ""
        "--glob", "-g"
            default = "*"
        "--distributed", "-d"
            action = :store_true
        "--dobenchmark", "-b"
            action = :store_true
        "--saveinterval", "-s"
            arg_type = Int
            default = 60
        "--loginterval", "-l"
            arg_type = Int
            default = 60
    end

    return parse_args(s)
end

const cmdargs = parse_commandline()
const path = cmdargs["path"]
const outpath = joinpath(path, "out")
const distributed = cmdargs["distributed"]
const dobenchmark = cmdargs["dobenchmark"]
const saveinterval = cmdargs["saveinterval"]
const loginterval = cmdargs["loginterval"]

mkpath(outpath)

const infiles = String[]
const outfiles = String[]
for f in glob(joinpath(path, cmdargs["glob"]))
    if occursin(".jld2", f)
        outfile = splitext(joinpath(outpath, splitdir(f)[2]))[1] * ".arrow"
        push!(infiles, f)
        push!(outfiles, outfile)
    end
end

printlog("Found $(length(infiles)) input $(length(infiles) == 1 ? "file" : "files")")
length(infiles) == 0 && exit()


using CUDA

if distributed
    exeflags = ["--project=$(Base.active_project())", "--threads=1"]
    if haskey(ENV, "SLURM_NTASKS")
        addprocs(SlurmManager(launch_timeout = 600), exeflags = exeflags)
    else
        addprocs(length(devices()), exeflags = exeflags)
    end
end

@everywhere begin
    using CUDA, LinearAlgebra
    import NPhotons as nph
end

if !haskey(ENV, "SLURM_NTASKS")
    asyncmap(workers()) do pid
        remotecall_wait(pid) do
            device!(myid() % length(devices()))
        end
    end
end

printlog("Main process on $(gethostname())")
printlog.(fetch.([@spawnat w "$(gethostname()): $(name(device())) $(device().handle)" for w in workers()]))
# printlog.(fetch.([@spawnat w Base.active_project() for w in workers()]))

const tperworker = zeros(nworkers())

@everywhere function work(logp, f)
    logp(f)
end

@everywhere function dowork(logp, fs, results)
    while true
        f = take!(fs)
        t = CUDA.@elapsed result = work(logp, f)
        put!(results, (result, t))
    end
end

@everywhere function startwork(alloc, fs, results)
    logp = fetch(alloc)
    dowork(logp, fs, results)
end

@everywhere function benchmark(alloc, f)
    logp = fetch(alloc)
    work(logp, f)
    1/100 * @elapsed for _ in 1:100
        work(logp, f)
    end
end


const benchmarks = Dict(
    "NVIDIA GeForce RTX 3090" => 3.9, 
    "NVIDIA GeForce GTX 1070" => 0.4, 
    "NVIDIA GeForce RTX 4070 Ti" => 4.3, 
    "NVIDIA GeForce RTX 4090" => 6.7, 
    "NVIDIA GeForce GTX 1080 Ti" => 0.6, 
    "NVIDIA GeForce GTX 1080" => 0.5, 
    "NVIDIA GeForce RTX 2080 Ti" => 1.4, 
    "NVIDIA GeForce RTX 2080" => 0.85, 
    "NVIDIA RTX A4000" => 1.8, 
    "NVIDIA GeForce RTX 4070" => 3.5
)

function setup(forwardmodel, computeconfiguration, sampler, precision = Float32)
    if distributed
        hostnames = fetch.([@spawnat w gethostname() for w in workers()])
        devicenames = fetch.([@spawnat w name(device()) for w in workers()])

        fchannels = [RemoteChannel(()->Channel{typeof(sampler.initialconf)}(1), worker) for worker in workers()]
        rchannels = [RemoteChannel(()->Channel{Tuple{Float64, Float64}}(1), worker) for worker in workers()]

        weights = get.(Ref(benchmarks), devicenames, 1.0)

        if dobenchmark && length(unique(devicenames)) > 1
            printlog("Benchmarking")
            bconfs = nph.distribute(computeconfiguration, weights)
            blogps = map(bconfs, workers()) do bconf, worker
                @spawnat worker nph.ComputeAllocation(CuArray, precision, forwardmodel, bconf)
            end

            times = fetch.(map(blogps, workers()) do blogp, worker
                @async @fetchfrom worker benchmark(blogp, sampler.initialconf)
            end)

            weights ./= times
        end
        
        printlog("Assigning images to workers")
        
        dconfs = nph.distribute(computeconfiguration, weights)
        for (hn, dn, w, l) in zip(hostnames, devicenames, weights, length.(dconfs))
            printlog("$hn $dn: $l images, weight $(round(w, sigdigits = 2))")
        end

        logps = map(dconfs, workers()) do dconf, worker
            @spawnat worker nph.ComputeAllocation(CuArray, precision, forwardmodel, dconf)
        end

        for (logp, f, r, worker) in zip(logps, fchannels, rchannels, workers())
            remote_do(startwork, worker, logp, f, r)
        end

        function logpDist(f)
            @sync for c in fchannels
                @async put!(c, f)
            end
            # vals = take!.(rchannels)
            vals = fetch.([@async take!(c) for c in rchannels])
            
            tperworker .+= last.(vals)
            sum(first, vals)
        end
    else
        nph.ComputeAllocation(CuArray, precision, forwardmodel, computeconfiguration)
    end
end

function gc()
    @sync for worker in workers()
        @spawnat worker (GC.gc(); CUDA.reclaim())
    end
end

JLD2.rconvert(::Type{nph.SimulatedAnnealing}, nt::NamedTuple) = nph.SimulatedAnnealing(; nt...)

function loadinputs(infile, outfile)
    @load infile sampler forwardmodel computeconfiguration
    sampler = load(infile, "sampler", typemap=Dict("NPhotons.SimulatedAnnealing" => JLD2.Upgrade(nph.SimulatedAnnealing)))
    reference = jldopen(f -> get(f, "reference", nothing), infile)
    samplekwargs = jldopen(f -> get(f, "samplekwargs", (;)), infile)
    precision = jldopen(f -> get(f, "precision", Float32), infile)
    outstep = jldopen(f -> get(f, "outstep", 1), infile)

    logp = setup(forwardmodel, computeconfiguration, sampler, precision)
    logpref = isnothing(reference) ? 0.0 : logp(reference)
    printlog("logpref = $logpref")
    efunc = Base.Fix1(-, logpref) ∘ logp

    chain = if isfile(outfile)
        printlog("Resuming previous run from $(outfile)")
        [nph.loadChain(outfile)[end]]
    else
        nph.initChain(sampler, efunc)
    end

    efunc, sampler, chain, samplekwargs, outstep
end

function process!(efunc, sampler, chain, samplekwargs, outfile, outstep)
    tperworker .= 0

    tsave = now()
    tlog = now()
    
    nlog = chain[end].nsteps

    endstep = nph.nstepsRemaining(sampler, chain; samplekwargs...) + chain[end].nsteps

    nph.sample!(chain, sampler, efunc; showprogress = false, samplekwargs...) do chain, (step, startstep, endstep)
        if now() - tlog > Second(loginterval)
            steps = (step - nlog)
            trem = Millisecond(round(Int, (endstep - step) / steps * (now() - tlog).value))
            trem = Dates.CompoundPeriod(Dates.canonicalize(trem).periods[1:min(end,2)])
            if !isempty(trem.periods) && trem.periods[end] isa Millisecond
                trem = Dates.CompoundPeriod(trem.periods[1:max(1, end - 1)])
            end
            tperstep = round(Int, (now() - tlog).value / steps)
            printlog("$(step) / $(endstep)  E = $(@sprintf("%.1e", chain[end].energy))  $(tperstep)ms / step  ETA: $(trem)")

            if distributed 
                printlog(join(string.(round.(Int, 1000 .* tperworker ./ steps)), ", ") * " [ms]")
                tperworker .= 0
                # GC.gc(); gc()
            end

            tlog = now()
            nlog = step
        end
        
        if now() - tsave > Minute(saveinterval)
            t = @elapsed nph.saveChain(outfile, chain, stride = outstep)
            printlog("Saved to $(outfile) in $(round(t, sigdigits = 3)) seconds.")
            tsave = now()
        end

        endtime - now(UTC) > Minute(5)
    end

    chain[end].nsteps >= endstep
end


function main()
    for (infile, outfile) in zip(infiles, outfiles)
        printlog("Loading inputs from $(infile)")
        
        efunc, sampler, chain, samplekwargs, outstep = loadinputs(infile, outfile)

        printlog("Processing $(infile)")

        finished = process!(efunc, sampler, chain, samplekwargs, outfile, outstep)

        nph.saveChain(outfile, chain, stride = outstep)

        printlog("$(finished ? "Finished" : "Pausing while")  processing $(infile)")

        endtime - now() < Minute(1) && break
    end
end

main()
