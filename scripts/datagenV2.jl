using LinearAlgebra, ProgressMeter, JLD2, StaticArrays, Dates, FileIO, ArgParse, Glob, StatsBase, ThreadsX

import NPhotons as nph

function printlog(x...)
    println(rpad(string(now()), 23, " "), "  ", join(x, " ")...)
    flush(stdout)
end

const s = ArgParseSettings()
@add_arg_table! s begin
    "--nimages", "-n"
        default = 1000000
        arg_type = Int
    "--nfiles", "-N"
        default = 1
        arg_type = Int
    "infile"
        required = true
        arg_type = String
    "--loginterval", "-l"
        arg_type = Int
        default = 60
    "--useupperbound", "-u"
        action = :store_true
end

const A = (; parse_args(s, as_symbols = true)...)
mkpath("out")

function main()
    printlog("Loading $(A.infile)")
    @load A.infile object forwardmodel qmax

    channel = Channel{Bool}()

    ngen = 0; tlog = now()
    @async while take!(channel)
        ngen += 1
        if now() - tlog > Second(A.loginterval)
            printlog("$ngen / $(A.nimages * A.nfiles) images generated")
            tlog = now()
        end
    end

    upperbound = if A.useupperbound
        points = normalize.(randn(SVector{3, Float64}, 20000))
        xs = range(0, qmax, length = 200)
        ys = [maximum(object.(x .* points)) for x in xs]
        nph.LinearInterpolator(xs, 1.5 .* ys)
    else
        x -> Inf
    end

    for i in 1:A.nfiles
        images = ThreadsX.map(1:A.nimages) do I
            put!(channel, true)
            nph.generateImage(object, forwardmodel, qmax = qmax, upperbound = upperbound)
        end
        
        outfile = nph.enumpath(joinpath("out", splitext(A.infile)[1] * "_generated.jld2"))
        nph.saveImages(outfile, images; object, forwardmodel, qmax)
        printlog("Saved $(A.nimages) images to $outfile")
    end
    
    put!(channel, false)
end

main()
