using NPhotons
using Colors, StaticArrays, LinearAlgebra, StatsBase

using Telegram, Telegram.API
const tg = TelegramClient("1474468146:AAF-1cLZGsQvSlZ9igMcBIOdW5f7SjetLKA", chat_id = "79637328");
notify() = sendMessage(text="Calculation Complete");

theme = Theme(Volume = (algorithm = :iso, isovalue = 1.0, isorange = 0.9, colorrange = (0, 2)))
set_theme!(theme)

function sendPlot(fig)
    save("plots/message.png", fig)
    sendPhoto(photo = open("plots/message.png", "r"))
end

function numpath(path)
    name, ext = splitext(path)
    for i in Iterators.countfrom(1)
        if !ispath("$name$i$ext")
            return "$name$i$ext"
        end
    end
end

function focus()
    display("text/javascript", "window.focus()")
    IJulia.clear_output()
    nothing
end

function window(figlike, f = true)
    backend = Makie.current_backend()
    scene = Makie.get_scene(figlike)
    Makie.update_state_before_display!(figlike)
    screen = Makie.getscreen(backend, scene)
    display(screen, scene)
    f && focus()
end

struct IntervalTicks
    step
end
MakieLayout.get_tickvalues(t::IntervalTicks, vmin, vmax) = ceil(Int, vmin / t.step) * t.step : t.step : floor(Int, vmax / t.step) * t.step

const xlog10 = (xscale = log10, xticks = LogTicks(IntervalTicks(1)), xminorticksvisible=true, xminorticks = IntervalsBetween(9))
const ylog10 = (yscale = log10, yticks = LogTicks(IntervalTicks(1)), yminorticksvisible=true, yminorticks = IntervalsBetween(9))


xinc!(ax, xs...) = vlines!(ax, collect(xs), color = :transparent)
yinc!(ax, ys...) = hlines!(ax, collect(ys), color = :transparent)
include!(ax, ys) = scatter!(ax, xs, ys, color = :transparent)


function liftevery(innode, dt)
    t = Observable(time_ns())
    outnode = Observable(innode[])
    on(innode) do val
        if time_ns() - t[] > dt * 1e9
            t[] = time_ns()
            outnode[] = val
        end
    end
    outnode
end


@recipe(MultiLines) do scene
    Attributes(
        colormap = Reverse(:Set1_3),
        linewidth = 2,
        color = nothing
    )
end

function Makie.plot!(sc::MultiLines{<:Tuple{AbstractVector{<:Real}, AbstractVector{<:AbstractVector{<:Real}}}})
    xs = sc[1]
    yss = sc[2]
    cs = sc.color

    ls = Observable(Point2f[])
    colors = Observable(Float64[])

    function update_plot(xs, yss, cs)
        colors[]

        empty!(ls[])
        empty!(colors[])

        cs = isnothing(cs) ? range(0, 1, length = length(yss)) : cs

        for (t, ys) in zip(cs, yss)
            append!(ls[], Point2f.(xs, ys))
            push!(ls[], Point2f(NaN, NaN))
            append!(colors[], fill(t, length(ys) + 1))
        end

        colors[] = colors[]
    end

    Makie.Observables.onany(update_plot, xs, yss, cs)

    update_plot(xs[], yss[], cs[])

    # if !isnothing(sc.color[])
    #     lines!(sc, ls, color = sc.color, colormap = sc.colormap, linewidth = sc.linewidth)
    # else
    lines!(sc, ls, color = colors, colormap = sc.colormap, linewidth = sc.linewidth)
    # end

    sc
end

function Makie.convert_arguments(P::Type{<:MultiLines}, yss::AbstractVector{<:AbstractVector{<:Real}})
    (1:length(yss[1]), yss)
end

function Makie.convert_arguments(P::Type{<:MultiLines}, xs, yss::AbstractMatrix{<:Real})
    (xs, collect(eachcol(yss)))
end

function Makie.convert_arguments(P::Type{<:MultiLines}, yss::AbstractMatrix{<:Real})
    (1:size(yss, 1), collect(eachcol(yss)))
end

Makie.used_attributes(::Any, x::NPhotons.AtomVolume) = (:ngrid,)

function Makie.convert_arguments(P::Type{<:Volume}, f::NPhotons.AtomVolume; ngrid = 50)
    r = maximum(norm, f.positions) + 3maximum(f.widths)
    xs = range(-r, r, length = ngrid)
    ys = [f(SA[x, y, z], :real) for x in xs, y in xs, z in xs]
    ys ./= maximum(ys)
    (xs, xs, xs, ys)
end

function Makie.convert_arguments(P::Type{<:Scatter}, f::NPhotons.AtomVolume)
    Makie.convert_arguments(P, f.positions)
end


linkCameras!(scenes...) = linkCameras!(scenes)
function linkCameras!(contents)
    scenes = map(contents) do s
        if s isa LScene
            s.scene
        else
            s
        end
    end
    linkCams!(scenes)
end
# linkCameras!(lscenes::Vector{LScene}) = linkCams!([ls.scene for ls in lscenes])


function linkCams!(scenes)
    for scene in scenes[1:end]
        scene.camera = copy(scenes[1].camera)
        on(scene.camera_controls.eyeposition) do ep
            for scene2 in scenes
                scene2.camera_controls.eyeposition.val = ep
            end
        end
        on(scene.camera_controls.lookat) do ep
            for scene2 in scenes
                scene2.camera_controls.lookat.val = ep
            end
        end
        on(scene.camera_controls.upvector) do ep
            for scene2 in scenes
                scene2.camera_controls.upvector.val = ep
            end
        end
        on(scene.camera_controls.fov) do ep
            for scene2 in scenes
                scene2.camera_controls.fov.val = ep
            end
        end
        on(scene.camera_controls.zoom_mult) do ep
            for scene2 in scenes
                scene2.camera_controls.zoom_mult.val = ep
            end
        end
        on(scene.lights[1].position) do ep
            for scene2 in scenes
                scene2.lights[1].position.val = ep
            end
        end
    end
end

function volshow(f::NPhotons.AtomVolume; resolution = (800, 800), ngrid = 50, kwargs...)
    r = maximum(norm, f.positions) + 3maximum(f.widths)
    xs = range(-r, r, length = ngrid)
    ys = f.(nph.grid(xs, dims = 3), :real)

    fig = Figure(resolution = resolution); lscene = LScene(fig[1,1], show_axis=false)
    Camera3D(lscene.scene)
    volshow!(lscene, xs, ys; kwargs...)
    fig
end

function volshow!(lscene, X, V; isovalue = 1.0, isorange = 0.9, colormap = :viridis, normalized = true)
    node = lift(convert(Observable, V)) do x
        normalized ? x / maximum(x) : x
    end
    volume!(lscene, X, X, X, node, algorithm = :iso, isovalue = isovalue, isorange = isorange, 
        colormap = colormap, colorrange = (isovalue - isorange, isovalue + isorange), overdraw = true)
    lscene
end

function ensureweights(x)
    if isa(x, Tuple)
        x
    else
        ([x], [1.0])
    end
end

function monitor(object, state; sortdt = Second(10), aligndt = Second(5), volumedt = Second(1), pseudolog = true)
    fig = Figure(resolution = (1850,1850));
    ga = fig[1,1] = GridLayout()
    gb = fig[2,1] = GridLayout()
    
    if !isa(object, Tuple)
        object = ([object], [1.0])
    end
    objs, objws = object

    r = maximum(maximum(norm, o.positions) + 3maximum(o.widths) for o in objs)
    xs = range(-r, r, length = 50)
    grid = nph.grid(xs, xs, xs)
    
    
    snode = Observable(state)
    onode = Observable(object)

    fnode = Observable(ensureweights(state.configuration))
    wnode = Observable(last.(ensureweights.(state.history)))
    
    perm = collect(1:length(objs))
    Rs = [nph.randomRotation() for o in objs]

    tsort = now() - Second(100)
    talign = now() - Second(100)
    tvol = now() - Second(100)
    on(snode) do state
        fs, ws = ensureweights(state.configuration)
        objs, objws = onode[]

        if now() - tsort > sortdt
            al = [nph.aligned(fs[j], objs[i], ntries = 2000) for i in 1:length(objs), j in 1:length(fs)]
            D = [nph.pointCloudDistance(objs[i].positions, al[i, j].positions) for i in 1:length(objs), j in 1:length(fs)]
            perm = hungarian(D)[1]
            tsort = now()
        end
        if now() - talign > aligndt
            Rs = nph.align.(fs[perm], objs, ntries = 1000)
            talign = now()
        end
        if now() - tvol > volumedt
            fnode[] = (Rs .* fs[perm], ws[perm])
            tvol = now()
        end
        wnode[] = getindex.(normalize.(last.(ensureweights.(state.history)), 1), Ref(perm))
    end

    snode[] = state

    lscenes = []
    for i in 1:length(onode[][1])
        lscene = LScene(ga[1, i], show_axis = false)
        push!(lscenes, lscene)
        Camera3D(lscene.scene)
        node = lift(f -> f[1][i].(grid, :real), onode)
        volshow!(lscene, xs, node, colormap = :ice)
    end
    for i in 1:length(fnode[][1])
        lscene = LScene(length(objs) > 1 ? ga[2, i] : ga[1, i + 1], show_axis = false)
        push!(lscenes, lscene)
        Camera3D(lscene.scene)
        node = lift(f -> f[1][i].(grid, :real), fnode)
        volshow!(lscene, xs, node, colormap = :viridis)
    end


    linkCameras!([lscene.scene for lscene in lscenes])
    # colgap!(gr, 2); rowgap!(gr, 2)
    
    ticks = [-10 .^ (0:10); 0; 1 .* 10 .^ (0:10)]
    ticklabels = ["-10" .* Makie.to_superscript.(0:10); "0"; "10" .* Makie.to_superscript.(0:10)]
    ax1 = if pseudolog
        Axis(gb[1,1], yscale = Makie.pseudolog10, yticks = (ticks, ticklabels), xlabel = "annealing steps", ylabel = "-log(p)")
    else
        Axis(gb[1,1], xlabel = "annealing steps", ylabel = "-log(p)")
    end
    lines!(ax1, lift(state -> state.energies, snode))
    hlines!(ax1, [0, 1], color = :transparent)
    on(snode) do s
        autolimits!(ax1)
    end
    
    if length(objs) > 1
        colors = cgrad(:seaborn_bright, 10, categorical = true)
        ax2 = Axis(gb[1,2], xlabel = "annealing steps", ylabel = "weights")
        for i in 1:length(state.configuration[1])
            lines!(ax2, lift(ws -> [SA[state.histstep * (j-1), ws[j][i]] for j in 1:length(ws)], wnode), label = "weight $i", color = colors[i])
        end
        hlines!(ax2, objws, color = colors[1:length(objws)], linewidth = 2)
        axislegend(ax2, position = :lt)

        on(snode) do s
            autolimits!(ax2)
        end
    end
    # ax3 = Axis(fig[2, 1:2])
    # barplot!(ax3, (1:length(objws)) .- 0.12, objws, width = 0.2, color = colors[1:length(objws)])
    # barplot!(ax3, (1:length(objws)) .+ 0.12, lift(last, wnode), width = 0.2, color = colors[1:length(objws)])
    # ylims!(ax3, -0.05, 1.05)
    # hidexdecorations!(ax3)
    

    
    
    
    # rowsize!(fig.layout, 1, Relative(0.5))
    # rowsize!(fig.layout, 2, Relative(0.1))
    
    # display(fig)
    fig, onode, snode
end