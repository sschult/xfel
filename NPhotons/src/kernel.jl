@inline function warpreduce(op, x, o = 16, u = 0, mask = 0xFFFFFFFF)
    while o > u
        x = op(x, shfl_down_sync(mask, x, o));
        o ÷= 2
    end
    x
end

@inline warpsum(x, args...) = warpreduce(+, x, args...)
@inline warpprod(x, args...) = warpreduce(*, x, args...)


function kernel_cpu!(R, X, P, I, ::Val{maxo}, ::Val{stride}) where {maxo, stride}
    for i in axes(I, 1), j in axes(P, 2)
        l = I[i,1]
        y = zero(eltype(R))

        for tidx in 1:stride
            x = fill(P[l+1,j], maxo ÷ stride + 1)
            for k = 2:l+1
                for o in 0:maxo÷stride
                    x[o + 1] *= X[I[i,k] + o * stride + tidx - 1, j]
                end
            end
            y += sum(x)
        end

        R[i, j] += y
    end

    return
end

function kernel!(results, allresults, fvalues, pfactors, imagelocations, images, polarization, background, imagerange, ioff, roff, ::Val{maxo}, ::Val{chunksize}, ::Val{nsingle}) where {maxo, chunksize, nsingle}
    tidx = threadIdx().x; noffsets = maxo ÷  chunksize + 1
    i = (blockIdx().y - one(blockIdx().y)) * blockDim().y + threadIdx().y + ioff
    j = (blockIdx().z - one(blockIdx().z)) * blockDim().z + threadIdx().z + roff

    fvalues, imagelocations, images, pfactors = CUDA.Const.((fvalues, imagelocations, images, pfactors))
    polarization = isnothing(polarization) ? nothing : CUDA.Const(polarization); 
    background = isnothing(background) ? nothing : CUDA.Const(background)

    @inbounds if i <= length(imagerange)
        imageindex = imagelocations[imagerange[i]]
        imagelength = images[imageindex]
        
        x = MVector{noffsets, eltype(pfactors)}(undef)
        for o in 1:noffsets
            x[o] = pfactors[imagelength + one(imagelength), j, ndims(pfactors) == 3 ? 1 + ((o - 1) * chunksize + tidx - 1) : 1]
        end

        @inline function imageproduct!(xt, k1, k2)
            @inbounds for k in k1:k2
                p = images[imageindex + k]
                for o in 1:noffsets
                    intensity = fvalues[p + ((o - one(o)) * chunksize + tidx - 1), j]
                    !isnothing(polarization) && (intensity *= polarization[p])
                    !isnothing(background) && (intensity += background[p, j])
                    xt[o] *= intensity
                end
            end
        end

        if eltype(pfactors) == eltype(fvalues)
            imageproduct!(x, UInt32(1), imagelength)
        else
            xt = ones(MVector{noffsets, eltype(fvalues)})
            k1 = UInt32(1)
            while k1 <= imagelength
                imageproduct!(xt, k1, min(k1 + nsingle - one(k1), imagelength))
                x .*= xt
                xt .= one(eltype(xt))
                k1 += nsingle
            end
        end

        cx = convert(MVector{length(x), eltype(results)}, x)
        y = warpsum(sum(cx), chunksize ÷ 2)

        if tidx == 1
            CUDA.@atomic results[i, j - roff] += y
        end

        if !isnothing(allresults)
            for o in 1:noffsets
                allresults[(o - one(o)) * chunksize + tidx, i, j - roff] = x[o]
            end
        end
    end

    return
end

function applyKernel!(results, allresults, fvalues::CuArray, pfactors, imagelocations, images, ::Val{npercircle}, rotationrange, polarization = nothing, background = nothing, imagerange = nothing) where npercircle
    chunksize = min(8, npercircle)
    dimy = 512 ÷ chunksize
    nsingle = 50

    if isnothing(imagerange)
        imagerange = eachindex(imagelocations)
    end
    imagerange = UnitRange{UInt32}(imagerange)

    for ir in partitionrange(1 : ceil(Int, length(imagerange) / dimy), maxl = 65535)
        blocks = (1, length(ir), length(rotationrange))
        ioff = UInt32((minimum(ir) - 1) * dimy)
        roff = UInt32(minimum(rotationrange) - 1)

        @cuda name="logp_kernel" always_inline=true threads=(chunksize,dimy,1) blocks=blocks kernel!(results, allresults, fvalues, pfactors, imagelocations, images, polarization, background, imagerange, ioff, roff, Val(Int32(npercircle - 1)), Val(Int32(chunksize)), Val(UInt32(nsingle)))
    end
end

function applyKernel!(R, X, P, I, maxoffset)
    kernel_cpu!(R, X, P, I, Val(maxoffset), Val(8))
end
