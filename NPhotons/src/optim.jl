const Itp = LinearInterpolator{Float64, NoBoundaries}

makeitp(ys::Pair{<:Number, <:Number}) = makeitp([Float64(ys[1]), Float64(ys[2])])
makeitp(y::Number) = makeitp([Float64(y), Float64(y)])
makeitp(ys::AbstractVector{<:Number}) = makeitp(range(0, 1, length = length(ys)), ys)
makeitp(xs::AbstractVector{<:Number}, ys::AbstractVector{<:Number}) = LinearInterpolator(xs, ys, NoBoundaries())
makeitp(xsys::AbstractVector{<:AbstractVector}) = makeitp(xsys[1], xsys[2])
makeitp(xsys::Tuple{<:AbstractVector, <:AbstractVector}) = makeitp(xsys[1], xsys[2])
makeitp(xsys::AbstractVector{<:Pair{<:Number, <:Number}}) = makeitp(Float64.(first.(xsys)), Float64.(last.(xsys)))
makeitp(itp::Itp) = itp

Base.convert(::Type{Itp}, x::Float64) = makeitp(x)

struct NoForce end
(::NoForce)(f, t) = 0 * f.positions

struct RepulsiveForce
    distance::Itp
    strength::Itp
    relative::Bool
    profile::Symbol
end

function RepulsiveForce(; distance, strength, relative = false, profile = :gaussian)
    RepulsiveForce(makeitp(distance), makeitp(strength), relative, profile)
end

function (force::RepulsiveForce)(f, t)
    d = force.distance(t)
    s = force.strength(t)
    if d == 0
        return 0f.positions
    end
    map(f) do (y1, h1, w1)
        s * sum(f) do (y2, h2, w2)
            r = norm(y1 - y2)
            σ = force.relative ? w1^2 + w2^2 : one(w1)

            a = if force.profile == :gaussian
                exp(-r^2 / (2*d^2 * σ))
            elseif force.profile == :smoothstep
                smoothstep(r, 0.9d, d, 1, 0)
            end
            a * (y1 - y2) / ifelse(r == 0, one(r), r)
        end
    end
end

struct CompactifyingForce
    a::Itp
    b::Itp
    threshold::Itp
    strength::Itp
end

function CompactifyingForce(; a, b, threshold, strength)
    CompactifyingForce(makeitp(a), makeitp(b), makeitp(threshold), makeitp(strength))
end

function (force::CompactifyingForce)(f, t)
    a = force.a(t); b = force.b(t)
    threshold = force.threshold(t)
    strength = force.strength(t)
    map(f) do (y1, h1, w1)
        s = sum(f) do (y2, h2, w2)
            smoothstep(norm(y1 - y2), a, b, 1, 0)
        end
        r = sum(f) do (y2, h2, w2)
            smoothstep(norm(y1 - y2), 2a, 2b, 1, 1e-5) * (y2 - y1)
        end
        strength * normalize(r) * smoothstep(s, threshold - 1, threshold, 1, 0)
    end
end

struct AnnealedSGD{T, F} <: AbstractSampler
    initialconf::T
    
    σ::Itp
    q::Itp
    
    η::Itp
    β::Itp

    ηfactor::Tuple{Itp, Itp, Itp}

    nsteps::Int
    nperlevel::Int
    refstep::Int

    force::F

    maxstepsize::Float64
    radreg::Itp
    
    function AnnealedSGD(initialconf, σ, q, η, β, ηfactor, nsteps, nperlevel, refstep, force, maxstepsize, radreg)
        new{typeof(initialconf), typeof(force)}(initialconf, makeitp(σ), makeitp(q), makeitp(η), makeitp(β), makeitp.(ηfactor), nsteps, nperlevel, refstep, force, maxstepsize, makeitp(radreg))
    end
end

function AnnealedSGD(; init, σ = 0.0, q = 1e10, η = 1.0, β = 0.0, ηfactor = (1.0, 0.0, 0.0), nsteps, nperlevel, refstep = 50, maxstepsize = Inf, force = NoForce(), radreg = 0.0)
    AnnealedSGD(init, σ, q, η, β, ηfactor, nsteps, nperlevel, refstep, force, maxstepsize, radreg)
end

@kwdef struct SGDState{F, DF, P}
    conf::F
    grad::DF
    force::DF
    velocity::DF

    nsteps::Int
    t::Float64
    
    energy::Float64
    batchlogp::Float64
    time::Float64

    params::P
end

function init(opt::AnnealedSGD)
    f = smoothen(deepcopy(opt.initialconf), opt.σ(0))
    df = differential(f)
    SGDState(
        conf = f, grad = df, velocity = 0.0df, force = 0.0df, 
        nsteps = 0, t = -1 / (opt.nsteps - 1), energy = NaN, batchlogp = NaN, time = time(),
        params = (η = opt.η(0), β = opt.β(0), σ = opt.σ(0), q = opt.q(0)), 
    )
end

nsteps(optimizer::AnnealedSGD) = opt.nsteps * opt

function optimize!(callback::Function, opt, chain, grad, logpr, reference; showprogress = true)
    prog = Progress(opt.nsteps, showspeed = true, enabled = showprogress)

    ts = range(0, 1, length = opt.nsteps)
    ts = ts[findfirst(>(chain[end].t), ts):end]

    refσ = chain[end].params.σ

    for (i, t) in enumerate(ts)
        η = opt.η(t); β = opt.β(t); σ = opt.σ(t); q = opt.q(t)
        ηfactor = map(itp -> itp(t), opt.ηfactor)
    
        c = chain[end]; f = deepcopy(c.conf)
        
        if mod1(c.nsteps + 1, opt.nperlevel) == 1 || i == 1
            refσ = σ
            f.widths .= smoothen(sharpen(f, c.params.σ), refσ).widths
            @tc "filter" disable_sigint() do 
                filterImages!(grad, σ = σ, qmax = q, smooth = 0.05)
                filterImages!(logpr, σ = σ, qmax = q, smooth = 0.05)
            end
        end
        @tc "randomize" randomizeImages!(grad)

        @tc "gradient" p, g = disable_sigint(() -> grad(f; radreg = opt.radreg(t)))

        @t  "force" begin
            ff = 0g
            forces = opt.force isa Vector ? opt.force : [opt.force]
            for force in forces
                ff.positions .+= force(f, t)
            end
        end
        dv = g + ff
        dv.positions .-= Ref(mean(dv.positions))
        dv.heights .-= mean(dv.heights)
        
        v = β * c.velocity + dv
        s = (η .* ηfactor) * v
        if isfinite(opt.maxstepsize)
            s.positions .= clampnorm.(s.positions, opt.maxstepsize)
        end
        newf = f + s

        if ηfactor[2] != 0
            for _ in 1:10
                newf.heights .= clamp.(newf.heights, extrema(reference.heights)...)
                newf.heights .*= sum(reference.heights) / sum(newf.heights)
            end
        end
        if ηfactor[3] != 0
            sref = smoothen(reference, refσ)
            for _ in 1:10
                newf.widths .= clamp.(newf.widths, extrema(sref.widths)...)
                newf.widths .*= sum(sref.widths) / sum(newf.widths)
            end
        end

        newf.positions .-= (mean(newf.positions, Weights(newf.heights)),)

        energy = if mod1(c.nsteps + 1, opt.refstep) == 1
            @tc "reference" logpr(smoothen(reference, refσ); radreg = opt.radreg(t)) - logpr(newf; radreg = opt.radreg(t))
        else
            c.energy
        end

        push!(chain, SGDState(
            conf = newf, grad = g, velocity = v, force = ff, nsteps = c.nsteps + 1, batchlogp = p,
            energy = energy, time = time(), t = t, params = (η = η, β = β, σ = refσ, q = q)
        ))

        next!(prog)

        @t "callback" b = callback(chain)
        !b && break
    end

    chain
end

