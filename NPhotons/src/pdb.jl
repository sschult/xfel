function getpdb(pdb)
    if isfile(pdb)
        return read(pdb, BioStructures.PDBFormat)
    end
    pdb = uppercase(pdb)
    if occursin(".pdb", pdb)
        return read(pdb, BioStructures.PDBFormat)
    end
    try
        path = joinpath("pdb", pdb * ".pdb")
        if !isfile(path)
            BioStructures.downloadpdb(pdb, dir="pdb/")
        end
        return read(path, BioStructures.PDBFormat)
    catch e
        println("PDB not found, trying mmCIF")
    end
    path = joinpath("pdb", pdb * ".cif")
    if !isfile(path)
        BioStructures.downloadpdb(pdb, dir="pdb/", format = BioStructures.MMCIFFormat)
    end
    return read(path, BioStructures.MMCIF)
end

const atomfactorlist = Dict(
    "C" => (0.7, 6.0),
    "N" => (0.65, 7.0),
    "S" => (1.0, 16.0),
    "O" => (0.6, 8.0),
    "default" => (0.7, 6.0),
    "P" => (0.65, 15.0),
    "H" => (0.3, 1.0),
    "D" => (0.3, 1.0)
)

function element(name)
    string(filter(x -> occursin(x, "CNSOPHD"), name)[1])
end

function atomic_number(name)
    atomfactorlist[element(name)][2]
end

function atomic_radius(name)
    atomfactorlist[element(name)][1]
end

function loadGRO(path; normalized = true, subtractcm = true)
    M = readlines(path)
    positions = [parse.(Float64, split(l[23:44])) for l in M[3:end-1]]
    filter!(==(3) ∘ length, positions)
    positions = 10 .* SVector{3, Float64}.(positions);
    atomnames = [first(strip(l[13:15])) for l in M[3:end-1]]

    atomfactorlist = Dict(
        "C" => (0.7, 6.0),
        "N" => (0.65, 7.0),
        "S" => (1.0, 16.0),
        "O" => (0.6, 8.0),
        "default" => (0.7, 6.0),
        "P" => (0.65, 15.0),
        "H" => (0.3, 1.0),
        "D" => (0.3, 1.0)
    )

    af = get.(Ref(atomfactorlist), atomnames, Ref((0.7, 6.0)))
    widths = first.(af)
    heights = last.(af)

    subtractcm && (positions .-= Ref(mean(positions)))
    o = AtomVolume(positions, heights, widths)
    if normalized
        o = normalize(o)
    end
    o
end

function loadPDB(pdb, select = :heavy; normalization = :l1, subtractcm = true)
    selector = Dict(
        :heavy => BioStructures.heavyatomselector, 
        :calpha => BioStructures.calphaselector,
        :all => BioStructures.allselector
    )[select]
    
    struc = getpdb(pdb)

    atomfactorlist = Dict(
        "C" => (0.7, 6.0),
        "N" => (0.65, 7.0),
        "S" => (1.0, 16.0),
        "O" => (0.6, 8.0),
        "default" => (0.7, 6.0),
        "P" => (0.65, 15.0),
        "H" => (0.3, 1.0),
        "D" => (0.3, 1.0)
    )
    # println(struc)
    atoms = BioStructures.collectatoms(struc, selector)
    positions = SVector{3,Float64}[]
    widths = Float64[]
    heights = Float64[]
    for atom in atoms
        push!(positions, BioStructures.coords(atom))
        el = BioStructures.element(atom)
        if el == ""
            el = BioStructures.atomname(atom)[1:1]
        end
        af = get(atomfactorlist, el, (0.7, 6.0))
        push!(widths, af[1])
        push!(heights, af[2])
    end

    subtractcm && (positions .-= Ref(mean(positions)))
    o = AtomVolume(positions, heights, widths, maximum(norm, positions, init = 0.0) + maximum(widths, init = 0.0))
    if normalization == :l1
        o = normalize(o)
    elseif normalization == :crambin1
        o.heights .*= 0.00125
    end
    o
end

function exportPDB(path, f::AtomVolume)
    atoms = map(enumerate(f)) do (i, (y, h, σ))
        atomfactors = [(key, value[1]) for (key, value) in atomfactorlist if key != "default"]
        element = argmin(a -> abs(a[2] - σ), atomfactors)[1]
        PDBTools.Atom(index = i-1, name = element, x = y[1], y = y[2], z = y[3])
    end
    PDBTools.write_pdb(path, atoms)
end

function exportPDB(filename, fs::Array)
    atomfactors = [(key, value[1]) for (key, value) in atomfactorlist if key != "default"]
    open(expanduser(filename), "w") do file
        @showprogress for f in fs
            for (i, (y, h, σ)) in enumerate(f)
                element = argmin(a -> abs(a[2] - σ), atomfactors)[1]
                atom = PDBTools.Atom(index = i-1, name = element, x = y[1], y = y[2], z = y[3])
                # if PDBTools.only(atom)
                    println(file, PDBTools.write_pdb_atom(atom))
                # end
            end
            println(file, "ENDMDL")
        end
    end
end