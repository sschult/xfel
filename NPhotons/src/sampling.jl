abstract type AbstractProposal end

@kwdef struct AdaptiveProposal{T} <: AbstractProposal
    proposer::T
    initialstepsize::Float64 = 1.0
    steplambda::Float64 = 1.01
    stepmin::Float64 = 1e-2
    stepmax::Float64 = 1e2
    targetrate::Float64 = 0.23
end

AdaptiveProposal(proposer; kwargs...) = AdaptiveProposal(; proposer = proposer, kwargs...)


function initParams(p::AdaptiveProposal)
    p.initialstepsize
end

function nextParams(p::AdaptiveProposal, stepsize, accepted::Bool)
    if accepted
        stepsize *= p.steplambda ^ ((1 - p.targetrate) / p.targetrate)
    else
        stepsize /= p.steplambda
    end
    clamp(stepsize, p.stepmin, p.stepmax)
end

function propose(conf, proposal::AdaptiveProposal, stepsize)
    neighbor(proposal.proposer, conf, stepsize)
end

function proposaldensity(oldconf, newconf, proposal::AdaptiveProposal, stepsize)
    logprop(proposal.proposer, newconf, oldconf, stepsize)
end



@kwdef struct AlternatingProposal{T} <: AbstractProposal
    proposals::T
end

AlternatingProposal(proposals...) = AlternatingProposal(proposals)

function initParams(proposal::AlternatingProposal)
    (; index = 1, params = initParams.(proposal.proposals))
end

function nextParams(proposal::AlternatingProposal, params, accepted::Bool)
    i = params.index
    newparams = @set params.params[i] = nextParams(proposal.proposals[i], params.params[i], accepted)
    newparams = @set newparams.index = mod1(i + 1, length(newparams.params))
end

function propose(conf, proposal::AlternatingProposal, params)
    propose(conf, proposal.proposals[params.index], params.params[params.index])
end

function proposaldensity(oldconf, newconf, proposal::AlternatingProposal, params)
    proposaldensity(oldconf, newconf, proposal.proposals[params.index], params.params[params.index])
end

abstract type AbstractSampler end

@kwdef struct SimulatedAnnealing{T1, T2<:AbstractProposal} <: AbstractSampler
    initialconf::T1
    initialtemperature::Float64 = 1.0
    targettemperature::Float64 = 1.0
    thalf::Float64 = Inf

    proposal::T2
end

# function SimulatedAnnealing(initialmodel, initialtemperature, tlambda, proposer)
#     SimulatedAnnealing(initialmodel, initialtemperature, tlambda, (proposer,))
# end

# function SimulatedAnnealing(initialmodel, proposers; kwargs...)
#     !isa(proposers, Tuple) && (proposers = (proposers,))
#     # thalf != 0 && (tlambda = 2^(-1/thalf))

#     SimulatedAnnealing(; initialmodel = initialmodel, proposers = proposers, kwargs...)
# end

@kwdef struct AnnealingState{T1, T2}
    conf::T1
    energy::Float64

    nsteps::Int
    proposalParams::T2

    temperature::Float64
end

function Base.show(io::IO, state::AnnealingState)
    show(io, AnnealingState)
    print(io, "(energy = $(state.energy), nsteps = $(state.nsteps), temperature = $(state.temperature))")

    if !get(io, :compact, false)
        println(io, ":")
        print(io, "  ")
        show(io, state.conf)
    end
end


function initState(sampler::SimulatedAnnealing; energy = Inf)
    AnnealingState(
        conf = sampler.initialconf, energy = energy, nsteps = 0,
        proposalParams = initParams(sampler.proposal), 
        temperature = sampler.initialtemperature
    )
end

function initState(sampler, efunc)
    initState(sampler; energy = efunc(sampler.initialconf))
end

function initChain(sampler, args...; kwargs...)
    [initState(sampler, args...; kwargs...)]
end

function step(state::AnnealingState, sampler::SimulatedAnnealing, efunc)
    newconf = propose(state.conf, sampler.proposal, state.proposalParams)
    newenergy = efunc(newconf)

    forward = proposaldensity(state.conf, newconf, sampler.proposal, state.proposalParams)
    backward = proposaldensity(newconf, state.conf, sampler.proposal, state.proposalParams)

    deltaE = newenergy - state.energy
    deltaG = forward - backward
    
    !isfinite(newenergy) && @warn "infinite value encountered"

    accepted = isfinite(newenergy) && (deltaE < 0 || exp(-(deltaE + deltaG) / state.temperature) > rand())
    AnnealingState(
        conf = accepted ? newconf : state.conf,
        energy = accepted ? newenergy : state.energy,
        nsteps = state.nsteps + 1,
        proposalParams = nextParams(sampler.proposal, state.proposalParams, accepted),
        temperature = max(sampler.targettemperature, 2^(-1/sampler.thalf) * state.temperature)
    )
end

function nstepsRemaining(thalf::Real, temperature::Real, targettemperature::Real = 1)
    round(Int, log(2^(-1/thalf), targettemperature / temperature))
end

function nstepsRemaining(sampler, state::AnnealingState; addsteps = 0, nsteps = nothing)
    if isnothing(nsteps)
        nstepsRemaining(sampler.thalf, state.temperature, sampler.targettemperature) + addsteps
    else
        max(0, nsteps - state.nsteps) + addsteps
    end
end
nstepsRemaining(sampler, chain::AbstractArray; kwargs...) = nstepsRemaining(sampler, chain[end]; kwargs...)
nstepsRemaining(sampler) = nstepsRemaining(sampler.thalf, sampler.initialtemperature, sampler.targettemperature)

function sample!(callback::Function, chain::AbstractArray, sampler::AbstractSampler, efunc; showprogress = true, kwargs...)
    nsteps = nstepsRemaining(sampler, chain; kwargs...)
    prog = Progress(nsteps; enabled = showprogress, showspeed = true)

    startstep = chain[end].nsteps + 1
    endstep = startstep + nsteps

    for i in startstep:endstep
        s = step(chain[end], sampler, efunc)
        push!(chain, s)

        !callback(chain, (i, startstep, endstep)) && break

        next!(prog; showvalues = ((:nsteps, s.nsteps), (:energy, s.energy), (:temperature, s.temperature), (:params, s.proposalParams)))
    end

    chain
end

function sample!(chain::AbstractArray, sampler::AbstractSampler, args...; kwargs...)
    sample!(s -> true, chain, sampler, args...; kwargs...)
end

function sample(callback::Function, sampler::AbstractSampler, efunc, args...; kwargs...)
    sample!(callback, [initState(sampler, efunc)], sampler, efunc, args...; kwargs...)
end

function sample(sampler::AbstractSampler, args...; kwargs...)
    sample(s -> true, sampler, args...; kwargs...)
end

function acceptanceRate(chain::AbstractArray; n = 100)
    a = [chain[i].conf != chain[i+1].conf for i in 1:length(chain)-1]
    # movingGaussian(a, n)
    movingAverage(a, n)
end