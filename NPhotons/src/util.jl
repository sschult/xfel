import Base.angle

prange(min, max, length) = range(min, ifelse(length > 0, max, min), length = length + 1)[1:end - 1]
ssqrt(x) = sign(x) * sqrt(abs(x))
pseudolog(x) = sign(x) * log(abs(x) + 1)
pseudolog10(x) = sign(x) * log10(abs(x) + 1)
polar2xyz(ra) = ((r, a) = ra; [r * cos(a), r * sin(a), 0.0])
polar2xy(ra) = ((r, a) = ra; [r * cos(a), r * sin(a)])
sph2xyz(ϕ, θ) = SA[cos(ϕ) * sin(θ), sin(ϕ) * sin(θ), cos(θ)]
sph2xyz(ϕθ) = sph2xyz(ϕθ...)
xyz2sph(xyz) = [norm(xyz), acos(xyz[3] / norm(xyz)), atan(xyz[1], xyz[2])]
roundmult(val, prec) = (inv_prec = 1 / prec; round(val * inv_prec) / inv_prec)
roundTo(val, rnge) = clamp(roundmult(val - rnge[1], step(rnge)) + rnge[1], rnge[1], rnge[end])
findrange(val, rnge) = clamp(round(Int, (val - rnge[1]) / Base.step(rnge)) + 1, 1, length(rnge))
ceilrange(val, rnge) = clamp(ceil(Int, (val - rnge[1]) / Base.step(rnge)), 1, length(rnge))
floorrange(val, rnge) = clamp(floor(Int, (val - rnge[1]) / Base.step(rnge)), 1, length(rnge))
angle(a, b) = (na = norm(a); nb = norm(b); ifelse(na * nb == 0, zero(na), acos(clamp(a ⋅ b / max(norm(a) * norm(b)), -1, 1))))
angled(a, b) = (na = norm(a); nb = norm(b); ifelse(na * nb == 0, zero(na), acosd(clamp(a ⋅ b / max(norm(a) * norm(b)), -1, 1))))
project_path(parts...) = normpath(joinpath(@__DIR__, "..", parts...))
grid(xs...; dims = 0) = SVector.([reshape(xs[mod1(i, length(xs))], fill(1, i - 1)..., :) for i in 1:ifelse(dims == 0, length(xs), dims)]...)
conflse(ps, ws) = sum(logsumexp(ps, ws', dims = 2))
conflse(ps) = sum(logsumexp(ps, fill(1/size(ps, 2), size(ps, 2))', dims = 2))
conflse(ps::Array{<:Vector}, ws) = sum(logsumexp(reduce(hcat, ps), ws', dims = 2))
conflse(ps::Array{<:Vector}) = conflse(ps, fill(1/length(ps), length(ps)))
dihedral(a, b, c) = mod1(atan(((a × b) × (b × c)) ⋅ b / norm(b), (a × b) ⋅ (b × c)), 2pi)
dihedral(a, b, c, d) = mod1(dihedral(b - a, c - b, d - c), 2pi)
logrange(a, b; length) = exp.(range(log(length != 1 ? a : b), log(b), length = length))
logrange(T, a, b; length) = unique(round.(T, logrange(a, b, length = length)))
downsample(a, n, ker = sum) = map(idx -> ker(view(a, idx...)), Iterators.product(Iterators.partition.(axes(a), n)...))
d2e(x; λ = 2.5) = 2pi/λ * normalize(x + SA[0, 0, 2pi/λ]) - SA[0, 0, 2pi/λ]
e2d(k; λ = 2.5) = (k + SA[0, 0, 2pi/λ]) * (2pi/λ / (k[3] + 2pi/λ)) - SA[0, 0, 2pi/λ]
proj2d(x) = x[SA[1,2]]
polarizationFactor(k; λ = 2.5) = 1.0 - (k[2] * λ / 2pi)^2;
getperiodic(A, i) = A[mod1(i, length(A))]
middles(A) = middle.(A[1:end-1], A[2:end])
clampnorm(v, r) = norm(v) <= r ? v : r * normalize(v)


smoothstep(x) = ifelse(x < 0, zero(x), ifelse(x > 1, one(x), x * x * (3 - 2x)))
smoothstep(x, x0, x1) = smoothstep((x - x0) / (x1 - x0))
smoothstep(x, x0, x1, y0, y1) = y0 + (y1 - y0) * smoothstep(x, x0, x1)
smoothbump(x) = smoothstep(1 - x) * smoothstep(x + 1)
smootherstep(x) = ifelse(x < 0, zero(x), ifelse(x > 1, one(x), x * x * x * (x * (6x - 15) + 10)))
smootherstep(x, x0, x1) = smootherstep((x - x0) / (x1 - x0))
smootherstep(x, x0, x1, y0, y1) = y0 + (y1 - y0) * smootherstep(x, x0, x1)
smootherbump(x) = smootherstep(1 - x) * smootherstep(x + 1)

achievableResolution(natoms, ngaussians) = 3*cbrt(5*3/4/pi)*cbrt(natoms/ngaussians)


getconf(state) = state.conf
getenergy(state) = state.energy
gettemperature(state) = state.temperature
getnsteps(state) = state.nsteps
getpositions(f) = f.positions
getheights(f) = f.heights
getwidths(f) = f.widths

function serializeIndexImages(indexImages)
    inds = Int[]
    result = eltype(eltype(indexImages))[]
    for image in indexImages
        push!(result, length(image))
        push!(inds, length(result))
        append!(result, image)
    end
    inds, result
end

function smoothingKernel(σ)
    kernel = AtomVolume(SA[SA[0.0,0.0,0.0]], SA[1.0], SA[Float64(σ)])
    r -> kernel(SA[r,0.0,0.0])
end
avKernel(σ) = smoothingKernel(σ)

function smoothingFilter(σ)
    kernel = smoothingKernel(σ)
    image -> [p for p in image if kernel(norm(p)) > rand()]
end

function smoothenImages(images, σ)
    smoothingFilter(σ).(images)
end

function smoothenImage(images, σ)
    smoothingFilter(σ)(images)
end

function fieldsizeof(alloc)
    fs = [(f, sizeof(getfield(alloc, f))) for f in propertynames(alloc)]
    filter!(>(0) ∘ last, fs)
    l = maximum(length ∘ string ∘ first, fs) + 1
    for (f, s) in sort(fs, by = last, rev = true)
        if s > 0 && getfield(alloc, f) isa AbstractArray
            println("$(rpad(f, l)) $(Base.format_bytes(s))")
        end
    end
end


function repulsiveForce(f; strength = 1.0, d = 1.0, relative = false)
    map(f) do (y1, h1, w1)
        sum(f) do (y2, h2, w2)
            r = norm(y1 - y2)
            σ = relative ? w1^2 + w2^2 : one(w1)
            exp(-r^2 / (2*d^2 * σ)) * (y1 - y2) / ifelse(r == 0, one(r), r)
        end
    end
end

function coulombForce(f, d = 1.0)
    map(f) do (y1, h1, w1)
        sum(f) do (y2, h2, w2)
            r = norm(y1 - y2)
            r != 0 ? (y1 - y2) ./ r^3 : 0 * y1
        end
    end
end

function compactifyingForce(f, a = 1.0, b = 2.0, threshold = 1)
    map(f) do (y1, h1, w1)
        s = sum(f) do (y2, h2, w2)
            smoothstep(norm(y1 - y2), a, b, 1, 0)
        end
        -normalize(y1) * smoothstep(s, threshold - 1, threshold, 1, 0)
    end
end


function bondforceAttractive(f, a = 1.0, b = 2.0, relative = true)
    map(f) do (y1, h1, w1)
        y2, h2, w2 = argmin(yhw -> norm(yhw[1] - y1) + (yhw[1] == y1) * Inf, f)
        r = norm(y2 - y1)
        smoothstep(r, a, b, 0, 1) * (y2 - y1) / ifelse(r == 0, one(r), r)
    end
end

function bondforceRepulsive(f, a = 0.5, b = 1.0, relative = true)
    map(f) do (y1, h1, w1)
        sum(f) do (y2, h2, w2)
            r = norm(y2 - y1)
            smoothstep(r, a, b, 1, 0) * (y1 - y2) / ifelse(r == 0, one(r), r)    
        end
    end
end

function imageMatrix(npixels, indexImages)
    imageIndices = spzeros(Int, npixels, length(indexImages))
    for (i, indexImage) in enumerate(indexImages)
        for j in indexImage
            imageIndices[j, i] += 1
        end
    end
    imageIndices
end

function lengthMatrix(npixels, indexImages)
    imageLengths = spzeros(Int, maximum(length, indexImages) + 1, length(indexImages))
    for (i, indexImage) in enumerate(indexImages)
        imageLengths[length(indexImage) + 1, i] += 1
    end
    imageLengths
end


function countAttributions(images::Vector{<:Vector{<:Any}})
    v = 1 / length(images)
    c = Accumulator{eltype(eltype(images)), Float64}()
    for image in images
        for p in image
            inc!(c, p, v)
        end
    end
    collect(values(sort(Dict(c))))
end

function countAttributions(images)
    countAttributions(getindex.(images, 2))
end

function gaussJacobiQuadrature(D::Beta, n)
    xs, ws = gaussjacobi(n, D.β - 1, D.α - 1)
    xs .= (xs .+ 1) ./ 2
    ws ./= sum(ws)
    xs, ws
end

function maprange(X, a = 0, b = 1)
    low, high = extrema(X)
    (X .- low) ./ (high - low) .* (b - a) .+ a
end

function ensureweights(x)
    if isa(x, Tuple)
        x
    else
        ([x], [1.0])
    end
end

_getstructures(x::AtomVolume) = x
_getstructures(x) = _getstructures(x[1])

function getstructures(x)
    s = _getstructures(x)
    s isa Vector ? s : [s]
end

function partitionrange(rnge; stride = 0, maxl = 1)
    n = length(rnge)
    s = ifelse(stride != 0, stride, ceil(Int, n / ceil(n / maxl)))
    (rnge[i:min(n, i+s-1)] for i in 1:s:n)
end

function spiral(n; r = 10.0, h = 20.0, m = 2)
    φs = range(0, 2pi*m, length = n); zs = range(-h/2, h/2, length = n)
    [SA[r * cos(φ), r * sin(φ), z] for (φ, z) in zip(φs, zs)]
end

function nrrw(n, d; ntries = 10000)
    ps = [SA[0.0,0.0,0.0]]
    for i in 1:ntries
        length(ps) >= n && break
        q = ps[end] .+ d .* normalize(randn(3))
        if all(norm.(Ref(q) .- ps) .>= d)
            push!(ps, q)
        end
    end
    ps .- Ref(mean(ps))
end

# function assignJobs(costs, threshold)
#     ids = similar(costs, Int)
#     c = 0.0
#     n = 1
#     for i in eachindex(costs)
#         if c + costs[i] >= threshold
#             if c == 0
#                 ids[i] = n
#             else
#                 n += 1
#                 ids[i] = n
#             end
#             c = costs[i]
#         else
#             c = c + costs[i]
#             ids[i] = n
#         end
#     end
#     ids
# end

function movingAverage(A, n; stride = 1)
    [mean(view(A, max(firstindex(A), i - n) : min(i + n, lastindex(A)))) for i in 1:stride:length(A)]
end

function movingGaussian(A, n; stride = 1, showprogress = false)
    G = sparse([exp(-i^2/(2*n^2)) for i in -length(A):length(A)])
    droptol!(G, 1e-5)
    prog = Progress(length(1:stride:length(A)), enabled = showprogress)
    map(reverse(1:stride:length(A))) do i
        next!(prog)
        inds, vals = findnz(G[i:i+length(A)-1])
        sum(A[inds] .* vals) / sum(vals)
    end
end

function normalprod(d1, d2)
    Normal((d1.μ * d2.σ^2 + d2.μ * d1.σ^2) / (d1.σ^2 + d2.σ^2), sqrt(d1.σ^2 * d2.σ^2 / (d1.σ^2 + d2.σ^2)))
end

function normalprod(d1::MvNormal, d2::MvNormal)
    Σ = (d1.Σ^-1 + d2.Σ^-1)^-1
    MvNormal(Σ * (d1.Σ^-1 * d1.μ + d2.Σ^-1 * d2.μ), Σ)
end

function enumpath(path; sep = "_")
    name, ext = splitext(path)
    i = 1
    while isfile(name * "$sep$i" * ext) 
        i += 1
    end
    return name * "$sep$i" * ext
end

function logsumexp!(M, A, B, W, X)
    maximum!(M, X)
    A .= W .* exp.(X .- M)
    sum!(B, A)
    M .+= log.(B)
    M
end

function duplicateChunks!(A, B, chunksize)
    reshape(A, 2chunksize, :)[1:chunksize, :] .= reshape(B, chunksize, :)
    reshape(A, 2chunksize, :)[chunksize+1:end, :] .= reshape(B, chunksize, :)
    A
end

function reduceChunks!(B, A, chunksize)
    reshape(B, chunksize, :) .= @view reshape(A, 2chunksize, :)[1:chunksize, :]
    reshape(B, chunksize, :) .+= @view reshape(A, 2chunksize, :)[chunksize+1:end, :]
    B
end


function logsumexp(X::AbstractArray{T}, W::AbstractArray; dims=:) where {T<:Real}
    u = reduce(max, X, dims=dims, init=oftype(log(zero(T)), -Inf))
    u isa AbstractArray || isfinite(u) || return float(u)
    
    if u isa AbstractArray
        u .+ log.(sum(W .* exp.(X .- u); dims=dims))
    else
        u + log(sum(((x, w),) -> w * exp(x - u), zip(X, W)))
    end
end

function logsumexp(X::AbstractArray{T}, dims=:) where {T<:Real}
    u = reduce(max, X, dims=dims, init=oftype(log(zero(T)), -Inf))
    u isa AbstractArray || isfinite(u) || return float(u)
    
    if u isa AbstractArray
        u .+ log.(sum(exp.(X .- u); dims=dims))
    else
        u + log(sum(x -> exp(x - u), X))
    end
end

function dihangle(i, j, k, l)
    u1 = j - i
    u2 = k - j
    u3 = l - k
    n = u2 × u3

    atan(norm(u2) * (u1 ⋅ n), u1 × u2 ⋅ n)
end

function dihangled(i, j, k, l)
    u1 = j - i
    u2 = k - j
    u3 = l - k
    n = u2 × u3

    atand(norm(u2) * (u1 ⋅ n), u1 × u2 ⋅ n)
end

function printM(args...; newline = true, clear = false)
    pargs = collect(Iterators.flatten(zip(args, [", " for i in 1:length(args) - 1])))
    push!(pargs, args[end])
    if newline
        println(pargs...)
    else
        print(pargs...)
    end
    # flush(stdout)
end

function printPerLine(args...)
    IJulia.clear_output(true)
    for arg in args
        println(arg)
    end
end

function printProgress(args...)
    IJulia.clear_output(true)
    printM(args...; newline = false)
    flush(stdout)
end


function itp(f, radius, ngrid = 301)
    xs = range(-radius, radius, length = ngrid)
    values = @showprogress [f(SA[x, y, z]) for x in xs, y in xs, z in xs]
    interpolation = CubicSplineInterpolation((xs, xs, xs), values, extrapolation_bc = 0)
    x -> norm(x) > radius ?  0.0 : interpolation(x...)
end

function itpCUDA(f, radius, ngrid = 301)
    xs = range(-radius, radius, length = ngrid)
    values = zeros(ngrid, ngrid, ngrid)
    cuvalues = CUDA.zeros(ngrid, ngrid)
    cuf = cuda(f)
    points = cu([SA[xs[1], y, z] for y in xs, z in xs])
    @showprogress for i in 1:ngrid
        copyto!(points, [SA[xs[i], y, z] for y in xs, z in xs])
        evaluate!(cuvalues, cuf, points)
        values[i,:,:] .= Array(cuvalues)
    end
    interpolation = CubicSplineInterpolation((xs, xs, xs), values, extrapolation_bc = 0)
    function itp(x)::Float64
        norm(x) > radius ?  0.0 : interpolation(x...)
    end
end


function adjacencyMatrix(points, d, sym = false)
    adj = zeros(Bool, length(points), length(points))
    for i in 1:length(points), j in 1:i - 1
        if norm(points[i] - points[j]) < d || (sym && norm(points[i] + points[j]) < d)
            adj[i,j] = adj[j,i] = 1
        end
    end
    adj
end


function triangles(adj)
    triangles = []
    for i in 1:size(adj, 1)
        for j in 1:i
            if adj[i,j]
                ks = findall(adj[:,i] .* adj[:,j])
                for k in ks
                    push!(triangles, [i,j,k])
                end
            end
        end
    end
    sort!.(triangles)
    unique!(triangles)
    triangles
end

function approxUnique(arr, d = 0.001; showprogress = false)
    result = [arr[1]]
    prog = Progress(length(arr) - 1)
    for a in arr[2:end]
        if all(norm.(Ref(a) .- result) .> d)
            push!(result, a)
        end
        showprogress && next!(prog)
    end
    result
end

function approxPush!(arr, e, d = 0.001)
    for i in eachindex(arr)
        if norm(e - arr[i]) <= d
            return i
        end
    end
    push!(arr, e)
    return length(arr)
end

function getIcosphereVerts(nsub = 0, symmetric = false)
    points = SVector{3, Float64}[]
    for s1 in [-1,1], s2 in [-(1 + sqrt(5)) / 2,(1 + sqrt(5)) / 2]
        push!(points, SA[0, s1, s2])
        push!(points, SA[s1, s2, 0])
        push!(points, SA[s2, 0, s1])
    end
    points = LinearAlgebra.normalize.(points)

    adj = adjacencyMatrix(points, 1.1)
    triangles = [points[t] for t in NPhotons.triangles(adj)]

    for n in 1:nsub
        newtriangles = []
        for (a, b, c) in triangles
            ab = normalize(a + b)
            ac = normalize(a + c)
            bc = normalize(b + c)
            append!(points, [ab, ac, bc])
            push!(newtriangles, [a,  ab, ac])
            push!(newtriangles, [b,  ab, bc])
            push!(newtriangles, [c,  ac, bc])
            push!(newtriangles, [ab, ac, bc])
        end
        triangles = newtriangles
    end
    points = approxUnique(points)

    if symmetric
        sympoints = [points[1]]
        for p in points
            d = minimum(norm.(Ref(p) .- sympoints))
            d = min(d, minimum(norm.(Ref(p) .+ sympoints)))
            if d > 0.01 
                push!(sympoints, p)
            end
        end
        return sympoints
    end

    return points
end

function icosahedronVertices()
    points = Vector{Float64}[]
    for s1 in [-1,1], s2 in [-(1 + sqrt(5)) / 2,(1 + sqrt(5)) / 2]
        push!(points, [0, s1, s2])
        push!(points, [s1, s2, 0])
        push!(points, [s2, 0, s1])
    end
    LinearAlgebra.normalize!.(points)
end

function getIcosphereTriangles(nsub = 0, symmetric = false)
    points = icosahedronVertices()
    
    adj = adjacencyMatrix(points, 1.1)
    triangles = [points[t] for t in NPhotons.triangles(adj)]

    for n in 1:nsub
        newtriangles = []
        for (a, b, c) in triangles
            ab = normalize(a + b)
            ac = normalize(a + c)
            bc = normalize(b + c)
            push!(newtriangles, [a,  ab, ac])
            push!(newtriangles, [b,  ab, bc])
            push!(newtriangles, [c,  ac, bc])
            push!(newtriangles, [ab, ac, bc])
        end
        triangles = newtriangles
    end
    return triangles
end

function getIcosphereTriangulation(nsub = 0, symmetric = false)
    points = icosahedronVertices()
    
    adj = adjacencyMatrix(points, 1.1)
    triangles = [points[t]  for t in NPhotons.triangles(adj)]

    for n in 1:nsub
        newtriangles = []
        for (a, b, c) in triangles
            ab = normalize(a + b)
            ac = normalize(a + c)
            bc = normalize(b + c)
            push!(newtriangles, [a,  ab, ac])
            push!(newtriangles, [b,  ab, bc])
            push!(newtriangles, [c,  ac, bc])
            push!(newtriangles, [ab, ac, bc])
        end
        triangles = newtriangles
    end
    return triangles
end

function gridinUnitBall(res, dim = 3)
    xs = [x for x in range(-1, 1, length = res)]
    if dim == 2
        vs = [[x,y] for x in xs, y in xs]
        return vs[norm.(vs) .< 1]
    end
    vs = [[x,y,z] for x in xs, y in xs, z in xs]
    vs[norm.(vs) .< 1]
end

function gridinBall(step, radius)
    rnge = -radius:step:radius
    rnge = rnge .- (rnge[1] + rnge[end]) / 2

    grid = SVector{3,Float64}[]
    for x in rnge, y in rnge, z in rnge
        p = SA[x, y, z]
        if norm(p) < radius
            push!(grid, p)
        end
    end
    grid
end

function tailMultinomial(ps, ks, N)
    result = 1.0
    for n in 1:length(ks)
        for subset in combinations(1:length(ks), n)
            result += (-1)^n * cdfMultinomial(ps[subset], ks[subset], N)
        end
    end
    result
end

# function cdfMultinomial(ps, ks, N)::Float64
#     dist = Multinomial(N, [ps; 1-sum(ps)])
#     result = 0.0
#     for K in Iterators.product([0:k for k in ks]...)
#         result += pdf(dist, [K..., N - sum(K)])
#     end
#     result
# end

function cdfGammaPoisson(k, α, β)
    # p = 1 - 1 / β
    p = β / (1 + β)
    cdf(Binomial(k + α, 1 - p), k)
end

function cdfMultinomial(ps, ks, N)::Float64
    dist = Multinomial(N, [ps; 1 - sum(ps)])
    cdfMultinomial!(dist, [ks; 0], 0, 0, ks, N)
end

function cdfMultinomial!(dist, ks, i, sumks, maxks, N)::Float64
    if i == length(maxks) 
        ks[end] = N - sumks
        return pdf(dist, ks)
    end

    result = 0.0
    for k in 0:maxks[i + 1]
        ks[i + 1] = k
        result += cdfMultinomial!(dist, ks, i + 1, sumks + k, maxks, N)
    end
    result
end

function tailTrinomial(p1, p2, k1, k2, N)
    marginal1 = cdf(Binomial(N, Float64(p1)), k1 - 1)
    marginal2 = cdf(Binomial(N, Float64(p2)), k2 - 1)


    joint = 0.0
    for k in 0:k1 + k2 - 2
        p = pdf(Binomial(N, p1 + p2), k)
        for i in 0:k
            if i < k1 && k - i < k2
                joint += p * pdf(Binomial(k, p1 / (p1 + p2)), i)
            end
        end
    end

    1 - marginal1 - marginal2 + joint
end

function cumulativeMultinomial(p, n, N)
    dist = Multinomial(N, p)
    sum(pdf(dist, collect(k) .- 1) for k in Iterators.product(Base.OneTo.(n)...))
end

function bin(k, n, p)
    binomial(n, k) * p^k * (1 - p)^(n - k)
end


function cumbin(k, n, p)
    k < 0 && return 0.0
    beta_inc(n - k, k + 1, 1 - p)[1]
end


function logbinomial(n, m)
    logn = log(n)
    logm = log(m)
    nm = n - m
    lognm = log(nm)
    n * logn - m * logm - nm * lognm + 0.5 * (logn - logm - lognm - log(2pi))
end

# function angle(v1, v2)::Float64
#     n1 = LinearAlgebra.norm(v1)
#     n2 = LinearAlgebra.norm(v2)
#     if n1 != 0 && n2 != 0
#         return acos((v1 ⋅ v2) / (n1 * n2))
#     end
#     return 0.0
# end

function RegularInterpolation(f; N = 10, radius = 1)
    rnge = range(-radius, radius, length = N)
    data = [f([x, y, z]) for x in rnge, y in rnge, z in rnge]
    itp = LinearInterpolation((rnge, rnge, rnge), data)
    return (x -> itp(x...))
end

function randfromSphere(d)
    while true
        p = rand(d) .* 2 .- 1
        if norm(p) < 1
            return p
        end
    end
end

function circleGrid(N, radius = 1)
    xi = [SA[0.0, 0.0, 0.0]]
    for i in 2:N
        r = 2 / (2N - 1) * (i - 1)# + 0.05
        for a in prange(0, 2pi, 6 * (i - 1))# .+ rand() * 2pi
            push!(xi, SA[r * cos(a), r * sin(a), 0.0])
        end
    end
    xi .*= radius
end

function weightedCircleGrid(density = x -> 1, radius = 1; npercircle = 0)
    radii = [0.0, 0.0]
    r = 0
    while (r += 1 / density(r)) < radius
        push!(radii, r)
    end
    push!(radii, radii[end] + 2 * (radius - radii[end]))
    xi = SVector{3,Float64}[]
    weights = Float64[]

    for i in 2:length(radii) - 1
        r = radii[i]
        n = if npercircle == 0
            n = ceil(Int, 6 * r / radii[3] * density(r) / density(radii[3]))
        else
            npercircle
        end

        append!(xi, [SA[r * cos(a), r * sin(a), 0.0] for a in prange(0, 2pi, n)])# .+ rand()*2pi])

        outer = (radii[i + 1] + r) / 2
        inner = (radii[i - 1] + r) / 2
        append!(weights, [(outer^2 - inner^2) / n for j in 1:n])
    end
    xi, weights * pi
end
weightedCircleGrid(h::Real, σ::Real, radius = 1; min = 1, npercircle = 0) = weightedCircleGrid(x -> h * (exp(-0.5x^2 / σ^2) / σ + min), radius, npercircle = npercircle)

function gaussLegendreEwaldGrid(; qmax, n, npercircle, wavelength)
    r = 2pi / wavelength
    amax = 2asin(min(1, 0.5qmax / r))

    angles, weights = gausslegendre(n)
    angles .= amax .* (0.5 .* angles .+ 0.5)

    pixels = SVector{3,Float64}[]
    pixelWeights = Float64[]

    for (a, w) in zip(angles, weights)
        append!(pixels, [r * SA[sin(a) * cos(φ), sin(a) * sin(φ), cos(a) - 1] for φ in prange(0, 2pi, npercircle)])
        append!(pixelWeights, fill(w * 2pi * r * sin(a) / npercircle, npercircle))
    end
    
    pixelWeights .*= 2pi * r^2 * (1 - cos(amax)) / sum(pixelWeights)

    pixels, pixelWeights
end

function weightedEwaldGrid(; qstep, qmax, npercircle, wavelength, noffsets = 1)
    r = 2pi / wavelength
    amax = 2asin(min(1, 0.5qmax / r))
    astep = qstep / r
    angles = [0.0; 0:astep:amax]
    push!(angles, angles[end] + 2 * (amax - angles[end]))

    pixels = SVector{3,Float64}[]
    weights = Float64[]

    for i in 2:length(angles) - 1
        a = angles[i]
        
        for j in 1:noffsets
            φs = prange(0, 2pi, npercircle); φs = φs .+ (φs[2] - φs[1]) * (j-1) / noffsets
            append!(pixels, [r * SA[sin(a) * cos(φ), sin(a) * sin(φ), cos(a) - 1] for φ in φs])
        end

        outer = (angles[i + 1] + a) / 2
        inner = (angles[i - 1] + a) / 2
        append!(weights, fill(2pi*r^2 * (cos(inner) - cos(outer)) / (npercircle * noffsets), npercircle * noffsets))
    end

    pixels, weights
end

function weightedEwaldGrid2(; nradial, qmax, npercircle, wavelength)
    r = 2pi / wavelength
    amax = 2asin(min(1, 0.5qmax / r))
    angles = range(0, amax, nradial + 1)
    φs = 2pi .* (0.5 / npercircle : 1 / npercircle : 1)
    
    pixels = SVector{3,Float64}[]
    weights = Float64[]

    for i in 1:length(angles) - 1
        inner = angles[i]; outer = angles[i+1]; a = (inner + outer) / 2
        append!(pixels, [r * SA[sin(a) * cos(φ), sin(a) * sin(φ), cos(a) - 1] for φ in φs])
        append!(weights, fill(2pi*r^2 * (cos(inner) - cos(outer)) / npercircle, npercircle))
    end

    pixels, weights
end


function findclosest(xs::Array{Array{Array{Float64,1},1},1}, b::Array{Array{Float64,1},1}; sym = false)
    tree = NearestNeighbors.KDTree(hcat(b...))
    [findclosest(x, tree; sym = false) for x in xs]    
end

function findclosest(x::Array{Array{Float64,1},1}, b::Array{Array{Float64,1},1}; sym = false)
    findclosest(hcat(x...), hcat(b...); sym = false)
end

function findclosest(X::Matrix, B::Matrix; sym = false)
    tree = NearestNeighbors.KDTree(B)
    findclosest(X, tree; sym = false)
end

function findclosest(x::Array{Array{Float64,1},1}, tree::NearestNeighbors.KDTree; sym = false)
    findclosest(hcat(x...), tree; sym = false)
end

function findclosest(X::Matrix, tree::NearestNeighbors.KDTree; sym = false)
    inds, dists = k11(tree, X)
    if sym
        indsSym, distsSym = k11(tree, -X)
        for n in eachindex(indsSym)
            if dists[n] > distsSym[n]
                inds[n] = indsSym[n]
            end 
        end
    end
    inds
end

function k11(tree, X)
    k = NearestNeighbors.knn(tree, X, 1)
    inds = [i[1] for i in k[1]]
    dists = [i[1] for i in k[2]]
    inds, dists
end

function indexProduct!(dest, values, indices::Array)
    dest .= @view values[:,indices[1]]
    for i in 2:length(indices)
        dest .*= @view values[:,indices[i]]
    end
    dest
end

function indexProduct!(dest, values, counts)
    for (j, c) in counts
        dest .*= (@view values[:, j]).^c
    end
    dest
end

function indexProduct(values, indices::Array)
    indexProduct!(ones(size(values, 1)), values, indices)
end

# function pointCloudDistance(X, Y)
#     norm(minimum(norm.(Ref(x) .- Y).^2) for x in X) / length(X) + norm(minimum(norm.(Ref(y) .- X).^2) for y in Y) / length(Y)
# end

function pointCloudDistance(X, Y)
    if isempty(X) || isempty(Y) 
        return NaN
    end
    dists = pairwise(SqEuclidean(), X, Y)
    (sum(minimum(dists, dims = 2)) / length(X) + sum(minimum(dists, dims = 1)) / length(Y)) / 2
end

function pointCloudDistance!(dists, dX, dY, X, Y)
    if isempty(X) || isempty(Y) 
        return NaN
    end
    pairwise!(dists, SqEuclidean(), X, Y)
    (sum(minimum!(dX, dists)) / length(X) + sum(minimum!(dY', dists)) / length(Y)) / 2
end

function pointCloudDistance!(dists, dX, dY, X, Y, hX, hY)
    if isempty(X) || isempty(Y) 
        return NaN
    end
    pairwise!(dists, SqEuclidean(), X, Y)
    dists .+= (hX .- hY').^2
    (sum(minimum!(dX, dists)) / length(X) + sum(minimum!(dY', dists)) / length(Y)) / 2
end

function pointCloudMatch(X, Y)
    dists = pairwise(SqEuclidean(), X, Y)
    [Tuple.(argmin(dists, dims = 1))[:]; Tuple.(argmin(dists, dims = 2))[:]]
end
