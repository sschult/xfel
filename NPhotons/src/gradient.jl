function kernelGradient!(dfvalues, dnphotonLikelihoods, allKernelResults, fvalues, pfactors, imagelocations, images, polarization, background, ioff, chunksize, ::Val{maxo}) where {maxo}
    
    x = (blockIdx().x - one(blockIdx().x)) * blockDim().x + threadIdx().x
    i = (blockIdx().y - one(blockIdx().y)) * blockDim().y + threadIdx().y + ioff
    j = (blockIdx().z - one(blockIdx().z)) * blockDim().z + threadIdx().z

    allKernelResults = CUDA.Const(allKernelResults); 

    fvalues, imagelocations, images, pfactors = CUDA.Const.((fvalues, imagelocations, images, pfactors))
    polarization = isnothing(polarization) ? nothing : CUDA.Const(polarization); 
    background = isnothing(background) ? nothing : CUDA.Const(background)

    start = x + blockDim().x * (threadIdx().y - one(threadIdx().y))
    step = blockDim().x * blockDim().y

    G = CuDynamicSharedArray(eltype(allKernelResults), size(dfvalues, 1))

    k = start
    @inbounds while k <= length(G)
        G[k] = 0
        k += step
    end

    sync_threads()

    @inbounds if i <= length(imagelocations)

        imageindex = imagelocations[i]
        imagelength = images[imageindex]
        l = imagelength + one(imagelength)
        
        dn = zero(eltype(G))

        rs = MVector{maxo + 1, Float32}(undef)
        for o in zero(maxo):maxo
            off = o * chunksize + x
            rs[o + 1] = allKernelResults[off, i, j]
        end

        for k in UInt32(1):imagelength
            p = images[imageindex + k]

            for o in zero(maxo):maxo
                off = o * chunksize + x

                if !isnothing(background)
                    b = background[p, j] / (!isnothing(polarization) ? polarization[p] : 1)
                    CUDA.@atomic G[p + off - UInt32(1)] += rs[o + 1] / (fvalues[p + off - UInt32(1), j] + b)
                else
                    CUDA.@atomic G[p + off - UInt32(1)] += rs[o + 1]
                end
            end
        end

        if ndims(dnphotonLikelihoods) == 3
            for o in zero(maxo):maxo
                CUDA.@atomic dnphotonLikelihoods[l, j, o * chunksize + x] += rs[o + 1]
            end
        else
            for o in zero(maxo):maxo
                dn += rs[o + 1]
            end
            y = warpsum(dn, chunksize ÷ 2, 0, 0x000000FF << 8 * (mod1(i, 4) - 1))
            if x == 1
                CUDA.@atomic dnphotonLikelihoods[l, j] += y
            end
        end
    end
    
    sync_threads()

    k = start
    @inbounds while k <= length(G)
        CUDA.@atomic dfvalues[k, j] += G[k] / (isnothing(background) ? fvalues[k, j] : 1)
        k += step
    end

    return
end

function kernelGradientInt!(dfvalues, dnphotonLikelihoods, allKernelResults, fvalues, pfactors, imagelocations, images, polarization, background, ioff, chunksize, iscale, ::Val{maxo}) where {maxo}
    
    x = (blockIdx().x - one(blockIdx().x)) * blockDim().x + threadIdx().x
    i1 = (blockIdx().y - one(blockIdx().y)) * blockDim().y + threadIdx().y + ioff
    j = (blockIdx().z - one(blockIdx().z)) * blockDim().z + threadIdx().z

    allKernelResults, fvalues, imagelocations, images, pfactors = CUDA.Const.((allKernelResults, fvalues, imagelocations, images, pfactors))
    polarization = isnothing(polarization) ? nothing : CUDA.Const(polarization); 
    background = isnothing(background) ? nothing : CUDA.Const(background)

    start = x + blockDim().x * (threadIdx().y - one(threadIdx().y))
    step = blockDim().x * blockDim().y

    @inbounds begin
        G = CuDynamicSharedArray(UInt32, min(12 * 1024, size(dfvalues, 1)))
        k = start
        while k <= length(G)
            G[k] = 0
            k += step
        end
        sync_threads()
        
        i = i1
        while i <= length(imagelocations)
            imageindex = imagelocations[i]
            imagelength = images[imageindex]
            l = imagelength + one(imagelength)
            
            rs = MVector{maxo + 1, Float32}(undef)
            for o in zero(maxo):maxo
                off = o * chunksize + x
                rs[o + one(o)] = allKernelResults[off, i, j]
            end

            if ndims(dnphotonLikelihoods) == 3
                for o in zero(maxo):maxo
                    CUDA.@atomic dnphotonLikelihoods[l, j, o * chunksize + x] += rs[o + one(o)]
                end
            else
                y = warpsum(sum(rs), chunksize ÷ 2, 0, 0x000000FF << 8 * (mod1(i1, 4) - 1))
                if x == 1
                    CUDA.@atomic dnphotonLikelihoods[l, j] += y
                end
            end

            for k in UInt32(1):imagelength
                p = images[imageindex + k]

                for o in zero(maxo):maxo
                    off = o * chunksize + x

                    if !isnothing(background)
                        b = background[p, j] / (!isnothing(polarization) ? polarization[p] : 1)
                        r = rs[o + one(o)] / (fvalues[p + off - one(off), j] + b)
                    else
                        r = rs[o + one(o)]
                    end

                    k = p + off - UInt32(1)
                    if k <= length(G)
                        CUDA.@atomic G[k] += Base.fptoui(UInt32, r * iscale)
                    else
                        CUDA.@atomic dfvalues[k, j] += r / (isnothing(background) ? fvalues[k, j] : 1)
                    end
                end
            end

            i += gridDim().y * blockDim().y
        end
        
        sync_threads()

        k = start
        while k <= length(G)
            fv = isnothing(background) ? fvalues[k, j] : 1
            CUDA.@atomic dfvalues[k, j] += G[k] / (iscale * fv)
            k += step
        end
    end

    return
end

# function kernelGradient2!(dfvalues, dnphotonLikelihoods, kernelResults, fvalues, pfactors, imagelocations, images, polarization, background, ioff, chunksize, ::Val{maxo}) where {maxo}
    
#     x = (blockIdx().x - one(blockIdx().x)) * blockDim().x + threadIdx().x
#     i = (blockIdx().y - one(blockIdx().y)) * blockDim().y + threadIdx().y + ioff
#     j = (blockIdx().z - one(blockIdx().z)) * blockDim().z + threadIdx().z

#     wi = mod1((threadIdx().x + blockDim().x * (threadIdx().y - one(threadIdx().y))), warpsize())

#     kernelResults = CUDA.Const(kernelResults); 

#     fvalues, imagelocations, images, pfactors = CUDA.Const.((fvalues, imagelocations, images, pfactors))
#     polarization = isnothing(polarization) ? nothing : CUDA.Const(polarization); 
#     background = isnothing(background) ? nothing : CUDA.Const(background)

#     start = x + blockDim().x * (threadIdx().y - one(threadIdx().y))
#     step = blockDim().x * blockDim().y

#     G = CuDynamicSharedArray(Float32, size(dfvalues, 1))
#     locks = CuDynamicSharedArray(Int32, blockDim().x * (length(G) ÷ 128), 4 * length(G))

#     k = start
#     @inbounds while k <= length(G)
#         G[k] = 0
#         k += step
#     end

#     k = start
#     @inbounds while k <= length(locks)
#         locks[k] = 0
#         k += step
#     end

#     sync_threads()

#     @inbounds @fastmath if i <= length(imagelocations)
#         imageindex = imagelocations[i]
#         imagelength = images[imageindex]
#         l = imagelength + one(imagelength)
        
#         dn = zero(eltype(G))

#         rs = MVector{maxo + 1, Float32}(undef)
#         for o in zero(maxo):maxo
#             off = o * chunksize + x
#             rs[o + 1] = kernelResults[off, i, j]
#         end

#         for k in UInt32(1):imagelength
#             p = images[imageindex + k]
#             li = (fld1(p, 128) - 1) * blockDim().x + x

#             leave = false
#             while !leave
#                 if CUDA.atomic_cas!(pointer(locks, li), Int32(0), Int32(1)) == 0
#                     for o in zero(maxo):maxo
#                         off = o * chunksize + x
#                         G[p + off - UInt32(1)] += rs[o + 1]
#                     end

#                     locks[li] = 0
#                     leave = true
#                 end
#             end
#         end
#     end
    
#     sync_threads()

#     @inbounds @fastmath if i <= length(imagelocations)
#         if ndims(dnphotonLikelihoods) == 3
#             for o in zero(maxo):maxo
#                 CUDA.@atomic dnphotonLikelihoods[l, j, o * chunksize + x] += rs[o + 1]
#             end
#         else
#             for o in zero(maxo):maxo
#                 dn += rs[o + 1]
#             end
#             y = warpsum(dn, chunksize ÷ 2)
#             if x == 1
#                 CUDA.@atomic dnphotonLikelihoods[l, j] += y
#             end
#         end
#     end
    
#     sync_threads()

#     k = start
#     @inbounds while k <= length(G)
#         CUDA.@atomic dfvalues[k, j] += G[k] / (isnothing(background) ? fvalues[k, j] : 1)
#         k += step
#     end

#     return
# end

# function kernelGradient3!(dfvalues, dnphotonLikelihoods, kernelResults, fvalues, pfactors, imagelocations, images, polarization, background, ioff, chunksize, ::Val{maxo}) where {maxo}
    
#     x = (blockIdx().x - one(blockIdx().x)) * blockDim().x + threadIdx().x
#     i = (blockIdx().y - one(blockIdx().y)) * blockDim().y + threadIdx().y + ioff
#     j = (blockIdx().z - one(blockIdx().z)) * blockDim().z + threadIdx().z

#     wi = mod1((threadIdx().x + blockDim().x * (threadIdx().y - one(threadIdx().y))), warpsize())

#     kernelResults = CUDA.Const(kernelResults); 

#     fvalues, imagelocations, images, pfactors = CUDA.Const.((fvalues, imagelocations, images, pfactors))
#     polarization = isnothing(polarization) ? nothing : CUDA.Const(polarization); 
#     background = isnothing(background) ? nothing : CUDA.Const(background)

#     start = x + blockDim().x * (threadIdx().y - one(threadIdx().y))
#     step = blockDim().x * blockDim().y

#     G = CuDynamicSharedArray(Float32, size(dfvalues, 1))
#     locks = CuDynamicSharedArray(Int32, length(G) ÷ 128, 4 * length(G))

#     k = start
#     @inbounds while k <= length(G)
#         G[k] = 0
#         k += step
#     end

#     # k = start
#     # @inbounds while k <= length(locks)
#     #     locks[k] = 0
#     #     k += step
#     # end

#     sync_threads()

#     @inbounds @fastmath if i <= length(imagelocations)
#         imageindex = imagelocations[i]
#         imagelength = images[imageindex]
#         l = imagelength + one(imagelength)
        
#         dn = zero(eltype(G))

#         rs = MVector{maxo + 1, Float32}(undef)
#         for o in zero(maxo):maxo
#             off = o * chunksize + x
#             rs[o + 1] = kernelResults[off, i, j]
#         end

#         for k in UInt32(1):imagelength
#             p = images[imageindex + k]
#             # li = fld1(p, 128)

#             leave = false
#             while !leave
#                 old = G[p + x - UInt32(1)]
#                 new = old + rs[1]

#                 if CUDA.atomic_cas!(pointer(G, p + x - UInt32(1)), old, new) == old
#                     for o in one(maxo):maxo
#                         off = o * chunksize + x
#                         G[p + off - UInt32(1)] += rs[o + 1]
#                     end
#                     leave = true
#                 end
#             end
#         end

#         if ndims(dnphotonLikelihoods) == 3
#             for o in zero(maxo):maxo
#                 CUDA.@atomic dnphotonLikelihoods[l, j, o * chunksize + x] += rs[o + 1]
#             end
#         else
#             for o in zero(maxo):maxo
#                 dn += rs[o + 1]
#             end
#             y = warpsum(dn, chunksize ÷ 2)
#             if x == 1
#                 CUDA.@atomic dnphotonLikelihoods[l, j] += y
#             end
#         end
#     end
    
#     sync_threads()

#     k = start
#     @inbounds while k <= length(G)
#         CUDA.@atomic dfvalues[k, j] += G[k] / (isnothing(background) ? fvalues[k, j] : 1)
#         k += step
#     end

#     return
# end

function applyKernelGradient!(dfvalues, dnphotonLikelihoods, kernelResults, allKernelResults, fvalues::CuArray, pfactors, imagelocations, images, likelihoods, ::Val{npercircle}, polarization = nothing, background = nothing) where {npercircle}
    chunksize = min(8, npercircle)
    dimy = 768 ÷ chunksize
    maxo = Val(Int32((npercircle - 1) ÷ chunksize))

    # for ir in partitionrange(1 : ceil(Int, length(imagelocations) / dimy), maxl = 65535)
    #     blocks = (1, length(ir), size(allKernelResults, 3))
    #     threads = (chunksize, dimy, 1)
    #     shmem = sizeof(eltype(allKernelResults)) * size(dfvalues, 1)
    #     ioff = UInt32((minimum(ir) - 1) * dimy)
    #     @cuda name="grad_kernel" threads=threads blocks=blocks shmem=shmem fastmath=true maxregs=40 kernelGradient!(dfvalues, dnphotonLikelihoods, allKernelResults, fvalues, pfactors, imagelocations, images, polarization, background, ioff, Int32(chunksize), maxo)
    # end

    iscale = Float32(typemax(UInt32) / (maximum(view(kernelResults, 1:length(likelihoods), :) ./ likelihoods)) / (dimy * 10))
    
    threads = (chunksize, dimy, 1)
    blocks = (1, max(1, length(imagelocations) ÷ 10 ÷ dimy), size(allKernelResults, 3))
    shmem = min(48 * 1024, 4 * size(dfvalues, 1))
    @cuda name="grad_kernel2" threads=threads blocks=blocks shmem=shmem fastmath=true maxregs=40 kernelGradientInt!(dfvalues, dnphotonLikelihoods, allKernelResults, fvalues, pfactors, imagelocations, images, polarization, background, 0x0, Int32(chunksize), iscale, maxo)
end


mutable struct GradientAllocation{A1, F1, F2, F3, F4, L1, L2}
    alloc::A1

    dfvaluesDuplicated::F1
    dfvalues::F2

    fvaluesDuplicated::F3
    bvaluesDuplicated::F4

    dnphotonLikelihoods::L1
    dnphotonsPerRotation::L2
end

function GradientAllocation(alloc::ComputeAllocation)
    conf = alloc.configuration
    
    # @assert conf.ncalls == 1
    @assert !isnothing(alloc.allKernelResults)

    dfvaluesDuplicated = similar(alloc.fvaluesDuplicated)
    dfvalues = similar(alloc.fvalues)

    fvaluesDuplicated = similar(alloc.fvaluesDuplicated)
    bvaluesDuplicated = !isnothing(alloc.bvaluesDuplicated) ? similar(alloc.bvaluesDuplicated) : nothing

    dnphotonLikelihoods = similar(alloc.nphotonLikelihoods)
    dnphotonsPerRotation = similar(alloc.nphotonsPerRotation)

    GradientAllocation(alloc, dfvaluesDuplicated, dfvalues, fvaluesDuplicated, bvaluesDuplicated, dnphotonLikelihoods, dnphotonsPerRotation)
end

function compute_dfvalues!(grad::GradientAllocation, model::ForwardModel; slotweights = [1.0])
    alloc = grad.alloc
    npercircle = grad.alloc.configuration.params.npercircle
    
    @tc "setup" begin
        grad.dfvaluesDuplicated .= 0
        grad.dfvalues .= 0
        grad.dnphotonLikelihoods .= 0
        grad.dnphotonsPerRotation .= 0

        duplicateChunks!(grad.fvaluesDuplicated, alloc.fvalues, npercircle)
        !isnothing(alloc.bvalues) && duplicateChunks!(@view(grad.bvaluesDuplicated[:, 1]), alloc.bvalues, npercircle)
        !isnothing(alloc.bvalues) && (grad.bvaluesDuplicated .= @view grad.bvaluesDuplicated[:, 1:1])
    end

    function pullback(alloc, imagerange)
        nimages = length(alloc.indexImages)
        nslots = length(slotweights)
        likelihoods = reshape(view(alloc.likelihoods, 1 : nslots * nimages), nimages, nslots) * cu(slotweights)
        view(alloc.allKernelResults, :, 1:length(imagerange), :) ./= reshape(view(likelihoods, imagerange), 1, :)


        @tc "kernel_grad" applyKernelGradient!(grad.dfvaluesDuplicated, grad.dnphotonLikelihoods, alloc.kernelResults, alloc.allKernelResults, grad.fvaluesDuplicated, alloc.nphotonLikelihoods, view(alloc.indexImages.locations, imagerange), alloc.indexImages.indices, view(likelihoods, imagerange), alloc.npercircle, alloc.polarizationFactorsDuplicated, grad.bvaluesDuplicated)
    end

    @tc "compute" logp = sum(compute_imagelikelihoods!(grad.alloc, grad.alloc.forwardmodel, afterkernel = pullback), init = 0.0)

    @tc "nphoton" begin
        reduceChunks!(grad.dfvalues, grad.dfvaluesDuplicated, npercircle)

        l = 0 : size(grad.dnphotonLikelihoods, 1) - 1
        rnPR = reshape(alloc.nphotonsPerRotation, 1, size(alloc.nphotonsPerRotation)...)
        if model.intensitydist isa Tuple{Real}
            λ = grad.alloc.forwardmodel.meanintensity * mean(grad.alloc.forwardmodel.intensitydist)
            grad.dnphotonLikelihoods .*= -λ
        elseif model.intensitydist isa Gamma
            α = model.intensitydist.α
            β = 1 / model.intensitydist.θ / model.meanintensity
            grad.dnphotonLikelihoods .*= (-α .- l) ./ (rnPR .+ β)
        elseif isnothing(model.intensitydist)
            rnPR = reshape(alloc.nphotonsPerRotation, 1, size(alloc.nphotonsPerRotation)...)
            grad.dnphotonLikelihoods .= .- l ./ rnPR
        end
        sum!(reshape(grad.dnphotonsPerRotation, 1, :), reshape(grad.dnphotonLikelihoods, size(grad.dnphotonLikelihoods, 1), :))

        if isnothing(grad.alloc.premulPixelWeights)
            grad.dfvalues .+= grad.alloc.pixelWeights .* reshape(grad.dnphotonsPerRotation, 1, size(grad.dnphotonsPerRotation)...)
        else
            for p in 1:npercircle
                pw = view(grad.alloc.premulPixelWeights, :, p)
                dn = view(grad.dnphotonsPerRotation, :, p)
                grad.dfvalues .+= pw .* reshape(dn, 1, size(dn)...)
            end
        end

        grad.dfvalues .*= grad.alloc.rotationWeights'
    end

    logp, grad.dfvalues
end

function radreg!(grad::GradientAllocation, strength)
    alloc = grad.alloc
    Y = alloc.meanfvaluesDuplicated[2:end]
    dY = zero(Y)
    
    broadcast(alloc.indexImages.locations, Ref(alloc.indexImages.indices), Ref(dY)) do l, inds, dY
        for i in 1:inds[l]
            CUDA.@atomic dY[inds[l+i]] += 1
        end
    end
    dY ./= Y

    X = alloc.fvalues * alloc.rotationWeights
    λ = alloc.forwardmodel.meanintensity * mean(alloc.forwardmodel.intensitydist)

    df = reshape(dY, 2alloc.configuration.params.npercircle, :)[1:alloc.configuration.params.npercircle, :]
    df .= mean(df, dims = 1)
    df .-= λ .* reshape(length(alloc.indexImages) .* alloc.pixelWeights, size(df))

    grad.dfvalues .-= strength .* reshape(df, :) .* alloc.rotationWeights'
end

function compute_gradient!(grad::GradientAllocation, f, model::ForwardModel; η = 1, radreg = nothing, kwargs...)
    @tc "intensities" compute_intensities!(grad.alloc, f, model)
    @tc "dfvalues" logp, dfvalues = compute_dfvalues!(grad, model; kwargs...)
    
    if !isnothing(radreg)
        @tc "radreg" begin
            radreg!(grad, radreg)
            logp -= radreg * radial_loglikelihood(grad.alloc)
        end
    end

    @tc "torealspace" g = η * pullback(f, grad.alloc.rotatedPixels, dfvalues)
    
    logp, g
end

function (grad::GradientAllocation)(f::Structure, model::ForwardModel{<:Any, <:Any, <:Any}; kwargs...)
    # @tc "logp" logp = grad.alloc(f, model)
    compute_gradient!(grad, f, model; kwargs...)
end

function (grad::GradientAllocation)(f::Structure, model::ForwardModel{<:Any, <:Any, <:Distribution})
    alloc = grad.alloc
    @tc "logp" logp = grad.alloc(f, model)

    ηs, ws = gethitquadrature(model, alloc.configuration.nhitvalues)

    cf = iscuda(alloc) ? cuda(f, eltype(alloc.fvalues)) : f
    cm = iscuda(alloc) ? cuda(model, eltype(alloc.fvalues)) : model

    @tc "grad" g = sum(enumerate(zip(ηs, ws))) do (i, (η, w))
        @tc "intensities" compute_intensities!(alloc, cf, cm, η)
        @tc "likelihoods" compute_imagelikelihoods!(alloc, f, model; 
            results_slot = i, η = η, recompute_meanfvalues = false, usemaxnphotonLikelihoods = true)
        @tc "gradient" w * compute_gradient!(grad, f, model; η = η, slotweights = ws)
    end
    unsafe_free!(cf)
    hasnoise(cm) && unsafe_free!(cm)

    logp, g
end

function (grad::GradientAllocation)(f::Structure; kwargs...)
    grad(f, grad.alloc.forwardmodel; kwargs...)
end

function setqmax!(grad::GradientAllocation, args...; kwargs...)
    setqmax!(grad.alloc, args...; kwargs...)
end

function randomizeImages!(grad::GradientAllocation, args...; kwargs...)
    randomizeImages!(grad.alloc, args...; kwargs...)
end

function smoothen!(grad::GradientAllocation, args...; kwargs...)
    smoothen!(grad.alloc, args...; kwargs...)
end

function filterImages!(grad::GradientAllocation, args...; kwargs...)
    filterImages!(grad.alloc, args...; kwargs...)
end