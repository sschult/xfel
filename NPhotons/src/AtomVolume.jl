import Base.length, Base.getindex

abstract type Structure end

struct AtomVolume{T, A <: AbstractVector{<:SVector{3, <:AbstractFloat}}, B <: AbstractVector{<:AbstractFloat}} <: Structure
    positions::A
    heights::B
    widths::B

    function AtomVolume(positions, heights, widths)
        new{eltype(eltype(positions)), typeof(positions), typeof(widths)}(positions, heights, widths)
    end
end

function AtomVolume(positions, heights, widths, radius)
    AtomVolume(positions, heights, widths)
end

function Base.zero(::AtomVolume{T, A, B}) where {T, A, B}
    AtomVolume(A(), B(), B())
end

function emptyAtomVolume()
    AtomVolume(SVector{3,Float64}[], Float64[], Float64[])
end

function AtomVolume(N::Int, σ::Real)
    AtomVolume(fill(SA[0.0, 0.0, 0.0], N), σ)
end

function AtomVolume(positions::AbstractVector{<:AbstractVector}, σ::Real)
    numtype = eltype(eltype(positions))
    AtomVolume(convert.(SVector{3, numtype}, positions), ones(length(positions)), fill(Float64(σ), length(positions)))
end

function Base.repeat(f::AtomVolume; kwargs...)
    AtomVolume(repeat(f.positions; kwargs...), repeat(f.heights; kwargs...), repeat(f.widths; kwargs...))    
end

function randomize(f::AtomVolume)
    s = norm(std(f.positions)) / 1.59
    newf = deepcopy(f)
    newf.positions .= s .* randn.(eltype(newf.positions))
    newf
end

function randomAtomVolume(N::Int, σ::Real, radius; sum = 1.0)
    positions = [SVector{3}(randfromSphere(3)) * radius for i in 1:N]
    positions .-= Ref(mean(positions))
    AtomVolume(positions, σ) * (sum / N)
end

getpositions(f::AtomVolume) = f.positions
getheights(f::AtomVolume) = f.heights
getwidths(f::AtomVolume) = f.widths

function Base.:(==)(f::AtomVolume, g::AtomVolume)
    f.positions == g.positions && f.heights == g.heights && f.widths == g.widths
end

function Base.isequal(f::AtomVolume, g::AtomVolume)
    isequal(f.positions, g.positions) && isequal(f.heights, g.heights) && isequal(f.widths, g.widths)
end

function Base.hash(f::AtomVolume, h::UInt)
    hash(f.positions, hash(f.heights, hash(f.widths, hash(:AtomVolume, h))))
end

function Base.convert(::Type{AtomVolume{T, A, B}}, f::AtomVolume) where {T, A, B}
    AtomVolume(convert(A, f.positions), convert(B, f.heights), convert(B, f.widths))
end

function changePrecision(f::AtomVolume, T::Type)
    AtomVolume(SVector{3, T}.(f.positions), T.(f.heights), T.(f.widths))
end

function cuda(f::AtomVolume, T = Float32)
    cupositions = convert(CuArray{SVector{3,T}}, f.positions)
    cuheights = convert(CuArray{T}, f.heights)
    cuwidths = convert(CuArray{T}, f.widths)
    cuf = AtomVolume(cupositions, cuheights, cuwidths)
end

function CUDA.Adapt.adapt_structure(to::CUDA.CuArrayKernelAdaptor, f::AtomVolume)
    f.positions isa CuArray && return f
    positions = CUDA.Adapt.adapt_structure(to, SVector{3, Float32}.(f.positions))
    heights = CUDA.Adapt.adapt_structure(to, f.heights)
    widths = CUDA.Adapt.adapt_structure(to, f.widths)
    AtomVolume(positions, heights, widths)
end

function CUDA.Adapt.adapt_structure(to, f::AtomVolume)
    positions = CUDA.Adapt.adapt_structure(to, f.positions)
    heights = CUDA.Adapt.adapt_structure(to, f.heights)
    widths = CUDA.Adapt.adapt_structure(to, f.widths)
    AtomVolume(positions, heights, widths)
end

function unsafe_free!(f::AtomVolume)
    f.positions isa CuArray && CUDA.unsafe_free!(f.positions)
    f.heights isa CuArray && CUDA.unsafe_free!(f.heights)
    f.widths isa CuArray && CUDA.unsafe_free!(f.widths)
end

function getindex(f::AtomVolume, i::Int)
    f.positions[i]
end

function getindex(f::AtomVolume, what::Symbol)
    x -> f(x, what)
end

function Base.iterate(f::AtomVolume, state = 0)
    i = state + 1
    if i <= length(f)
        ((f.positions[i], f.heights[i], f.widths[i]), i)
    else
        nothing
    end
end

function Base.eltype(f::AtomVolume)
    Tuple{eltype(f.positions), eltype(f.heights), eltype(f.widths)}
end

function evaluateAtomVolume(positions, heights::AbstractArray{T}, widths, k, func = abs2)::T where T
    abs2(evaluateAtomVolumeComplex(positions, heights, widths, k))
end

function evaluateAtomVolumeComplex(positions, heights::AbstractArray{T}, widths, k)::Complex{T} where T
    result = zero(Complex{T})
    kk = dot(k, k)
    for (y, h, σ) in zip(positions, heights, widths)
        @fastmath result += h * exp(complex(-σ^2 * kk / 2, k ⋅ y))
    end
    result
end

function evaluate(f::AtomVolume, P)
    evaluateAtomVolume.(Ref(f.positions), Ref(f.heights), Ref(f.widths), P)
end

function evaluate!(X, f::AtomVolume, P)
    X .= evaluateAtomVolume.(Ref(f.positions), Ref(f.heights), Ref(f.widths), P)
end

function evaluate_complex(f::AtomVolume, P)
    evaluateAtomVolumeComplex.(Ref(f.positions), Ref(f.heights), Ref(f.widths), P)
end

function evaluate_complex!(X, f::AtomVolume, P)
    X .= evaluateAtomVolumeComplex.(Ref(f.positions), Ref(f.heights), Ref(f.widths), P)
end

function (f::AtomVolume)(x, y, z, args...; kwargs...)
    f(SA[x, y, z], args...; kwargs...)
end

function (f::AtomVolume)(x)
    evaluateAtomVolume(f.positions, f.heights, f.widths, x)
end

function (f::AtomVolume)(x, what; cutoff = Inf)
    if what == :real
        result = 0.0
        if cutoff == 0
            for (p, h, σ) in zip(f.positions, f.heights, f.widths)
                result += h * σ^-3 * exp(-1 / (2σ^2) * sqeuclidean(x, p))
            end
        else
            for (p, h, σ) in zip(f.positions, f.heights, f.widths)
                b = 1 / (2σ^2) * sqeuclidean(x, p)
                if b < cutoff
                    result += h * σ^-3 * (exp(-b) - exp(-cutoff))
                end
            end
        end
        return result / (2pi)^(3 / 2)
    elseif what == :fourier
        return f(x)
    elseif what == :complex
        return evaluateAtomVolumeComplex(f.positions, f.heights, f.widths, x)
    else
        zero(eltype(f.heights)) / zero(eltype(f.heights))
    end
end

function realspace(f::AtomVolume; cutoff = Inf)
    x -> f(x, :real; cutoff = Inf)
end

function evaluateRealFast(f, grid; cutoff = 10, showprogress = true)
    tree = KDTree(f.positions)
    prog = Progress(length(grid), enabled = showprogress)
    map(grid) do x
        next!(prog)
        sum(inrange(tree, x, cutoff); init = 0.0) do i
            f.heights[i] * f.widths[i]^-3 * exp(-1 / (2f.widths[i]^2) * sqeuclidean(x, f.positions[i]))
        end / (2pi)^(3 / 2)
    end
end

function evaluateRealCUDA(f::AtomVolume, r; ngrid = 100, cutoff = 10, showprogress = true)
    r = Float32(r)
    edges = range(-r, r, length = max(2, floor(Int, 2r/cutoff)))
    ncell = length(edges) - 1

    cells = [Tuple{SVector{3, Float32}, Float32, Float32}[] for _ in 1:ncell+2, _ in 1:ncell+2, _ in 1:ncell+2]
    sizehint!.(cells, 1000)

    for (y, h, σ) in f
        i, j, k = floorrange.(y, (edges,))
        for oi in -1:1, oj in -1:1, ok in -1:1
            push!(cells[i + oi + 1, j + oj + 1, k + ok + 1], (Float32.(y), Float32(h), Float32(σ)))
        end
    end
    cells = cells[2:end-1, 2:end-1, 2:end-1]
    
    positions = fill(SA[Inf32, Inf32, Inf32], maximum(length, cells), ncell, ncell, ncell)
    heights = fill(0f0, maximum(length, cells), ncell, ncell, ncell)
    widths = fill(1f0, maximum(length, cells), ncell, ncell, ncell)
    for I in CartesianIndices(cells)
        for (i, (y, h, σ)) in enumerate(cells[I])
            positions[i, I] = y
            heights[i, I] = h
            widths[i, I] = σ
        end
    end
    coeffs1 = heights ./ widths.^3
    coeffs2 = -0.5f0 ./ widths.^2

    pxs = range(-r, r, length = ngrid)
    pys = reshape(pxs, 1, :)
    pzs = reshape(pxs, 1, 1, :)
    cis1 = cu(floorrange.(pxs, (edges,)))
    cis2 = reshape(cis1, 1, :)
    cis3 = reshape(cis1, 1, 1, :)

    X = broadcast(pxs, pys, pzs, cis1, cis2, cis3, Ref(cu(positions)), Ref(cu(coeffs1)), Ref(cu(coeffs2))) do px, py, pz, i1, i2, i3, positions, coeffs1, coeffs2
        x = 0.0f0
        p = SA[px, py, pz]
        for i in 1:size(positions, 1)
            y = positions[i, i1, i2, i3]; c1 = coeffs1[i, i1, i2, i3]; c2 = coeffs2[i, i1, i2, i3]
            v = p - y
            x += c1 * exp(c2 * dot(v, v))
        end
        Float64(x) / (2pi)^(3/2)
    end
    range(-1.0r, 1.0r, length = ngrid), Array(X)
end

function l1(f::AtomVolume)
    sum(f.heights)
end

function LinearAlgebra.normalize(f::AtomVolume)
    f / l1(f)
end

function LinearAlgebra.normalize!(f::AtomVolume)
    f.heights ./= l1(f)
    f
end

function centerofmass(f::AtomVolume)
    mean(f.positions, Weights(f.heights))
end

function recenter!(f::AtomVolume)
    f.positions .-= (centerofmass(f),)
    f
end

function radialAverage(f::AtomVolume, r)
    sum(broadcast(f.positions, f.heights, f.widths, reshape.((f.positions, f.heights, f.widths), 1, :)...) do p1, h1, σ1, p2, h2, σ2
        h1 * h2 * sinc(r * norm(p1 - p2) / pi) * exp(-(σ1^2 + σ2^2) * r^2 / 2)
    end)
end

function radialAverage(f::AtomVolume, rs::AbstractArray)
    X = broadcast(f.positions, f.heights, f.widths, reshape.((f.positions, f.heights, f.widths), 1, :)..., reshape(rs, 1, 1, :)) do p1, h1, σ1, p2, h2, σ2, r
        h1 * h2 * sinc(r * norm(p1 - p2) / pi) * exp(-(σ1^2 + σ2^2) * r^2 / 2)
    end
    reshape(sum(X, dims = (1,2)), :)
end


function scatteringProbability(f::AtomVolume; λ = 2.5, qmax = Inf)
    result = 0.0
    r = min(2pi / λ, qmax / 2)
    for (p1, h1, σ1) in zip(f.positions, f.heights, f.widths)
        for (p2, h2, σ2) in zip(f.positions, f.heights, f.widths)
            k = norm(p1 - p2)
            s = sqrt(σ1^2 + σ2^2)
            if k <= 1e-5
                result += h1 * h2 * (1 - exp(-2*r^2*s^2)) / s^2
            else
                x = 2dawson(k / (√2 * s))
                x -= dawson((k - 2im * s^2 * r) / (√2 * s)) * exp(((k - 2im * s^2 * r)^2 - k^2) / (2s^2))
                x -= dawson((k + 2im * s^2 * r) / (√2 * s)) * exp(((k + 2im * s^2 * r)^2 - k^2) / (2s^2))
                result += real(h1 * h2 * x * sqrt(2) / (2k * s))
            end
        end
    end
    result * 2pi
end

function normalizeScattering(f::AtomVolume; nphotons = 1, λ = 2.5, qmax = Inf)
    f * sqrt(nphotons / scatteringProbability(f, λ = λ, qmax = qmax))
end


function envelope(f::AtomVolume)
    o = SA[0.0,0.0,0.0]
    e = AtomVolume(SA[o], SA[1.0], SA[minimum(f.widths)])
    envf = @set e.heights = SA[sqrt(f(o) / e(o))]
    r -> envf(SA[r,0.0,0.0])
end

function normalEnvelope(f::AtomVolume; dims = 3)
    MvNormal(fill(0.0, dims), 1 / (√2 * minimum(f.widths)))
end

function upperBound(f::AtomVolume, r)
    result = 0.0
    a = r^2 / 2
    for (h, σ) in zip(f.heights, f.widths)
        result += h * exp(-σ^2 * a)
    end
    abs2(result)
end

function smoothen(f::AtomVolume, σ)
    newf = deepcopy(f)
    newf.widths .= sqrt.(σ^2 .+ f.widths.^2);
    newf
end

function sharpen(f::AtomVolume, σ)
    newf = deepcopy(f)
    newf.widths .= sqrt.(f.widths.^2 .- σ^2);
    newf
end

function length(f::AtomVolume)
    return length(f.positions)
end

Base.broadcastable(f::AtomVolume) = Ref(f)

function Base.show(io::IO, cf::AtomVolume)
    compact = get(io, :compact, false)
    f = AtomVolume(convert(Array, cf.positions), convert(Array, cf.heights), convert(Array, cf.widths))
    println(io, "$(length(f))-element $(typeof(cf)):")
    println(io, "    Positions: ", string((p -> round.(p, digits = 3)).(f.positions[1:min(2,end)]))[1:end-1], ", ... ]")
    println(io, "    Heights: ", string(round.(f.heights[1:(min(6,end))], digits = 3))[1:end-1], ", ... ]")
    print(io, "    Widths: ", string(round.(f.widths[1:(min(6,end))], digits = 3))[1:end-1], ", ... ]")
end

function Base.:+(f::AtomVolume, g::AtomVolume)
    AtomVolume(vcat(f.positions, g.positions), vcat(f.heights, g.heights), vcat(f.widths, g.widths))
end

function Base.:-(f::AtomVolume, g::AtomVolume)
    f + (-1 * g)
end

moveAtoms!(f::AtomVolume, X) = (f.positions .+= X; f)
moveAtoms(f::AtomVolume, X) = moveAtoms!(deepcopy(f), X)

function rotate(f::AtomVolume, R)
    newf = deepcopy(f)
    for i in eachindex(f.positions)
        newf.positions[i] = R * f.positions[i]
    end
    newf
end

Base.:*(R, f::AtomVolume) = rotate(f, R)
Base.:*(a::Real, f::AtomVolume) = (g = deepcopy(f); g.heights .*= a; g)
Base.:*(f::AtomVolume, a::Real) = a * f
Base.:/(f::AtomVolume, a::Real) = a^-1 * f

function align(f::AtomVolume, g::AtomVolume; usecuda = true, kwargs...)
    if usecuda
        align_pointclouds_gpu(f.positions, g.positions; kwargs...)
    else
        align_pointclouds_cpu(f.positions, g.positions, f.heights, g.heights; kwargs...)
    end
end

function aligned(f::AtomVolume, g::AtomVolume; kwargs...)
    newf = deepcopy(f)
    newf.positions .-= Ref(mean(newf.positions, Weights(newf.heights)))
    align(newf, g; kwargs...) * newf
end


function alignReal(f::AtomVolume, g::AtomVolume; kwargs...)
    radius = 1.2maximum(maximum.(norm, (f.positions, g.positions)))
    align(realspace(f), realspace(g); radius = radius, kwargs...)
end

function alignedReal(f::AtomVolume, g::AtomVolume; kwargs...)
    newf = deepcopy(f)
    newf.positions .-= Ref(mean(newf.positions, Weights(newf.heights)))
    alignReal(newf, g; kwargs...) * newf
end


function enforceAtoms(f, object)
    assigned = zeros(Bool, length(f))
    minh, maxh = extrema(object.heights)
    minw, maxw = extrema(object.widths)
    newf = deepcopy(f)
    for i in sortperm(object.heights, rev = true)
        h = object.heights[i]; w = object.widths[i]
        j = argmin((f.heights .- h).^2 ./ (maxh - minh)^2 .+ (f.widths .- w).^2 ./ (maxw - minw)^2 .+ ifelse.(assigned, Inf, 0))
        newf.heights[j] = h
        newf.widths[j] = w
        assigned[j] = true
    end
    newf
end