const timer = TimerOutput()

macro tc(name, ex)
    quote 
        @timeit_debug timer $name begin
            local ret = $(esc(ex))
            NPhotons.timeit_debug_enabled() && CUDA.synchronize(; blocking = false)
            ret
        end
    end
end

macro t(name, ex)
    :(@timeit_debug timer $name $(esc(ex)))
end

reset_timer!() = TimerOutputs.reset_timer!(timer)

function print_timer(args...; color = true, kwargs...)
    io = IOBuffer()
    show(io, timer, allocations = false, sortby = :firstexec)
    s = read(seekstart(io), String)
    if color
        ls = split(s, "\n")
        println.(ls[1:2])
        println(ls[4])
        # printstyled(ls[4], color = 1); println()
        println.(ls[6:7])
        for l in ls[8:end-1]
            i = findfirst(!=(' '), l)
            if isnothing(i)
                println(l)
            else
                printstyled(l, color = i ÷ 2 + 1); println()
            end
        end
        println(ls[end])
    else
        println(s)
    end
end

enable_timings() = TimerOutputs.enable_debug_timings(NPhotons)
disable_timings() = TimerOutputs.disable_debug_timings(NPhotons)

macro timed(ex)
    :(reset_timer!(); x = $(esc(ex)); print_timer(); x)
end
