
function fsc_cpu(f1::AtomVolume, f2::AtomVolume, radius::Real, points, weights)
    fsc_cpu(f1[:complex], f2[:complex], radius, points, weights)
end

function fsc_cpu(y1::AbstractArray, y2::AbstractArray, weights::AbstractArray)
    A = Diagonal(weights)
    real(dot(y1, A, y2) / sqrt(dot(y1, A, y1) * dot(y2, A, y2)))
end

function fsc_cpu(f1, f2, radius::Real, points, weights)
    fsc_cpu(f1.(radius .* points), f2.(radius .* points), weights)
end

function fsc_cpu(f1, f2, radius::Real; precision = 41)
    points, weights = lebedevGrid(precision = precision)
    fsc_cpu(f1, f2, radius, points, weights)
end

function fsc_cpu(f1, f2, radii; precision = 41, showprogress = false)
    points, weights = lebedevGrid(precision = precision)
    
    prog = Progress(length(radii), enabled = showprogress)
    map(radii) do r
        next!(prog)
        fsc_cpu(f1, f2, r, points, weights)
    end
end

function fsc_gpu(f1, f2, points, weights, radii; precision = 41)
    P = cu(SVector{3, Float32}.(points)) .* reshape(cu(map(Float32, radii)), 1, :)
    W = cu(weights)

    F1 = evaluate_complex(cu(f1), P)
    F2 = evaluate_complex(cu(f2), P)

    C12 = sum(real.(conj.(F1) .* W .* F2), dims = 1)
    C11 = sum(real.(conj.(F1) .* W .* F1), dims = 1)
    C22 = sum(real.(conj.(F2) .* W .* F2), dims = 1)

    Array{Float64}(reshape(C12 ./ sqrt.(C11 .* C22), :))
end

function fsc_gpu(f1, f2, radii; precision = 41, kwargs...)
    points, weights = lebedevGrid(precision = precision)
    fsc_gpu(f1, f2, points, weights, radii; kwargs...)
end

function fsc_gpu(f1, f2, radius::Number; kwargs...)
    fsc_gpu(f1, f2, [radius]; kwargs...)[1]
end

function radialCovariance(f1, f2, rs, s = 0.0)
    b = broadcast(f1.positions, f1.heights, f1.widths, Ref(f2.positions), Ref(f2.heights), Ref(f2.widths), reshape(rs, 1, :)) do p1, h1, σ1, ps, hs, σs, r
        x = zero(h1)
        for (p2, h2, σ2) in zip(ps, hs, σs)
            x += h1 * h2 * sinc(r * norm(p1 - p2) / pi) * exp(-(σ1^2 + σ2^2 - 2s^2) * r^2 / 2)
        end
        x
    end
    reshape(sum(b, dims = (1)), :)
end

function fsc_analytic(f1, f2, rs::AbstractArray)
    s = min(minimum(f1.widths), minimum(f2.widths))
    x = radialCovariance(f1, f2, rs, s) ./ sqrt.(radialCovariance(f1, f1, rs, s) .* radialCovariance(f2, f2, rs, s))
    Array{Float64}(x)
end

function fsc_analytic(f1, f2, r::Number)
    fsc_analytic(f1, f2, r:r)[1[]]
end

function fsc(f1, f2, r; analytic = nothing, usecuda = true, kwargs...)
    analytic = isnothing(analytic) ? usecuda : analytic

    if analytic
        if usecuda
            fsc_analytic(cu(f1), cu(f2), cu(map(Float32, r)))
        else
            fsc_analytic(f1, f2, r)
        end
    else
        if usecuda
            fsc_cpu(f1, f2, r; kwargs...)
        else
            fsc_gpu(f1, f2, r; kwargs...)
        end
    end
end


function fscres_gpu(f1, f2; threshold = 0.5, kmax = 5.0, tol = 1e-3, kperstep = 11, maxiterations = 100)
    kmax = Float64(kmax)
    kmin = 0.0
    for _ in 1:maxiterations
        ks = range(kmin, kmax, length = kperstep)
        fscs = fsc(f1, f2, ks, analytic = true, usecuda = true)
        imin = findlast(x -> x > threshold && isfinite(x), fscs)
        imax = findfirst(x -> x < threshold && isfinite(x), fscs)

        if isnothing(imin)
            return 0.0
        end
        kmin = ks[imin]
        kmax = isnothing(imax) ? 10kmax : ks[imax]
        if kmax - kmin < tol
            return kmin
        end
    end
    @warn "maximum iterations exceeded"
    return kmin
end

function fscres_cpu(f1, f2, precision = 41, rstep = 0.1, threshold = 0.5, rmax = 100.0, tol = 1e-5)
    points, weights = lebedevGrid(precision = precision)

    r = 0.0
    for i in 1:100000
        if fsc_cpu(f1, f2, r + rstep, points, weights) > threshold
            r += rstep
        else
            rstep /= 2
        end
        if rstep < tol || r >= rmax
            return r
        end
    end
    r
end

function fscResolution(f1, f2; usecuda = true, align = false, fourier = false, alignkwargs = (;), kwargs...)
    if align
        f1 = aligned(f1, f2; usecuda, alignkwargs...)
    end
    k = if usecuda
        fscres_gpu(f1, f2, kwargs...)
    else
        fscres_cpu(f1, g2, kwargs...)
    end
    fourier ? k : 2pi / k
end