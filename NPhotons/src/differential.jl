struct DifferentialAtomVolume{A, B, C}
    positions::A
    heights::B
    widths::C
end

function DifferentialAtomVolume(f::AtomVolume)
    DifferentialAtomVolume(f.positions, f.heights, f.widths)
end

function Base.:+(f::AtomVolume, df::DifferentialAtomVolume)
    AtomVolume(f.positions .+ df.positions, f.heights .+ df.heights, f.widths .+ df.widths)
end

function Base.:+(df::DifferentialAtomVolume, dg::DifferentialAtomVolume)
    DifferentialAtomVolume(df.positions .+ dg.positions, df.heights .+ dg.heights, df.widths .+ dg.widths)
end

function Base.:*(df::DifferentialAtomVolume, dg::DifferentialAtomVolume)
    DifferentialAtomVolume(((x, y) -> (x .* y)).(df.positions, dg.positions), df.heights .* dg.heights, df.widths .* dg.widths)
end

function Base.:/(df::DifferentialAtomVolume, dg::DifferentialAtomVolume)
    DifferentialAtomVolume(((x, y) -> (x ./ y)).(df.positions, dg.positions), df.heights ./ dg.heights, df.widths ./ dg.widths)
end

Base.:*(a::Tuple, df::DifferentialAtomVolume) = DifferentialAtomVolume(a...) * df
Base.:*(df::DifferentialAtomVolume, a::Tuple) = df * DifferentialAtomVolume(a...)
Base.:*(a::Real, df::DifferentialAtomVolume) = (a, a, a) * df
Base.:*(df::DifferentialAtomVolume, a::Real) = df * (a, a, a)
Base.:/(df::DifferentialAtomVolume, a::Tuple) = df / DifferentialAtomVolume(a...)
Base.:/(df::DifferentialAtomVolume, a::Real) = df / (a, a, a)

norms(df::DifferentialAtomVolume) = DifferentialAtomVolume(norm.(df.positions), norm.(df.heights), norm.(df.widths))
Base.sqrt(df::DifferentialAtomVolume) = DifferentialAtomVolume((x -> sqrt.(x)).(df.positions), sqrt.(df.heights), sqrt.(df.widths))
Base.:^(df::DifferentialAtomVolume, n) = DifferentialAtomVolume((x -> x.^n).(df.positions), df.heights .^ n, df.widths .^ n)

function Base.convert(::Type{DifferentialAtomVolume{A, B, C}}, df::DifferentialAtomVolume) where {A, B, C}
    DifferentialAtomVolume(convert(A, df.positions), convert(B, df.heights), convert(C, df.widths))
end

differential(f::AtomVolume) = DifferentialAtomVolume(f)

function kernel_pullback!(g, positions, widths, heights, ks, cs, dfs)
    i = (blockIdx().x - one(blockIdx().x)) * blockDim().x + threadIdx().x
    j = (blockIdx().y - one(blockIdx().y)) * blockDim().y + threadIdx().y
    tidx = threadIdx().x

    S = CuStaticSharedArray(SVector{5, Float32}, 32)

    @inbounds @fastmath begin
        if i <= length(ks)
            k = ks[i]
            y = positions[j]
            σ = widths[j]
            h = heights[j]    
    
            d = (dfs[i] * exp(-σ^2 * (k ⋅ k) / 2)) * cis(k ⋅ y) * conj(cs[i])
            dp = warpsum.([-2 * k * h * imag(d); 2 * real(d); -2 * σ * h * real(d) * (k ⋅ k)])
            
            f, m = fldmod1(tidx, warpsize())
            if m == 1
                S[f] = dp
            end
            sync_threads()

            if tidx <= warpsize()
                df = warpsum.(S[tidx], blockDim().x ÷ warpsize() ÷ 2)
                if tidx == 1
                    for n in 1:5
                        CUDA.@atomic g[n, j] += df[n]
                    end
                end
            end
        end
    end

    return
end

function pullback(f::AtomVolume, ks, dfs)
    ks = reshape(ks, :)
    cf = (dfs isa CuArray) ? cuda(f) : f
    @tc "eval" cs = evaluate_complex(cf, ks)

    g = CUDA.zeros(5, length(f))
    
    threads = (512, 1, 1)
    blocks = ceil.(Int, (length(ks), length(f), 1) ./ threads)
    shmem = 5 * 32 * sizeof(eltype(dfs))

    @cuda name="pull_kernel" threads=threads blocks=blocks shmem=shmem always_inline=true maxregs=40 kernel_pullback!(g, cf.positions, cf.widths, cf.heights, ks, cs, dfs)

    T = eltype(eltype(f.positions))
    r = Array{T}(g)

    cs isa CuArray && CUDA.unsafe_free!(cs)
    g isa CuArray && CUDA.unsafe_free!(g)
    unsafe_free!(cf)
    
    # r = zeros(5length(f))

    result = DifferentialAtomVolume(SVector{3, T}.(eachcol(Array(r[1:3, :]))), r[4, :], r[5, :])
end