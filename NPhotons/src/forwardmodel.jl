abstract type AbstractDetector end

struct Detector{T1, T2} <: AbstractDetector
    xs::T1
    ys::T1
    vals::T2
end

function (d::Detector)(x, y)
    if x < minimum(d.xs) || x > maximum(d.xs) || y < minimum(d.ys) || y > maximum(d.ys)
        zero(eltype(d.vals))
    else
        ix = findrange(x, d.xs)
        iy = findrange(y, d.ys)
        d.vals[ix, iy]
    end
end

(d::Detector)(p::SVector{2, <:Real}) = d(p...)
(d::Detector)(k::SVector{3, <:Real}; kwargs...) = d(e2d(k; kwargs...)[SA[1,2]])
(d::Detector)(x, y, z; kwargs...) = d(SA[x, y, z]; kwargs...)
(d::Detector)(p; kwargs...) = d(length(p) == 2 ? p[SA[1,2]] : p[SA[1,2,3]]; kwargs...)

function Detector(pixelpositions::AbstractArray, pixelsize::Real; ngrid = 201)
    dxs = getindex.(pixelpositions, 1); dys = getindex.(pixelpositions, 2)
    xs = range(minimum(dxs) - pixelsize, maximum(dxs) + pixelsize, length = ngrid)
    ys = range(minimum(dys) - pixelsize, maximum(dys) + pixelsize, length = ngrid)

    grid = vcat.(NPhotons.grid(xs, ys)[:], Ref(SA[0.0]))
    vals = zeros(ngrid, ngrid)
    inds = roundImages(pixelpositions[:], [grid])[1]
    for i in 1:length(vals)
        vals[i] = norm(pixelpositions[inds[i]] - grid[i], Inf) <= pixelsize
    end

    Detector(xs, ys, vals)
end

struct RectangularDetector{T} <: AbstractDetector
    x::T
    y::T
end

(d::RectangularDetector)(x, y) = ifelse(abs(x) < d.x && abs(y) < d.y, 1.0, 0.0)
(d::RectangularDetector)(p::SVector{2, <:Real}) = d(p...)
(d::RectangularDetector)(k::SVector{3, <:Real}; kwargs...) = d(e2d(k; kwargs...)[SA[1,2]])
(d::RectangularDetector)(x, y, z; kwargs...) = d(SA[x, y, z]; kwargs...)
(d::RectangularDetector)(p; kwargs...) = d(length(p) == 2 ? p[SA[1,2]] : p[SA[1,2,3]]; kwargs...)

struct IdealDetector <: AbstractDetector end
(d::IdealDetector)(p) = one(eltype(p))
(d::IdealDetector)(x, y) = one(x)
(d::IdealDetector)(x, y, z) = one(x)

abstract type AbstractForwardModel end

@kwdef struct ForwardModel{NT <: AbstractFloat, VT, HT1, HT2, DT <: AbstractDetector} <: AbstractForwardModel
    λ::NT = 2.5
    inelastic::VT = nothing
    background_elastic::VT = nothing
    background_inelastic::VT = nothing
    
    hitdist::HT1 = (1,)
    intensitydist::HT2 = (1,)
    meanintensity::NT = 1.0

    detector::DT = IdealDetector()
    polarization::Bool = false
end

function ForwardModel(inelastic, background_elastic, background_inelastic; qmax = Inf, bsigma = 2.0, kwargs...)
    inelastic = normalizeScattering(AtomVolume(1, 0.001), nphotons = inelastic, qmax = qmax)
    background_elastic = normalizeScattering(AtomVolume(1, bsigma), nphotons = background_elastic, qmax = qmax)
    background_inelastic = normalizeScattering(AtomVolume(1, 0.001), nphotons = background_inelastic, qmax = qmax)
    ForwardModel(; inelastic = inelastic, background_elastic = background_elastic, background_inelastic = background_inelastic, kwargs...)
end

function hasnoise(model::ForwardModel)
    !isnothing(model.inelastic)
end

function smoothen(m::ForwardModel, σ)
    !hasnoise(m) && return m
    m = @set m.inelastic = smoothen(m.inelastic, σ)
    m = @set m.background_elastic = smoothen(m.background_elastic, σ)
    m = @set m.background_inelastic = smoothen(m.background_inelastic, σ)
end

function intensities(f, m::AbstractForwardModel, R, k; η = 1)
    fp = m.polarization ? polarizationFactor(k; λ = m.λ) : 1.0
    if isnothing(m.inelastic)
        (η * fp * f(R * k), )
    else
        (η * fp * f(R * k), η * m.inelastic(k), fp * m.background_elastic(k), m.background_inelastic(k))
    end
end

function intensity(f, m::AbstractForwardModel, R, k; η = 1)
    sum(intensities(f, m, R, k; η = η))
end

function envelope(f, m::AbstractForwardModel; η = 1)
    sη = sqrt(η)
    if isnothing(m.inelastic)
        envelope(sη * f)
    else
        envs = envelope.((sη * f, sη * m.inelastic, m.background_elastic, m.background_inelastic))
        k -> sum(e(k) for e in envs)
    end
end

function cuda(m::ForwardModel, T = Float32)
    inelastic = cuda(m.inelastic, T)
    background_elastic = cuda(m.background_elastic, T)
    background_inelastic = cuda(m.background_inelastic, T)
    ForwardModel(m.λ, inelastic, background_elastic, background_inelastic, m.hitdist, m.intensitydist, m.meanintensity, m.detector, m.polarization)
end

function unsafe_free!(m::ForwardModel)
    unsafe_free!(m.inelastic)
    unsafe_free!(m.background_elastic)
    unsafe_free!(m.background_inelastic)
end

struct WeightedEnsemble{S <: AbstractVector, T <: AbstractWeights}
    structs::S
    weights::T

    function WeightedEnsemble(structs::AbstractVector, weights::AbstractWeights)
        @assert length(structs) == length(weights)
        new{typeof(structs), typeof(weights)}(structs, weights)
    end
end

WeightedEnsemble(structs::AbstractVector, weights::AbstractVector) = WeightedEnsemble(structs, Weights(weights))
WeightedEnsemble(structs::AbstractVector) = WeightedEnsemble(structs, ones(length(structs)))
UniformEnsemble(structs) = WeightedEnsemble(structs, uweights(length(structs)))

Base.length(ensemble::WeightedEnsemble) = length(ensemble.structs)
Base.iterate(ensemble::WeightedEnsemble, args...) = Base.iterate(ensemble.structs, args...)
_getstructures(x::WeightedEnsemble) = _getstructures(x.structs)

function Random.rand(rng::AbstractRNG, s::Random.SamplerTrivial{<:WeightedEnsemble})
    sample(rng, s[].structs, s[].weights)
end


function generateImage(f, m::AbstractForwardModel, ::Val{attribute} = Val(false); qmax = Inf, R = nothing, upperbound = x -> Inf) where attribute
    ewaldradius = 2pi / m.λ
    ewaldarea = 4pi * ewaldradius^2

    R = isnothing(R) ? randomRotation() : R
    I0 = m.meanintensity * rand(m.intensitydist)
    η = rand(m.hitdist)

    envf = envelope(f, m, η = η)
    fmax = Float64(intensity(f, m, R, SA[0.0,0.0,0.0]; η = η))

    image = SVector{3, Float64}[]
    attrs = Int[]

    for j in 1:rand(Poisson(I0 * fmax * ewaldarea))
        k = ewaldradius * (normalize(randn(SVector{3, Float64})) - SA[0.0, 0.0, 1.0])
        nk = norm(k)
        
        rval = rand() * fmax
        if nk < qmax && rval < envf(nk)
            b = upperbound(nk)
            if rval < b
                d = m.detector(k)
                for (i, I) in enumerate(cumsum(intensities(f, m, R, k; η = η)))
                    @assert d * I < b
                    if rval < d * I
                        push!(image, k)
                        attribute && push!(attrs, i)
                        break
                    end
                end
            end
        end
    end
    
    attribute ? (image, attrs, (; η, I0, R)) : image
end

function generateImage(ensemble::WeightedEnsemble, m::AbstractForwardModel, ::Val{attribute} = Val(false); kwargs...) where attribute
    i = sample(1:length(ensemble), ensemble.weights)
    image = generateImage(rand(ensemble), m, Val(attribute); kwargs...)
    attribute ? (i, image) : image
end

function generateImages(f, m::AbstractForwardModel, nimages::Int, args...; showprogress = true, kwargs...)
    prog = Progress(nimages, enabled = showprogress)
    map(1:nimages) do i
        next!(prog)
        generateImage(f, m, args...; kwargs...)
    end
end

function generateImagesThreaded(f, m::AbstractForwardModel, nimages::Int, args...; showprogress = true, kwargs...)
    prog = Progress(nimages, enabled = showprogress)

    images = Vector{typeof(generateImage(f, m, args...; kwargs...))}(undef, nimages)
    Threads.@threads for i in 1:nimages
        next!(prog)
        images[i] = generateImage(f, m, args...; kwargs...)
    end
    finish!(prog)
    images
end