module NPhotons

using ProgressMeter, LinearAlgebra, DataStructures, NearestNeighbors, Distances, StaticArrays, Interpolations, CUDA, JLD2, FileIO, Random, Distributions, SpecialFunctions, Combinatorics, IterTools, DelimitedFiles, StatsBase, StatsFuns, Accessors, Arrow, FastGaussQuadrature, SparseArrays, TimerOutputs, Glob, BasicInterpolators
import Humanize: digitsep
import BioStructures, PDBTools

using CUDA: i32

include("timing.jl")
include("AtomVolume.jl")
include("GaussianShellVolume.jl")
include("sampling.jl")
include("rotations.jl")
include("util.jl")
include("fsc.jl")
include("kernel.jl")
include("pdb.jl")
include("forwardmodel.jl")
include("compute.jl")
include("differential.jl")
include("gradient.jl")
include("multigpu.jl")
include("optim.jl")
include("io.jl")

function monitor end

export AtomVolume, randomAtomVolume, normalizeScattering
export AbstractForwardModel, ForwardModel
export AbstractDetector, Detector, IdealDetector
export WeightedEnsemble, UniformEnsemble
export generateImage, generateImages, generateImagesThreaded
export IntegrationParameters, ComputeConfiguration, ComputeAllocation, GradientAllocation
export MultiComputeAllocation, MultiGradientAllocation
export AbstractProposal, AdaptiveProposal, AlternatingProposal
export AbstractSampler, SimulatedAnnealing, AnnealingState
export nstepsRemaining, acceptanceRate
export align, aligned, fsc, fscResolution, randomRotation
export getpositions, getheights, getwidths, getconf, getenergy, gettemperature, getnsteps
export saveChain, loadChain, loadChainAsArray, exportJob, saveImages, loadImages

end
