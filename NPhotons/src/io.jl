function ArrowTypes.arrowname(::Type{AtomVolume{T, A, B}}) where {T, A, B}
    :AtomVolume
end

function ArrowTypes.JuliaType(::Val{:AtomVolume}, T::Type{NamedTuple{names, types}}) where {names, types}
    NT = eltype(eltype(fieldtype(T, :positions)))
    AtomVolume{NT, Vector{SVector{3, NT}}, Vector{eltype(fieldtype(T, :heights))}}
end

function ArrowTypes.fromarrow(::Type{AtomVolume{T, A, B}}, positions, heights, widths) where {T, A, B}
    AtomVolume(convert(A, positions), convert(B, heights), convert(B, widths))
end

function ArrowTypes.arrowname(::Type{AnnealingState{T1, T2}}) where {T1, T2}
    :AnnealingState
end

function ArrowTypes.JuliaType(::Val{:AnnealingState}, T::Type{NamedTuple{names, types}}) where {names, types}
    AnnealingState{fieldtype(T, :conf), fieldtype(T, :proposalParams)}
end

function ArrowTypes.fromarrow(::Type{AnnealingState{T1, T2}}, args...) where {T1, T2}
    AnnealingState(args...)
end

function ArrowTypes.arrowname(::Type{DifferentialAtomVolume{A, B, C}}) where {A, B, C}
    :DifferentialAtomVolume
end

function ArrowTypes.JuliaType(::Val{:DifferentialAtomVolume}, T::Type{NamedTuple{names, types}}) where {names, types}
    NT = eltype(eltype(fieldtype(T, :positions)))
    DifferentialAtomVolume{Vector{SVector{3, NT}}, Vector{eltype(fieldtype(T, :heights))}, Vector{eltype(fieldtype(T, :widths))}}
end

function ArrowTypes.fromarrow(::Type{DifferentialAtomVolume{A, B, C}}, positions, heights, widths) where {A, B, C}
    DifferentialAtomVolume(convert(A, positions), convert(B, heights), convert(C, widths))
end

function ArrowTypes.arrowname(::Type{SGDState{T1, T2, T3}}) where {T1, T2, T3}
    :SGDState
end

function ArrowTypes.JuliaType(::Val{:SGDState}, T::Type{NamedTuple{names, types}}) where {names, types}
    SGDState{fieldtype(T, :conf), fieldtype(T, :grad), fieldtype(T, :params)}
end

function ArrowTypes.fromarrow(::Type{SGDState{T1, T2, T3}}, args...) where {T1, T2, T3}
    SGDState(args...)
end


function saveChain(path, chain; stride = 1)
    laststored = isfile(path) ? Arrow.Table(path).chain[end].nsteps : -1
    startindex = findfirst(c -> c.nsteps > laststored && c.nsteps % stride == 0, chain)
    if !isnothing(startindex)
        Arrow.append(path, (; chain = chain[startindex:stride:end]))
    end
end

function loadChain(path)
    Arrow.Table(path).chain
end

function loadChainAsArray(path; showprogress = true, stride = 1)
    chain = loadChain(path)
    prog = Progress(length(1:stride:length(chain)), enabled = showprogress)
    map(1:stride:length(chain)) do i
        next!(prog)
        chain[i]
    end
end

# function saveChunked(path, chain)
#     jldopen(path, "a+") do f
#         new = chain[get(f, "nstored", 0) + 1 : end]
#         haskey(f, "nstored") && delete!(f, "nstored")
#         f["nstored"] = length(chain)
#         chunks = filter(c -> occursin("chunk", c), keys(f))
#         chunkid = maximum([parse(Int, c[6:end]) for c in chunks], init = 0) + 1
#         f["chunk$chunkid"] = new
#     end
#     nothing
# end

# function loadChunked(path)
#     @assert isfile(path)
#     jldopen(path, "r") do f
#         A = f["chunk1"]
#         i = 2
#         while haskey(f, "chunk$i")
#             append!(A, f["chunk$i"])
#             i += 1
#         end
#         A
#     end
# end

function exportJob(path, data; distributed = false, benchmark = false, options = "", script = "run", kwargs...)
    jobname = splitdir(path)[2]
    if !isempty(options) options = options * " " end
    runpath = project_path("../scripts/$script.jl")
    command = if script == "datagenV2"
        "$runpath $jobname.jld2 $options"
    else
        "$runpath $(distributed ? "-d " : "")$(benchmark ? "-b " : "")$options-g $jobname.jld2"
    end
    export_jobscript("$path.sh", command; kwargs...)
    jldsave("$path.jld2"; data...)
    nothing
end

function jobscript(path, command; ntasks = 1, ngpus = 1, gputype = "", repeat = nothing, exclude = false, exclusive = false, mode = :singlenode, partition = "p00")
    jobname = splitext(splitdir(path)[2])[1]
    array = isnothing(repeat) ? "" : "#SBATCH --array=1-$(repeat)%1"
    exclude = exclude ? "#SBATCH --exclude=node36-[01-07]\n" : ""
    gpuspertask = isempty(gputype) ? "1" : "$gputype:1"

    if mode == :multinode
        """
        #!/bin/bash

        #SBATCH --partition p00
        #SBATCH --gpus-per-task=$gpuspertask
        #SBATCH --gpu-bind=single:1
        
        #SBATCH --ntasks=$ntasks
        #SBATCH --cpus-per-task=2
        #SBATCH --mem-per-cpu=5G
        
        #SBATCH --dependency=singleton
        #SBATCH --time=24:00:00
        #SBATCH --job-name=$jobname
        
        #SBATCH --mail-type=NONE
        #SBATCH --output=%x.out
        #SBATCH --error=%x.out
        #SBATCH --open-mode=append
        
        $(exclusive ? "#SBATCH --exclusive\n" : "")$exclude$array
        
        julia -t 1 --project=@. $command
        """
    elseif mode == :singlenode
        """
        #!/bin/bash

        #SBATCH --partition $partition
        #SBATCH --nodes=1
        #SBATCH --gpus=$gputype$(isempty(gputype) ? "" : ":")$ngpus
        $(!exclusive ? "#SBATCH --mem-per-gpu=10G" : "")
        $(!exclusive ? "#SBATCH --cpus-per-gpu=2" : "")
        
        #SBATCH --dependency=singleton
        #SBATCH --time=24:00:00
        #SBATCH --job-name=$jobname
        
        #SBATCH --mail-type=NONE
        #SBATCH --output=%x.out
        #SBATCH --error=%x.out
        #SBATCH --open-mode=append

        $(exclusive ? "#SBATCH --exclusive\n" : "")$exclude$array
        
        julia $(exclusive ? "" : "-t $(2ngpus)") --project=@. $command
        """
    end
end

function export_jobscript(path, args...; kwargs...)
    open(path, "w") do io
        write(io, jobscript(path, args...; kwargs...))
    end
end

function loadIndexImages(images, pixels; σ = 0.0, qmax = Inf, nimages = Inf, showprogress = true)
    tree = KDTree(reduce(hcat, pixels))
    s = smoothingFilter(σ) ∘ filter(<(qmax) ∘ norm)

    indexImages = Vector{UInt32}[]

    prog = ProgressUnknown(enabled = showprogress)
    for image in images
        push!(indexImages, nn(tree, s(image))[1])
        next!(prog)
        length(indexImages) >= nimages && break
    end
    finish!(prog)
 
    indexImages
end

function loadIndexImages(pattern::String, pixels; kwargs...)
    files = Glob.glob(pattern)
    @assert !isempty(files)
    loadIndexImages(files, pixels; kwargs...)
end

function loadIndexImages(files::Vector{String}, pixels; kwargs...)
    loadIndexImages(Iterators.flatten(imap(loadImages, files)), pixels; kwargs...)
end


function serializeImages(images)
    images = [(x -> Float32.(x)).(image) for image in images]
    padded = vcat.(images, Ref([fill(NaN, eltype(eltype(images)))]))
    reduce(vcat, padded)
end

function deserializeImages(photons::Array)
    loc = findall(x -> isnan(x[1]), photons)
    if isnan(photons[end][1])
        loc = loc[1:end-1]
    end
    view.(Ref(photons), UnitRange.([1; loc .+ 1], [loc .- 1; length(photons) - 1]))
end

function saveImages(path, images; metadata...)
    images = [(x -> Float32.(x)).(image) for image in images]
    padded = vcat.(images, Ref([fill(NaN, eltype(eltype(images)))]))
    jldsave(path, reduced = reduce(vcat, padded), metadata = NamedTuple(metadata))
end

function loadImages(path::String)
    loadImages(load(path, "reduced"))
end

function loadImages(reduced::Array)
    photons = reduced
    loc = @view findall(x -> isnan(x[1]), photons)[1:end - 1]
    images = view.(Ref(photons), UnitRange.([1; loc .+ 1], [loc .- 1; length(photons) - 1]))
end

function getinpath(job)
    "$(splitext(job)[1]).jld2"
end

function getoutpath(job)
    d, f = splitdir(splitext(job)[1])
    joinpath(d, "out", "$f.arrow")
end

getjobpaths(job) = (getinpath(job), getoutpath(job))