struct IntegrationParameters
    rotations::Vector{QuatRotation{Float64}}
    rotationWeights::Vector{Float64}
    
    pixels::Vector{SVector{3, Float64}}
    pixelWeights::Vector{Float64}

    precision::Int
    qstep::Float64
    qmax::Float64
    npercircle::Int
    wavelength::Float64
    noffsets::Int
end

function IntegrationParameters(; precision = 23, qstep, qmax, npercircle = 32, wavelength = 2.5, inner = true, stride = 1, noffsets = 1)
    pixels, pixelWeights = weightedEwaldGrid(qstep = qstep, qmax = qmax, npercircle = npercircle, wavelength = wavelength, noffsets = noffsets)
    rotations, rotationWeights = if inner
        rotationGrid(precision = precision)
    else
        uniformRotationGrid(precision = precision, npercircle = npercircle)
    end
    IntegrationParameters(rotations, rotationWeights, pixels, pixelWeights, precision, qstep, qmax, npercircle, wavelength, noffsets)
end

function Base.show(io::IO, params::IntegrationParameters)
    show(io, IntegrationParameters)
    print(io, "(precision = $(params.precision), qstep = $(params.qstep), qmax = $(params.qmax), npercircle = $(params.npercircle), wavelength = $(params.wavelength))")
end


struct ComputeConfiguration
    params::IntegrationParameters
    indexImages::Vector{Vector{Int}}

    batchsize::Int
    ncalls::Int
    inner::Bool
    nhitvalues::Int
    stride::Int
end

function ComputeConfiguration(images; qmax, σreg = 0.0, nimages = Inf, ncalls = 0, inner = true, nhitvalues = 10, stride = 1, sortimages = true, showprogress = true, batchsize = 0, kwargs...)
    params = IntegrationParameters(; qmax, inner = inner, kwargs...)
    indexImages = if eltype(eltype(images)) <: Integer
        copy(images)
    else
        loadIndexImages(images, params.pixels; σ = σreg, qmax = qmax, nimages = nimages, showprogress = showprogress)
    end
    sortimages && sort!(indexImages, by = length)
    ComputeConfiguration(params, indexImages, batchsize, ncalls, inner, nhitvalues, stride)
end

function setimages!(conf::ComputeConfiguration, images; σreg = 0.0, showprogress = false, sortimages = true)
    indexImages = if eltype(eltype(images)) <: Integer
        copy(images)
    else
        loadIndexImages(images, conf.params.pixels; σ = σreg, qmax = conf.params.qmax, showprogress = showprogress)
    end
    sortimages && sort!(indexImages, by = length)

    resize!(conf.indexImages, length(indexImages))
    conf.indexImages .= indexImages

    conf
end

function smoothen(conf::ComputeConfiguration, σ)
    ps = smoothingKernel(σ).(norm.(conf.params.pixels))
    newimages = map(conf.indexImages) do image
        image[rand.() .< ps[image]]
    end
    @set conf.indexImages = newimages
end

function setqmax(conf::ComputeConfiguration, qmax)
    ps = norm.(conf.params.pixels) .< qmax
    newimages = map(conf.indexImages) do image
        image[ps[image]]
    end
    @set conf.indexImages = newimages
end

Base.length(conf::ComputeConfiguration) = length(conf.indexImages)

function Base.show(io::IO, conf::ComputeConfiguration)
    show(io, ComputeConfiguration)
    print(io, "(ncalls = $(conf.ncalls), inner = $(conf.inner), nhitvalues = $(conf.nhitvalues))")

    if !get(io, :compact, false)
        params = conf.params
        println(io, ":")
        println(io, "  precision = $(params.precision), qstep = $(params.qstep), qmax = $(params.qmax), npercircle = $(params.npercircle), wavelength = $(params.wavelength),")
        print(io, "  $(length(conf.indexImages)) images, ")
        print(io, "$(sum(length, conf.indexImages)) photons, ")
        np = conf.inner ? 1 : conf.params.npercircle
        print(io, "$(length(conf.params.rotations)÷np) × $(conf.params.npercircle) = $(length(conf.params.rotations) ÷ np * conf.params.npercircle) rotations, ")
        print(io, "$(length(conf.params.pixels)÷conf.params.npercircle) × $(conf.params.npercircle) = $(length(conf.params.pixels)) pixels")
    end
end

struct SerializedIndexImages{I1, I2, I3}
    locations::I1
    lengths::I2
    indices::I3
end

function SerializedIndexImages(::Type{T}, ::Type{N}, indexImages) where {T, N}
    lengths = T{N}(length.(indexImages))
    locations, indices = T{N}.(serializeIndexImages(indexImages))
    SerializedIndexImages(locations, lengths, indices)
end

SerializedIndexImages(::Type{T}, indexImages) where {T} = SerializedIndexImages(T, eltype(eltype(indexImages)), indexImages)
SerializedIndexImages(indexImages) = SerializedIndexImages(Array, indexImages)

Base.length(images::SerializedIndexImages) = length(images.locations)

function Base.copyto!(imgs1::SerializedIndexImages, imgs2::SerializedIndexImages)
    @assert eltype(imgs1.locations) == eltype(imgs2.locations)

    n = length(imgs1)
    copyto!(imgs1.locations, 1:n, imgs1.locations, 1:n)
    copyto!(imgs1.lengths, 1:n, imgs1.lengths, 1:n)
    if length(imgs1.indices) < length(imgs2.indices)
        resize!(imgs1.indices, imgs2.indices)
    end
    copyto!(imgs1.indices, imgs2.indices)

    imgs1
end

function Base.resize!(images::SerializedIndexImages, n)
    resize!(images.locations, n)
    resize!(images.lengths, n)
end

function copytoresize!(imgs1::SerializedIndexImages, imgs2::SerializedIndexImages)
    resize!(imgs1, length(imgs2))
    copyto!(imgs1, imgs2)
end

struct ComputeAllocation{M, C1, I1, I2, F1, F2, F3, F4, V1, V2, W1, W2, L1, L2, L3, P1, P2, D1, D2, W3, B1, B2, S1, S2, R1, R2, R3, R4}
    configuration::ComputeConfiguration
    forwardmodel::M
    npercircle::C1

    indexImages::I1
    allindexImages::I2

    fvalues::F1
    fvaluesDuplicated::F2
    meanfvalues::F3
    meanfvaluesDuplicated::F4
    
    pixels::V1
    rotatedPixels::V2
    pixelWeights::W1
    rotationWeights::W2

    nphotonsPerRotation::L1
    nphotonLikelihoods::L2
    maxnphotonLikelihoods::L3
    
    polarizationFactors::P1
    polarizationFactorsDuplicated::P2

    detectionFactors::D1
    weightedDetectionFactors::D2

    premulPixelWeights::W3

    bvalues::B1
    bvaluesDuplicated::B2
    
    scratch1::S1
    scratch2::S2

    kernelResults::R1
    allKernelResults::R2
    likelihoods::R3
    loglikelihoods::R4
end

macro unpack_Allocation(q)
    code = Expr(:block, [ :($field = $q.$field) for field in fieldnames(ComputeAllocation) ]...)
    esc(code)
end

function ComputeAllocation(::Type{T}, ::Type{N}, ::Type{ND}, model::AbstractForwardModel, conf::ComputeConfiguration; storeall = false) where {T, N, ND}
    oversampledetector = 10
    npercircle = conf.params.npercircle

    shiftedIndexImages = [image .+ convert.(eltype(image), npercircle .* ((image .- 1) .÷ npercircle)) for image in conf.indexImages]

    allindexImages = SerializedIndexImages(T, shiftedIndexImages)
    indexImages = deepcopy(allindexImages)

    @assert conf.batchsize <= length(indexImages)
    if conf.batchsize != 0
        resize!(indexImages, conf.batchsize)
    end

    npix = length(conf.params.pixels)
    nrot = length(conf.params.rotations)
    nimg = length(indexImages)
    lmax = maximum(allindexImages.lengths, init = 0)

    fvalues = T{ND}(undef, npix, nrot)
    fvaluesDuplicated = T{N}(undef, 2npix, nrot)
    
    meanfvalues = T{ND}(undef, npix); meanfvalues .= 1
    meanfvaluesDuplicated = T{ND}(undef, 2npix + 1); meanfvaluesDuplicated .= 1

    pixels = T{SVector{3, Float32}, 1}(conf.params.pixels)
    rotatedPixels = T{SVector{3, Float32}, 2}([R * x for x in conf.params.pixels, R in conf.params.rotations])
    pixelWeights = T{ND}(conf.params.pixelWeights)
    rotationWeights = T{ND}(conf.params.rotationWeights)

    premulPixelWeights = if model.polarization || !isa(model.detector, IdealDetector)
        arr = zeros(length(pixelWeights), npercircle)
        for i in 0:npercircle-1
            circshift!(view(reshape(arr, npercircle, :, npercircle),:,:,i+1), reshape(conf.params.pixelWeights, npercircle, :), (i, 0))
        end
        arr
    else
        nothing
    end

    polarizationFactors = model.polarization ? polarizationFactor.(conf.params.pixels; λ = model.λ) : nothing
    polarizationFactorsDuplicated = if model.polarization
        rpolfacs = reshape(polarizationFactors, npercircle, :)
        T{N}(vcat(rpolfacs, rpolfacs)[:])
    else
        nothing
    end

    detectionFactors = if !isa(model.detector, IdealDetector)
        ps, ws = weightedEwaldGrid(
            qstep = conf.params.qstep / oversampledetector, qmax = conf.params.qmax, 
            npercircle = oversampledetector * npercircle, wavelength = model.λ,
            noffsets = conf.params.noffsets
        )
        ds = zeros(length(conf.params.pixels))
        counts = zeros(length(conf.params.pixels))
        for (i, p, w) in zip(nearestneighbors(conf.params.pixels, ps), ps, ws)
            ds[i] += model.detector(p) * w
            counts[i] += w
        end
        ds ./= (counts .== 0) .+ counts
        ds
    else
        nothing
    end

    if model.polarization
        for i in 0:npercircle-1
            view(reshape(premulPixelWeights, npercircle, :, npercircle),:,:,i+1) .*= circshift(reshape(polarizationFactors, npercircle, :), (i, 0))
        end
    end
    if !isa(model.detector, IdealDetector)
        for i in 0:npercircle-1
            view(reshape(premulPixelWeights, npercircle, :, npercircle),:,:,i+1) .*= circshift(reshape(detectionFactors, npercircle, :), (i, 0))
        end
    end


    polarizationFactors = isnothing(polarizationFactors) ? nothing : T{ND}(polarizationFactors)
    detectionFactors = isnothing(detectionFactors) ? nothing : T{ND}(detectionFactors)
    weightedDetectionFactors = isnothing(detectionFactors) ? nothing : detectionFactors .* pixelWeights
    premulPixelWeights = isnothing(premulPixelWeights) ? nothing : T{ND}(premulPixelWeights)

    nphotonsPerRotation = T{ND}(undef, nrot, isnothing(premulPixelWeights) ? 1 : npercircle)
    nphotonLikelihoods = T{ND}(undef, lmax + 1, nrot, (isnothing(premulPixelWeights) ? () : (npercircle,))...)
    maxnphotonLikelihoods = T{ND}(undef, lmax + 1)

    bvalues = hasnoise(model) ? T{ND}(undef, npix) : nothing
    bvaluesDuplicated = hasnoise(model) ? T{N}(undef, 2npix, nrot) : nothing

    scratch1 = T{ND}(undef, nimg)
    scratch2 = T{ND}(undef, 2npix ÷ npercircle)

    ncalls = if conf.ncalls == 0
        if storeall
            ceil(Int, nimg * nrot * npercircle / 5e9)
        else
            ceil(Int, nimg * nrot / 1e9)
        end
    elseif nimg * nrot / conf.ncalls > typemax(Int32)
        @warn "ncalls too large"
        ceil(Int, nimg * nrot / typemax(Int32))
    else
        conf.ncalls
    end

    kernelResults = T{ND}(undef, ceil(Int, nimg / ncalls), nrot)
    allKernelResults = storeall ? T{ND}(undef, npercircle, ceil(Int, nimg / ncalls), nrot) : nothing
    likelihoods = T{ND}(undef, nimg)
    loglikelihoods = T{ND}(undef, nimg)

    ComputeAllocation(conf, deepcopy(model), Val(npercircle), indexImages, allindexImages, fvalues, fvaluesDuplicated, meanfvalues, meanfvaluesDuplicated, pixels, rotatedPixels, pixelWeights, rotationWeights, nphotonsPerRotation, nphotonLikelihoods, maxnphotonLikelihoods, polarizationFactors, polarizationFactorsDuplicated, detectionFactors, weightedDetectionFactors, premulPixelWeights, bvalues, bvaluesDuplicated, scratch1, scratch2, kernelResults, allKernelResults, likelihoods, loglikelihoods)
end

function ComputeAllocation(model::AbstractForwardModel, conf::ComputeConfiguration; kwargs...)
    ComputeAllocation(CuArray, Float32, model, conf; kwargs...)
end

function ComputeAllocation(T1, T2, model::AbstractForwardModel, conf::ComputeConfiguration; kwargs...)
    ComputeAllocation(T1, T2, T2, model, conf; kwargs...)
end

function ComputeAllocation(T1, T2s::Tuple, model::AbstractForwardModel, conf::ComputeConfiguration; kwargs...)
    ComputeAllocation(T1, T2s[1], T2s[2], model, conf; kwargs...)
end

function setimages!(alloc::ComputeAllocation, newimages)
    @assert maximum(length, newimages) < size(alloc.nphotonLikelihoods, 1)

    npercircle = alloc.configuration.params.npercircle

    shiftedIndexImages = [image .+ convert.(eltype(image), npercircle .* ((image .- 1) .÷ npercircle)) for image in indexImages]
    newindexImages = SerializedIndexImages(Array, UInt32, shiftedIndexImages)
    
    copyto!(alloc.indexImages, newindexImages)
    copytoresize!(alloc.allindexImages, newindexImages)
    
    alloc
end

function filterImages!(alloc::ComputeAllocation, svals)
    alloc.indexImages.indices .= 0
    broadcast!(alloc.allindexImages.lengths, Ref(alloc.indexImages.indices), Ref(alloc.allindexImages.indices), Ref(svals), alloc.allindexImages.locations) do indices, allindices, svals, i
        l = allindices[i]
        jout = i
        lout = zero(l)
        for jin in i+one(i):i+l
            p = allindices[jin]
            if rand() < svals[p]
                indices[jout += one(jout)] = p
                lout += one(lout)
            end
        end
        indices[i] = lout
        lout
    end

    alloc
end

function filterImages!(alloc::ComputeAllocation; σ = 0, qmax = Inf, smooth = 0.0)
    qfunc = x -> ifelse(isfinite(qmax), smoothstep(x, qmax - smooth, qmax + smooth, 1, 0), 1.0)
    alloc.pixelWeights .= cu(alloc.configuration.params.pixelWeights)
    # alloc.pixelWeights .= cu(alloc.configuration.params.pixelWeights .* (norm.(alloc.configuration.params.pixels) .< qmax))

    s = Float32.(qfunc.(norm.(alloc.pixels)))
    
    if σ != 0
        s .*= Float32.(smoothingKernel(σ).(norm.(alloc.pixels)))
    end

    sd = duplicateChunks!(CUDA.zeros(2length(alloc.pixels)), s, alloc.configuration.params.npercircle)
    filterImages!(alloc, sd)
    CUDA.unsafe_free!(sd); CUDA.unsafe_free!(s)

    alloc
end

smoothen!(alloc::ComputeAllocation, σ) = filterImages!(alloc, σ = σ)
setqmax!(alloc::ComputeAllocation, qmax) = filterImages!(alloc, qmax = qmax)


function selectimages!(alloc::ComputeAllocation, inds)
    @assert length(inds) == length(alloc.indexImages)
    if alloc.indexImages.locations isa CuArray
        inds = cu(inds)
    end
    alloc.indexImages.locations .= @view alloc.allindexImages.locations[inds]
    alloc.indexImages.lengths .= @view alloc.allindexImages.lengths[inds]
    inds isa CuArray && CUDA.unsafe_free!(inds)
    alloc
end

function randomizeImages!(alloc::ComputeAllocation)
    inds = StatsBase.sample(1:length(alloc.allindexImages), length(alloc.indexImages), replace = false)
    selectimages!(alloc, inds)
end

function iscuda(alloc::ComputeAllocation)
    alloc.fvalues isa CuArray
end

function minsize!(alloc::ComputeAllocation, n)
    if length(alloc.likelihoods) < length(alloc.indexImages) * n
        resize!(alloc.likelihoods, length(alloc.indexImages) * n)
        resize!(alloc.loglikelihoods, length(alloc.indexImages) * n)
    end
end

function compute_foreground!(alloc::ComputeAllocation, f, η = 1)
    cf = iscuda(alloc) ? cuda(f, eltype(alloc.fvalues)) : f

    alloc.fvalues .= η .* cf.(alloc.rotatedPixels)
    cf !== f && unsafe_free!(cf)
end

function compute_background!(alloc::ComputeAllocation, m, η = 1)
    isnothing(alloc.bvalues) && return
    C(x) = iscuda(alloc) ? cuda(x, eltype(alloc.bvalues)) : x

    cf_inel = C(m.inelastic)
    cb_el = C(m.background_elastic)
    cb_inel = C(m.background_inelastic)

    if !isnothing(m.background_elastic)
        if !isnothing(alloc.polarizationFactors)
            alloc.bvalues .= alloc.polarizationFactors .* cb_el.(alloc.pixels)
        else
            alloc.bvalues .= cb_el.(alloc.pixels)
        end
        alloc.bvalues .+= η .* cf_inel.(alloc.pixels)
        alloc.bvalues .+= cb_inel.(alloc.pixels)
    end

    m.inelastic !== cf_inel && unsafe_free!(cf_inel); 
    m.background_elastic !== cb_el && unsafe_free!(cb_el); 
    m.background_inelastic !== cb_inel && unsafe_free!(cb_inel)
end

function compute_intensities!(alloc::ComputeAllocation, f, m, η = 1)
    compute_foreground!(alloc, f, η)
    compute_background!(alloc, m, η)
end

function compute_nphotonsPerRotation!(alloc::ComputeAllocation)
    @unpack_Allocation(alloc)

    mul!(nphotonsPerRotation, fvalues', isnothing(premulPixelWeights) ? pixelWeights : premulPixelWeights)

    if !isnothing(bvalues)
        if !isnothing(weightedDetectionFactors)
            nphotonsPerRotation .+= bvalues ⋅ weightedDetectionFactors
        else
            nphotonsPerRotation .+= bvalues ⋅ pixelWeights
        end
    end
    nphotonsPerRotation
end

function compute_radialAverages!(alloc::ComputeAllocation, model; η = 1)
    mean!(alloc.meanfvalues, alloc.fvalues)
    !isnothing(alloc.bvalues) && (alloc.meanfvalues .+= alloc.bvalues)
    
    if model.intensitydist isa Gamma || isnothing(model.intensitydist)
        # should fix, dont to this twice
        nPR = compute_nphotonsPerRotation!(alloc)
        rnPR = reshape(nPR, 1, size(nPR)...)

        β = isnothing(model.intensitydist) ? 0 : 1 / model.intensitydist.θ / model.meanintensity
        alloc.meanfvalues .*= mean(x -> 1 / (x + β), rnPR, dims = 2)
    end

    A = reshape(alloc.meanfvalues, alloc.configuration.params.npercircle, :)
    B = reshape(view(alloc.meanfvaluesDuplicated, 2:size(A, 2)+1), 1, :)
    mean!(B, A); A .= B
    duplicateChunks!(@view(alloc.meanfvaluesDuplicated[2:end]), alloc.meanfvalues, alloc.configuration.params.npercircle)
end

function applyKernel!(alloc::ComputeAllocation; rotationrange = nothing, imagerange = nothing)
    isnothing(rotationrange) && (rotationrange = axes(alloc.kernelResults, 2))
    isnothing(imagerange) && (imagerange = 1:length(alloc.indexImages))
    applyKernel!(alloc.kernelResults, alloc.allKernelResults, alloc.fvaluesDuplicated, alloc.nphotonLikelihoods, alloc.indexImages.locations, alloc.indexImages.indices, alloc.npercircle, rotationrange, alloc.polarizationFactorsDuplicated, alloc.bvaluesDuplicated, imagerange)
end

function compute_imagelikelihoods!(alloc::ComputeAllocation, model::ForwardModel; results_slot = 1, η = 1, recompute_meanfvalues = true, usemaxnphotonLikelihoods = true, afterkernel = ((args...; kwargs...) -> nothing))
    @unpack_Allocation(alloc)
    nimages = length(indexImages)
    npercircle = configuration.params.npercircle
    hasbackground = !isnothing(bvalues)

    minsize!(alloc, results_slot)

    @tc "setup" begin
        duplicateChunks!(fvaluesDuplicated, fvalues, npercircle)
        hasbackground && duplicateChunks!(@view(bvaluesDuplicated[:, 1]), bvalues, npercircle)
        hasbackground && (bvaluesDuplicated .= @view bvaluesDuplicated[:, 1:1])

        compute_nphotonsPerRotation!(alloc)
        recompute_meanfvalues && compute_radialAverages!(alloc, model)
        fvaluesDuplicated ./= @view(meanfvaluesDuplicated[2:end]); 
        hasbackground && (bvaluesDuplicated ./= @view(meanfvaluesDuplicated[2:end]))

        rnPR = reshape(nphotonsPerRotation, 1, size(nphotonsPerRotation)...)

        if model.intensitydist isa Tuple{Real}
            nphotonLikelihoods .= .- (model.meanintensity * mean(model.intensitydist)) .* rnPR
        elseif model.intensitydist isa Gamma
            α = model.intensitydist.α
            β = 1 / model.intensitydist.θ / model.meanintensity
            nphotonLikelihoods .= .- log.(rnPR .+ β) .* α
            fvaluesDuplicated ./= rnPR .+ β
            hasbackground && (bvaluesDuplicated ./= rnPR .+ β)
        elseif isnothing(model.intensitydist)
            fvaluesDuplicated ./= rnPR
            hasbackground && (bvaluesDuplicated ./= rnPR)
            nphotonLikelihoods .= 0
        end

        if usemaxnphotonLikelihoods
            maxnphotonLikelihoods .= @view nphotonLikelihoods[:, 1, 1]
        else
            maxnphotonLikelihoods .= 0
        end
        nphotonLikelihoods .= exp.(nphotonLikelihoods .- maxnphotonLikelihoods)
        
    end

    likelihoods = view(likelihoods, (1:nimages) .+ (results_slot - 1) * nimages)
    loglikelihoods = view(loglikelihoods, (1:nimages) .+ (results_slot - 1) * nimages)

    likelihoods .= 0

    npercall = size(kernelResults, 1)
    for imagerange in partitionrange(1:nimages, stride = npercall)
        kernelResults .= 0
        !isnothing(allKernelResults) && (allKernelResults .= 0)
        @tc "kernel_logp" applyKernel!(alloc, imagerange = imagerange)
        @tc "collect" begin 
            mul!(view(likelihoods, imagerange), view(kernelResults, 1:length(imagerange), :), rotationWeights)
            # kernelResults .*= reshape(rotationWeights, 1, :)
            # sum!(view(likelihoods, imagerange), view(kernelResults, 1:length(imagerange), :))
        end
        afterkernel(alloc, imagerange)
    end

    loglikelihoods .= log.(likelihoods) .- (configuration.inner ? log(npercircle) : 0.0)

    broadcast!(loglikelihoods, loglikelihoods, Ref(meanfvaluesDuplicated), Ref(indexImages.indices), indexImages.locations) do r, meanfvaluesDuplicated, indices, loc
        x = zero(r)
        for k in 1:indices[loc]
            x += log(meanfvaluesDuplicated[indices[loc + k] + 1])
        end
        r + x
    end

    if usemaxnphotonLikelihoods
        loglikelihoods .+= getindex.(Ref(maxnphotonLikelihoods), indexImages.lengths .+ 1)
    end
    
    loglikelihoods
end

function logsumexp(alloc::ComputeAllocation, weights::AbstractArray)
    minsize!(alloc, 2length(weights) + 1)

    nimg = length(alloc.indexImages); lens = length(weights)
    loglikelihoods = reshape(@view(alloc.loglikelihoods[1 : nimg * lens]), nimg, lens)

    A = reshape(@view(alloc.loglikelihoods[nimg * lens + 1 : 2nimg * lens]), nimg, lens)
    B = @view(alloc.loglikelihoods[2nimg * lens + 1 : 2nimg * lens + nimg])
    M = reshape(alloc.scratch1, :); 
    W = typeof(loglikelihoods)(normalize(weights, 1)')

    logsumexp!(M, A, B, W, loglikelihoods)
    
    W isa CuArray && CUDA.unsafe_free!(W)

    sum(M, init = 0.0)
end

function loglikelihoods(alloc::ComputeAllocation, f::Structure, model::ForwardModel{<:Any, <:Any, <:Any}; recompute = true)
    @tc "intensities" compute_intensities!(alloc, f, model)
    @tc "likelihoods" Array(view(compute_imagelikelihoods!(alloc, model), 1:length(alloc.indeximages)))
    logps
end

function loglikelihoods(alloc::ComputeAllocation, f::Structure; kwargs...)
    loglikelihoods(alloc, f, alloc.forwardmodel; kwargs...)
end

function (alloc::ComputeAllocation)(f::Structure, model::ForwardModel{<:Any, <:Any, <:Any}; radreg = nothing, recompute = true, kwargs...)
    @tc "intensities" compute_intensities!(alloc, f, model)
    @tc "likelihoods" logp = sum(compute_imagelikelihoods!(alloc, model; kwargs...), init = 0.0)
    if !isnothing(radreg)
        @tc "radreg" logp -= radreg * radial_loglikelihood(alloc)
    end
    logp
end


function radial_loglikelihood(alloc::ComputeAllocation)
    Y = @view alloc.meanfvaluesDuplicated[2:end]
    
    xs = broadcast(alloc.indexImages.locations, Ref(alloc.indexImages.indices), Ref(Y)) do l, inds, Y
        x = 0.0f0
        for i in 1:inds[l]
            x += log(Y[inds[l+i]])
        end
        Float64(x)
    end
    λ = alloc.forwardmodel.meanintensity * mean(alloc.forwardmodel.intensitydist)
    sum(xs) - λ * length(alloc.indexImages) * dot(alloc.meanfvalues, alloc.pixelWeights)
end

function gethitquadrature(model, nηs)
    if model.hitdist isa Beta
        gaussJacobiQuadrature(model.hitdist, nηs)
    else
        ηs = range(extrema(model.hitdist)..., length = nηs)
        cdfs = [0; cdf.(model.hitdist, middle.(ηs[2:end], ηs[1:end-1])); 1]
        ws = cdfs[2:end] .- cdfs[1:end-1]
        ηs, ws
    end
end

function (alloc::ComputeAllocation)(f::Structure, model::ForwardModel{<:Any, <:Any, <:Distribution}; recompute = true, nhv = nothing, kwargs...)
    minsize!(alloc, 2alloc.configuration.nhitvalues + 1)
    
    nηs = (!isnothing(nhv) ? nhv : alloc.configuration.nhitvalues)
    ηs, ws = gethitquadrature(model, nηs)

    if recompute
        cf = iscuda(alloc) ? cuda(f, eltype(alloc.fvalues)) : f
        cm = iscuda(alloc) ? cuda(model, eltype(alloc.fvalues)) : model

        # rs = cu(norm.(alloc.configuration.params.pixels[1:alloc.configuration.params.npercircle:end]))
        # reshape(alloc.meanfvalues, alloc.configuration.params.npercircle) .= radialAverage(f, rs)

        @tc "radial" begin
            compute_intensities!(alloc, cf, cm, 0.5)
            compute_radialAverages!(alloc, model)
        end

        for (i, η) in enumerate(ηs)
            @tc "intensities" compute_intensities!(alloc, cf, cm, η)
            @tc "likelihoods" compute_imagelikelihoods!(alloc, model; results_slot = i, η = η, recompute_meanfvalues = false, kwargs...)
        end
        unsafe_free!(cf)
        hasnoise(cm) && unsafe_free!(cm)
    end

    logsumexp(alloc, ws)
end

function (alloc::ComputeAllocation)(ensemble::WeightedEnsemble, model::ForwardModel; recompute = true, kwargs...)
    minsize!(alloc, 2length(ensemble) + 1)

    if recompute
        compute_background!(alloc, model)
        for (i, f) in enumerate(ensemble.structs)
            compute_foreground!(alloc, f)
            compute_imagelikelihoods!(alloc, model; results_slot = i, kwargs...)
        end
    end

    logsumexp(alloc, ensemble.weights)
end

function (alloc::ComputeAllocation)(f; kwargs...)
    alloc(f, alloc.forwardmodel; kwargs...)
end