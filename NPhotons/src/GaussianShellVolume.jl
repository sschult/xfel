import Base.length, Base.getindex

struct GaussianShellVolume{A <: AbstractVector{<:AbstractArray},B <: AbstractVector}
    positions::A
    heights::B
    rwidths::B
    awidths::B
end

function GaussianShellVolume(; qmax, d)
    nsub = 0
    verts = getIcosphereVerts(nsub, true)

    positions = [SA[0.0,0.0,0.0]]
    awidths = [1.0]
    for k in d:d:qmax
        while k * minimum(norm(verts[i] - verts[1]) for i in 2:length(verts)) > 1.0d
            nsub += 1
            verts = getIcosphereVerts(nsub, true)
        end
        append!(positions, k .* verts)
        aw = 0.7 * minimum(angle(verts[i], verts[1]) for i in 2:length(verts))
        append!(awidths, fill(aw, length(verts)))
    end

    heights = ones(length(positions))
    rwidths = fill(d, length(positions))
    GaussianShellVolume(positions, heights, rwidths, awidths)
end

function (f::GaussianShellVolume)(k)
    result = zero(eltype(f.heights))
    for (p, h, rw, aw) in zip(f.positions, f.heights, f.rwidths, f.awidths)
        result += h * smoothbump((norm(k) - norm(p)) / rw) * exp(-1 / (2 * aw^2) * min(angle(-k, p), angle(k, p))^2)
    end
    result
end

function CUDA.Adapt.adapt_structure(to::CUDA.CuArrayKernelAdaptor, f::GaussianShellVolume)
    positions = CUDA.Adapt.adapt_structure(to, SVector{3, Float32}.(f.positions))
    heights = CUDA.Adapt.adapt_structure(to, f.heights)
    rwidths = CUDA.Adapt.adapt_structure(to, f.rwidths)
    awidths = CUDA.Adapt.adapt_structure(to, f.awidths)
    GaussianShellVolume(positions, heights, rwidths, awidths)
end

function CUDA.Adapt.adapt_structure(to, f::GaussianShellVolume)
    positions = CUDA.Adapt.adapt_structure(to, f.positions)
    heights = CUDA.Adapt.adapt_structure(to, f.heights)
    rwidths = CUDA.Adapt.adapt_structure(to, f.rwidths)
    awidths = CUDA.Adapt.adapt_structure(to, f.awidths)
    GaussianShellVolume(positions, heights, rwidths, awidths)
end

evaluate(f::GaussianShellVolume, P) = f.(P)
evaluate!(X, f::GaussianShellVolume, P) = (X .= f.(P))
cuda(f::GaussianShellVolume) = cu(f)