using Rotations

function randomRotation(rng::AbstractRNG)
    RotMatrix{3}(UnitQuaternion(randn(rng), randn(rng), randn(rng), randn(rng)))
end

function randomRotation()
    rand(RotMatrix3{Float64})
end

function randomReflection()
    R = randomRotation()
    R^-1 * Diagonal(SA[-1,1,1]) * R
end

function randomSmallRotation(σ)
    # α = randn(rng) * σ
    # α = sign(α) * abs(α)^(1/3)
    # S = randomRotation(rng)
    # R = SA[cos(α) -sin(α) 0; sin(α) cos(α) 0; 0 0 1]
    # return RotMatrix3(S^-1 * R * S)
    if σ > 1.5
        return rand(RotMatrix3{Float64})
    else
        x = randn(SVector{3, Float64})
        return RotMatrix3{Float64}(AngleAxis(norm(x) * σ  , normalize(x)...))
    end
end

function rotationsFromPoles(poles, ncirc; sym = false)
    rotations = QuatRotation{Float64}[]
    e = [0.0, 0.0, 1.0]
    for b in poles
        R = rotation_between(b, e)
        # Re1 = R * [1.0, 0.0, 0.0]
        # Re1 = Re1 .* [1.0, 1.0, 0.0]
        # if norm(Re1) > 0
        #     R = rotation_between(Re1, [1.0, 0.0, 0.0]) * R
        # end
        for a in prange(0, (2 - sym) * pi, ncirc)# .+ 0.00001
            push!(rotations, R * AngleAxis(a, 0, 0, 1))
        end
    end
    rotations
end

function rotationFromPole(pole, angle)
    e = [0.0, 0.0, 1.0]
    R = rotation_between(pole, e)
    Re1 = R * [1.0, 0.0, 0.0]
    Re1 = Re1 .* [1.0, 1.0, 0.0]
    if norm(Re1) > 0
        R = rotation_between(Re1, [1.0, 0.0, 0.0]) * R
    end
    AngleAxis(angle, 0, 0, 1) * R
end


function lebedevGrid(;precision = 41)
    lebedev = readdlm(project_path("data/lebedev/lebedev_$(lpad(precision,3,"0")).txt"))
    sph2xyz.(lebedev[:,1] ./ 180 .* pi, lebedev[:,2] ./ 180 .* pi), lebedev[:,3]
end

function uniformRotationGrid(nsub, ncirc; sym = false)
    rotationsFromPoles(getIcosphereVerts(nsub), ncirc; sym = false)
end

function lebedevPrecisions()
    parse.(Int, getindex.(readdir(project_path("data/lebedev/"))[2:end], Ref(9:11)))
end

function rotationGrid(; precision, class = :lebedev)
    if class == :lebedev
        lebedev = readdlm(project_path("data/lebedev/lebedev_$(lpad(precision,3,"0")).txt"))
        xyz = sph2xyz.(lebedev[:,1] ./ 180 .* pi, lebedev[:,2] ./ 180 .* pi)
        rotations = rotation_between.(xyz, Ref([0.0, 0.0, 1.0]))
        rotationWeights = lebedev[:,3]
        return rotations, rotationWeights
    elseif class == :design
        files = readdir(project_path("data/designs"))
        file = files[findfirst(x -> occursin(string(precision), x), files)]
        xyz = Array.(eachcol(reshape(readdlm(project_path("data/designs/$(file)")), 3, :)))
        rotations = rotation_between.(xyz, Ref([0.0, 0.0, 1.0]))
        rotationWeights = fill(1/length(rotations), length(rotations))
        return rotations, rotationWeights
    end
    throw(ArgumentError("invalid class"))
end

function uniformRotationGrid(; precision, npercircle, class = :lebedev)
    if class == :lebedev
        lebedev = readdlm(project_path("data/lebedev/lebedev_$(lpad(precision,3,"0")).txt"))
        xyz = sph2xyz.(lebedev[:,1] ./ 180 .* pi, lebedev[:,2] ./ 180 .* pi)
        rotations = rotationsFromPoles(xyz, npercircle, sym = false)
        rotationWeights = repeat(lebedev[:,3] ./ npercircle, inner = (npercircle, 1))[:]
        return rotations, rotationWeights
    elseif class == :design
        files = readdir(project_path("data/designs"))
        file = files[findfirst(x -> occursin(string(precision), x), files)]
        xyz = Array.(eachcol(reshape(readdlm(project_path("data/designs/$(file)")), 3, :)))
        rotations = rotation_between.(xyz, Ref([0.0, 0.0, 1.0]))
        rotationWeights = fill(1/length(rotations), length(rotations))
        rotations, rotationWeights
    end


    throw(ArgumentError("invalid class"))
end




function align(f, g; res::Int = 10, ntries::Int = 1000, radius = 1, showprogress = false)
    vs = gridinUnitBall(res) .* radius
    fa = f.(vs)
    
    R = Array(randomRotation(3))
    prog = Progress(2ntries, enabled = showprogress)
    R, ga = alignGlobal!(R, fa, g, vs, ntries, prog)
    R, ga = alignLocal!(R, fa, g, vs, ntries, 0.5, prog)
    R
end

function alignGlobal!(R, fa, g, vs, M, prog)
    ga = g.(Ref(R) .* vs)
    d = abs(dot(fa, ga))
    
    nga = copy(ga)
    for i in 1:M
        S = Array(randomRotation(3))
        if rand() > 0.5
            S .*= -1
        end
        nga .= g.(Ref(S) .* vs)
        newd = abs(dot(fa, nga))
        if newd > d
            ga .= nga
            d = newd
            R .= S
        end
        next!(prog)
    end
    R, ga
end

function alignLocal!(R, fa, g, vs, M, σ, prog)
    ga = g.(Ref(R) .* vs)
    d = abs(dot(fa, ga))
    
    nga = copy(ga)
    for i in 1:M
        S = Array(randomSmallRotation(σ))
        SR = S * R
        nga .= g.(Ref(SR) .* vs)
        newd = abs(dot(fa, nga))
        if newd > d
            ga .= nga
            d = newd
            R .= SR
        end
        next!(prog)
    end
    R, ga
end

function align(fs; res::Int = 10, N::Int = 100, M::Int = 1000, radius = 1)
    vs = gridinUnitBall(res) .* radius
    rots = [Array(randomRotation(3)) for i in 1:length(fs)]
    fas = [fs[i].(Ref(rots[i]) .* vs) for i in 1:length(fs)]

    prog = Progress(N * length(fs), "Global: ")
    for n in 1:N
        for i in shuffle(1:length(fs))
            sfa = sum([fas[k] for k in 1:length(fas) if k != i])
            rots[i], fas[i] = alignGlobal!(rots[i], sfa, fs[i], vs, M)
            next!(prog)
        end
    end

    prog = Progress(N * length(fs), "Local:  ")
    for n in 1:N
        for i in shuffle(1:length(fs))
            sfa = sum([fas[k] for k in 1:length(fas) if k != i])
            rots[i], fas[i] = alignLocal!(rots[i], sfa, fs[i], vs, M, 0.5)
            next!(prog)
        end
    end

    rots
end

function kabsch(ps::Vector{SVector{3, T}}, qs::Vector{SVector{3, T}}) where T
    P = reshape(reinterpret(T, ps), 3, :)
    Q = reshape(reinterpret(T, qs), 3, :)
    kabsch(P, Q)
end

kabsch(P, Q) = kabsch(P * Q')
kabsch(f::AtomVolume, g::AtomVolume) = kabsch(f.positions, g.positions)

function kabsch(H)
    U, S, V = svd(H)
    U * Diagonal(SA[1, 1, det(V) * det(U)]) * V'
end

function icp(ps, qs; iterations = 10)
    P = reduce(hcat, ps); Q = reduce(hcat, qs)
    R = SMatrix{3, 3, eltype(eltype(ps))}(I)
    tree = NearestNeighbors.KDTree(Q)
    for i in 1:iterations
        NQ = Q[:, k11(tree, R * P)[1]]
        R = kabsch(R * P, NQ) * R
    end
    R
end


function align_pointclouds_cpu(fpos, gpos, fh, fw; ntries = 1000, mirror = 0.5, full = true, σ = 0.5, showprogress = false, initR = I)
    dists = zeros(length(fpos), length(gpos))
    dX = zeros(length(fpos))
    dY = zeros(length(gpos))

    R = SMatrix{3, 3, Float64, 9}(initR)
    Rfpos = Ref(R) .* fpos
    dR = pointCloudDistance!(dists, dX, dY, (Rfpos .= Ref(R) .* fpos), gpos, fh, gh)

    prog = Progress((1 + full) * ntries)
    
    if full
        for i in 1:ntries
            S = SMatrix{3, 3, Float64, 9}(randomRotation())
            if rand() < mirror
                S *= -1
            end
            dS = pointCloudDistance!(dists, dX, dY, (Rfpos .= Ref(S) .* fpos), gpos, fh, gh)
            if dS < dR
                R = S
                dR = dS
            end
            showprogress && next!(prog)
        end
    end
    
    for i in 1:ntries
        S = randomSmallRotation(σ)
        dS = pointCloudDistance!(dists, dX, dY, (Rfpos .= Ref(R * S) .* fpos), gpos, fh, gh)
        if dS < dR
            R = R * S
            dR = dS
        end
        showprogress && next!(prog)
    end
    
    R
end


function pcds!(D, ys, zs)
    broadcast!(D, ys, Ref(zs)) do y, zs
        x = Inf32
        for z in zs
            v = y .- z
            x = min(x, v ⋅ v)
        end
        sqrt(x)
    end
end

function align_pointclouds_gpu(fposcpu, gposcpu; ntries = 500, σfactor = 0.5, nlevels = 10, initR = I)
    σ = 2pi
    R = SMatrix{3, 3, Float64, 9}(initR)

    fpos = cu(SVector{3, Float32}.(fposcpu))
    gpos = cu(SVector{3, Float32}.(gposcpu))

    D1 = CuArray{Float32}(undef, length(fpos), ntries)
    D2 = CuArray{Float32}(undef, length(gpos), ntries)

    rotfpos = CuArray{SVector{3, Float32}}(undef, length(fpos), ntries)
    rotgpos = CuArray{SVector{3, Float32}}(undef, length(gpos), ntries)

    Rs = CuArray{SMatrix{3, 3, Float32, 9}}(undef, ntries)

    for level in 1:nlevels
        rotations = [randomSmallRotation(σ) * R for _ in 1:ntries]
        rotations .*= randomReflection.().^rand.(Bool)
        rotations[1] = R
        copyto!(Rs, SMatrix{3, 3, Float32}.(rotations))

        rotfpos .= reshape(Rs, 1, :) .* fpos
        rotgpos .= inv.(reshape(Rs, 1, :)) .* gpos

        @tc "D1" pcds!(D1, rotfpos, gpos)
        @tc "D2" pcds!(D2, rotgpos, fpos)
        
        σ *= σfactor

        @tc "argmin" R = rotations[argmin(reshape(mean(D1, dims = 1) .+ mean(D2, dims = 1), :))]
    end
    R
end