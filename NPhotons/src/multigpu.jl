abstract type MultiAllocation end

function Base.iterate(m::MultiAllocation, state = 0)
    i = state + 1
    if i <= length(m.allocs)
        (m.allocs[i], i)
    else
        nothing
    end
end

function Base.getindex(m::MultiAllocation, i)
    getindex(m.allocs, i)
end

function setqmax!(m::MultiAllocation, args...; kwargs...)
    for (d, a) in m
        device!(d)
        setqmax!(a, args...; kwargs...)
    end
end

function randomizeImages!(m::MultiAllocation, args...; kwargs...)
    for (d, a) in m
        device!(d)
        randomizeImages!(a, args...; kwargs...)
    end
end

function smoothen!(m::MultiAllocation, args...; kwargs...)
    for (d, a) in m
        device!(d)
        smoothen!(a, args...; kwargs...)
    end
end

function filterImages!(m::MultiAllocation, args...; kwargs...)
    for (d, a) in m
        device!(d)
        filterImages!(a, args...; kwargs...)
    end
end

function Base.length(m::MultiAllocation)
    Base.length(m.allocs)
end


function devicescore(device)
    CUDA.attribute(device, CUDA.DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT) * CUDA.attribute(device, CUDA.DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK)
end



struct MultiComputeAllocation{T<:AbstractVector} <: MultiAllocation
    allocs::T
end

function MultiComputeAllocation(model, conf, args...; devices = devices(), kwargs...)
    confs = distribute(conf, devicescore.(devices))
    allocs = map(confs, devices) do c, d
        device!(d) => ComputeAllocation(CuArray, Float32, model, c, args...; kwargs...)
    end
    MultiComputeAllocation(allocs)
end

function (m::MultiComputeAllocation)(f, args...; kwargs...)
    logps = Vector{Float64}(undef, length(m))
    @sync for (i, (d, logp)) in enumerate(m)
        @async begin
            device!(d)
            logps[i] = logp(f, args...; kwargs...)
            nothing
        end
    end
    sum(logps)
end


struct MultiGradientAllocation{T<:AbstractVector} <: MultiAllocation
    allocs::T
end

function MultiGradientAllocation(m::MultiComputeAllocation, args...; kwargs...)
    allocs = map(m.allocs) do (d, a)
        device!(d) => GradientAllocation(a; kwargs...)
    end
    MultiGradientAllocation(allocs)
end

function (m::MultiGradientAllocation)(f; kwargs...)
    logps = Vector{Float64}(undef, length(m))
    grads = Vector{DifferentialAtomVolume{typeof.((f.positions, f.heights, f.widths))...}}(undef, length(m))
    @sync for (i, (d, grad)) in enumerate(m)
        @async begin
            device!(d)
            p, g = grad(f; kwargs...)
            logps[i] = p
            grads[i] = g
            nothing
        end
    end
    sum(logps), sum(grads)
end



function assignJobs(N, weights; chunksize = 1)
    assignments = [Int[] for _ in weights]
    for i in 1:chunksize:N
        a = argmin(length.(assignments) ./ weights)
        for j in i:i+chunksize-1
            push!(assignments[a], j)
        end
    end
    assignments
end

function distribute(conf::ComputeConfiguration, workerweights::AbstractArray)
    batchsizes = round.(Int, conf.batchsize .* normalize(workerweights, 1))
    batchsizes[1] += conf.batchsize - sum(batchsizes)
    assignments = assignJobs(length(conf.indexImages), workerweights)
    confs = [@set conf.indexImages = conf.indexImages[a] for a in assignments]
    confs = [@set c.batchsize = b for (c, b) in zip(confs, batchsizes)]
    confs
end
distribute(conf::ComputeConfiguration, n::Integer) = distribute(conf, ones(n))




# function workertask(d, f, inchannel, outchannel)
#     device!(d)
#     while true
#         put!(outchannel, f(take!(inchannel)))
#     end
# end

# function spawntasks(m)
#     inchannels = [Channel{typeof(f)}(Inf) for _ in 1:length(m)]
#     outchannels = [Channel{typeof(sgrad(f))}(Inf) for _ in 1:length(m)]
    
#     tasks = map(1:length(m)) do i
#         @async workertask(m[i]..., inchannels[i], outchannels[i])
#     end
    
#     function c(f)
#         put!.(inchannels, f)
#         sum(last, take!.(outchannels))
#     end
# end