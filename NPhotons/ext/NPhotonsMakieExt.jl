module NPhotonsMakieExt

using NPhotons, Makie, LinearAlgebra, Interpolations, StatsBase, FileWatching, JLD2
# const nph = NPhotons
import Makie.SpecApi as S

Makie.used_attributes(::Type{<:Plot}, x::NPhotons.AtomVolume) = (:ngrid, :r, :itp, :normalization, :scale, :cutoff)

function Makie.convert_arguments(P::Type{<:Volume}, xs, ys, zs, f::NPhotons.AtomVolume; itp = 0, normalization = :maximum, scale = identity, cutoff = Inf)
    fs = [f(x, y, z, :real, cutoff = cutoff) for x in xs, y in ys, z in zs]
    if normalization == :maximum
        fs ./= maximum(fs)
    elseif normalization == :mean
        fs ./= sum(fs) / length(fs)
    elseif normalization == :gaussian
        fs ./= minimum(f.heights ./ f.widths.^3) / (2pi)^(3/2)
    elseif normalization == :meangaussian
        fs ./= mean(f.heights ./ f.widths.^3) / (2pi)^(3/2)
    end
    fs .= scale.(fs)

    if itp > 0
        fitp = cubic_spline_interpolation((xs, ys, zs), fs)
        xs = range(extrema(xs)..., length = length(xs) * itp)
        ys = range(extrema(ys)..., length = length(ys) * itp)
        zs = range(extrema(zs)..., length = length(zs) * itp)
        fs = [fitp(x, y, z) for x in xs, y in xs, z in xs]
    end
    (Makie.to_endpoints.(extrema.((xs, ys, zs)))..., Float32.(fs))
end

function Makie.convert_arguments(P::Type{<:Volume}, f::NPhotons.AtomVolume; ngrid = 50, r = nothing, itp = 0, normalization = :maximum, scale = identity, cutoff = nothing)
    if isnothing(cutoff)
        cutoff = 4maximum(f.widths)
    end
    r = isnothing(r) ? (maximum(x -> norm(x, Inf), f.positions) + 3maximum(f.widths)) : r

    xs, fs = NPhotons.evaluateRealCUDA(f, r; ngrid, cutoff)
    # xs = range(-r, r, length = ngrid)
    # fs = [f(x, y, z, :real, cutoff = cutoff) for x in xs, y in xs, z in xs]

    if normalization == :maximum
        fs ./= maximum(fs)
    elseif normalization == :mean
        fs ./= sum(fs) / length(fs)
    elseif normalization == :gaussian
        fs ./= minimum(f.heights ./ f.widths.^3) / (2pi)^(3/2)
    elseif normalization == :meangaussian
        fs ./= mean(f.heights ./ f.widths.^3) / (2pi)^(3/2)
    end
    
    if itp > 0
        fitp = cubic_spline_interpolation((xs, xs, xs), fs)
        xs = range(-r, r, length = itp * ngrid)
        fs = [fitp(x, y, z) for x in xs, y in xs, z in xs]
    end
    fs .= scale.(fs)

    (Makie.to_endpoints.(extrema.((xs, xs, xs)))..., Float32.(fs))
end

# function Makie.convert_arguments(P::Type{<:Volume}, f::NPhotons.AtomVolume; kwargs...)
#     Makie.convert_arguments(Volume, f; kwargs...)
# end

# function Makie.plot!(plot::Volume{<:Tuple{<:NPhotons.AtomVolume}})
#     println(plot)
#     valid_attributes = Makie.shared_attributes(plot, Volume)
#     volume!(plot, valid_attributes, plot[1])
# end

function Makie.convert_arguments(P::Type{<:Scatter}, f::NPhotons.AtomVolume)
    Makie.convert_arguments(P, f.positions)
end

cam3dfixed!(scene; kwargs...) = cam3d!(scene; zoom_shift_lookat = false, kwargs...)

function liftevery(innode, dt)
    t = Observable(time_ns())
    outnode = Observable(innode[])
    on(innode) do val
        if time_ns() - t[] > dt * 1e9
            t[] = time_ns()
            outnode[] = val
        end
    end
    outnode
end

function linkCams!(scenes)
    for scene in scenes
        scene.camera = copy(scenes[1].camera)
        on(scene.camera_controls.eyeposition) do ep
            for scene2 in scenes
                scene2.camera_controls.eyeposition.val = ep
            end
        end
        on(scene.camera_controls.lookat) do ep
            for scene2 in scenes
                scene2.camera_controls.lookat.val = ep
            end
        end
        on(scene.camera_controls.upvector) do ep
            for scene2 in scenes
                scene2.camera_controls.upvector.val = ep
            end
        end
        on(scene.camera_controls.fov) do ep
            for scene2 in scenes
                scene2.camera_controls.fov.val = ep
            end
        end
    end
end

function NPhotons.monitor(chain::Observable{<:AbstractArray}, object = nothing; dt = 0, aligndt = 5, volumedt = 1, showobject = true, centersize = 0, nhist = 0, yscale = :identity, includezero = true, partialylimits = false, cutoff = Inf, size = (1200, 1200), ntries = 500, showinfo = [:energy, :resolution, :fsc, :kfsc], kwargs...)
    # showinfo = true, showwidths = false, showres = false, showfsc = false, kwargs...)
    fig = Figure(; size = size, figure_padding = 25, kwargs...)

    vtick = liftevery(events(fig).tick, aligndt)

    stepslider = Slider(fig[4, :][2,1], range = range(0, 1, length = 10000))
    set_close_to!(stepslider, 1)

    dtchain = lift((c, v) -> c[1:max(1, round(Int, end * v))], liftevery(chain, dt), liftevery(stepslider.value, 0.03))


    if isnothing(object)
        showobject = false
        object = NPhotons.AtomVolume(1, 10)
    end

    fs = lift(liftevery(dtchain, volumedt)) do c
        x = NPhotons.getstructures(c[end].conf)
        for f in x
            f.positions .-= (mean(f.positions, Weights(f.heights)),)
        end
        x
    end

    preRs = Observable(NPhotons.align.(fs[], (object,), ntries = ntries))
    lk = ReentrantLock()
    on(liftevery(fs, aligndt)) do fs
        if !islocked(lk)
            Threads.@spawn lock(lk) do 
                preRs[] = [NPhotons.align(f, object, ntries = ntries, initR = R) for (f, R) in zip(fs, preRs[])]
            end
        end
    end
    Rs = lift(_ -> preRs[], fs)

    # on(liftevery(fs, aligndt)) do fs
    #     Rs[] = [NPhotons.align(f, object, ntries = ntries, initR = R) for (f, R) in zip(fs, Rs[])]
    # end

    # Rs = lift(f -> NPhotons.align.(f, object, ntries = ntries), liftevery(fs, aligndt))
    os = NPhotons.getstructures(object)
    

    volgrid = GridLayout(fig[1,1])
    rowsize!(fig.layout, 1, Relative(0.5))
    isorange = Observable(2.0)
    σreg = Observable(0.0)

    if showobject
        for (i, o) in enumerate(os)
            f = lift(c -> NPhotons.smoothen(o, c[end].params.σ), dtchain)
            ls = LScene(volgrid[1,i]; show_axis = false);
            cam = cam3dfixed!(ls, clipping_mode = :bbox_relative, far = 2)
            volume!(ls, (@lift NPhotons.smoothen($f, $σreg)), colormap = :ice, normalization = :meangaussian, scale = log10, isorange = isorange, cutoff = cutoff)
            if centersize != 0
                ds = lift((f, e, t) -> dot.(f.positions, Ref(e)), f, cam.eyeposition, events(fig).tick)
                scatter!(ls, @lift($f.positions), overdraw = true, markerspace = :data, markersize = markersize = lift(x -> 2centersize .* sqrt.(x.heights) ./ mean(sqrt, x.heights), f), color = ds, colorrange = @lift(extrema($ds) .+ (0.2 * -(extrema($ds)...), 0)), colormap = cgrad([:transparent, :black]))
            end
        end
    end

    pre_alfs = Observable([Rs[][i] * NPhotons.smoothen(fs[][i], σreg[]) for i in 1:length(fs[])])
    pre_converted_alfs = Observable([convert_arguments(Volume, f, normalization = :meangaussian, scale = log10, cutoff = cutoff) for f in pre_alfs[]])
    vlk = ReentrantLock()
    on(fs) do fs
        if !islocked(vlk)
            Threads.@spawn lock(vlk) do 
                nalfs = [Rs[][i] * NPhotons.smoothen(fs[i], σreg[]) for i in 1:length(fs)]
                pre_converted_alfs[] = [convert_arguments(Volume, f, normalization = :meangaussian, scale = log10, cutoff = cutoff) for f in nalfs]
                pre_alfs[] = nalfs
            end
        end
    end
    converted_alfs = lift(_ -> pre_converted_alfs[], vtick)
    alfs = lift(_ -> pre_alfs[], vtick)

    for i in 1:length(fs[])
        ls = LScene(volgrid[1 + (length(os) > 1), i + showobject * (length(os) <= 1)]; show_axis = false);
        cam = cam3dfixed!(ls)

        volume!(ls, [@lift($converted_alfs[i][j]) for j in 1:4]..., colormap = :viridis, isorange = isorange)
        if centersize != 0
            f = @lift $alfs[i]
            ds = lift((f, e, t) -> dot.(f.positions, Ref(e)), f, cam.eyeposition, events(fig).tick)
            scatter!(ls, @lift($f.positions), overdraw = true, markerspace = :data, markersize = markersize = lift(x -> 2centersize .* sqrt.(x.heights) ./ mean(sqrt, x.heights), f), color = ds, colorrange = @lift(extrema($ds) .+ (0.2 * -(extrema($ds)...), 0)), colormap = cgrad([:transparent, :black]))

            if nhist > 0
                history = lift(Rs) do Rs
                    points = NPhotons.getpositions.((Rs[1],) .* NPhotons.getconf.(dtchain[][max(1, end-nhist):end]))
                    points[end] .*= NaN
                    permutedims(reduce(hcat, points), (2,1))[:]
                end
                lines!(ls, history, linewidth = 1, color = (:black, 0.1), overdraw = true)
            end
        end
    end 


    linkCams!([ls.scene for ls in contents(volgrid[:,:])])

    σslider = Slider(volgrid[:, end+1], horizontal = false, range = LinRange(0, 5.0, 100))
    on(σslider.value) do v σreg[] = v end
    set_close_to!(σslider, -1)


    infogrid1 = GridLayout(fig[2,1])
    infogrid2 = GridLayout(fig[3,1])
    infogrids = [infogrid1, infogrid2]

    ninfo = length(showinfo)
    infoi = round.(Int, (1:ninfo) ./ (ninfo + 1)) .+ 1
    infoj = mod1.(1:ninfo, ceil(Int, ninfo / 2))
    infopos = Dict(info => infogrids[i][1, j] for (info, i, j) in zip(showinfo, infoi, infoj))

    if :energy in showinfo
        ekwargs = if yscale == :pseudolog
            yticks = [-10 .^ (0:10); 0; 10 .^ (0:10)]
            yticklabels = [[rich("-10", superscript(string(i))) for i in 0:10]; rich("0"); 
                        [rich("10", superscript(string(i))) for i in 0:10]]
            (yscale = ReversibleScale(x -> asinh(2*sqrt(6)*x), x -> sinh(x)/(2*sqrt(6))),
                yticks = (yticks, yticklabels))
        else
            (;)
        end
        ax = Axis(infopos[:energy]; subtitle = "-log(p)", ekwargs...)
        includezero && hlines!(ax, [0, 1], color = :transparent)
        energies = lift(dtchain) do c
            es = (s -> s.energy).(c)
            if yscale == :log
                tail = es[max(1, end-100):end]
                offset = max(1, std(tail, eweights(length(tail), 0.1)))
                Point2.(getnsteps.(c) .- c[1].nsteps, log10.(es .- minimum(es) .+ offset))
            else
                Point2.(getnsteps.(c) .- c[1].nsteps, es)
            end
        end
        lines!(ax, energies)
        on(energies, priority = -1) do s
            if partialylimits && length(s) > 10
                xlims!(ax, nothing, nothing)
                ymin, ymax = extrema(last.(s)[end÷10:end])
                ymin = min(0, ymin); ymax = max(0, ymax); 
                ylims!(ax, ymin - 0.1 * (ymax - ymin), ymax + 0.1 * (ymax - ymin))
            else
                autolimits!(ax)
            end
        end
    end
    if :resolution in showinfo || :kfsc in showinfo
        resolutions = Observable(Tuple{Int, Float64}[])

        if :resolution in showinfo
            axres = Axis(infopos[:resolution], subtitle = "resolution", yticks = WilkinsonTicks(10; k_min = 4))
            lines!(axres, resolutions)
            on(_ -> autolimits!(axres), resolutions)
        end
        if :kfsc in showinfo
            axk = Axis(infopos[:kfsc], subtitle = "kfsc", yticks = WilkinsonTicks(10; k_min = 4))
            lines!(axk, @lift((x -> (x[1], 2pi/x[2])).($resolutions)))
            on(_ -> autolimits!(axk), resolutions)
        end
        # scatter!(axres, [1,1], [0, 1], color = :transparent)
        reslk = ReentrantLock()
        on(Rs) do Rs
            if !islocked(reslk)
                if !isempty(resolutions[]) && dtchain[][end].nsteps < resolutions[][end][1]
                    resolutions[] = resolutions[][first.(resolutions[]) .< dtchain[][end].nsteps]
                end
                Threads.@spawn lock(reslk) do
                    x = (dtchain[][end].nsteps, NPhotons.fscResolution(Rs[1] * fs[][1], os[1]))
                    push!(resolutions[], x)
                end
            end
        end
        on(_ -> notify(resolutions), vtick)
    end
    if :fsc in showinfo
        axfsc = Axis(infopos[:fsc], subtitle = "FSC", xticks = 0:1:3, xminorticksvisible = true, xminorticks = 0:0.5:3, xminorgridvisible = true)
        ylims!(axfsc, -0.05, 1.05)

        if eltype(chain[]) <: NPhotons.SGDState
            vlines!(axfsc, @lift((x = $dtchain[end].params.q; x < 3 ? x : NaN)), color = :lightgrey)
            lines!(axfsc, 0:0.01:3.0, @lift((x = $dtchain[end].params.σ; NPhotons.smoothingKernel(x).(0:0.01:3.0) .* (x > 0 ? 1.0 : NaN))), color = :lightgrey)
        end

        function getfscs(Rs)
            ks = 0:0.05:3.0
            ys = NPhotons.fsc(Rs[1] * fs[][1], os[1], ks)
            Point2.(ks, ys)
        end

        tRs = Observable(Rs[])
        tfscs = Observable(getfscs(Rs[]))
        on(Rs -> Threads.@spawn(tfscs[] = getfscs(Rs)), Rs)
        fscs = lift(_ -> tfscs[], vtick)
        lines!(axfsc, fscs)
        on(fscs) do _
            reset_limits!(axfsc)
        end
    end

    if :stepsize in showinfo
        ax2 = Axis(infopos[:stepsize], 
            yscale = log10, yticks = LogTicks(-10:1:10), 
            yminorticksvisible=true, yminorticks = IntervalsBetween(9), subtitle = "step size"
        );
        scatter!(ax2, [1,1], [1, 10], color = :transparent)
        if eltype(chain[]) <: AnnealingState
            if dtchain[][end].proposalParams isa Real
                lines!(ax2, lift(c -> [s.proposalParams for s in c], dtchain))
            else
                for i in 1:length(dtchain[][end].proposalParams.params)
                    lines!(ax2, lift(c -> [s.proposalParams.params[i] for s in c], dtchain))
                end
            end
            on(s -> autolimits!(ax2), dtchain)
        elseif eltype(chain[]) <: NPhotons.SGDState
            lines!(ax2, lift(c -> [mean(norm, s.grad.positions) for s in c], dtchain), label = "g")
            lines!(ax2, lift(c -> (x = [mean(norm, s.force.positions) for s in c]; x .* ((x .> 1e-5) ./ (x .> 1e-5))), dtchain), label = "f")
            lines!(ax2, lift(c -> (x = [s.params.η * mean(norm, s.velocity.positions) for s in c]; x .* ((x .> 1e-5) ./ (x .> 1e-5))), dtchain), label = "v")
            axislegend(ax2, orientation = :horizontal, framevisible = false)
        end
        on(s -> autolimits!(ax2), dtchain)
    end
    if :acceptancerate in showinfo
        ax3 = Axis(infopos[:acceptancerate], subtitle = "acceptance rate")
        acceptancerates = lift(NPhotons.acceptanceRate, dtchain)
        lines!(ax3, acceptancerates)
        scatter!(ax3, [1,1], [0, 1], color = :transparent)
        on(s -> autolimits!(ax3), dtchain)
    end
    if :widths in showinfo
        ax4 = Axis(infopos[:widths], subtitle = "widths")
        widths = lift(c -> mean.(getwidths.(getconf.(c))), dtchain)
        lines!(ax4, widths)
        on(widths) do _
            autolimits!(ax4)
        end
    end
    if eltype(chain[]) <: AnnealingState
        Label(fig[4, :][1,1], lift(c -> "T = $(round(c[end].temperature, sigdigits = 5))   E = $(round(c[end].energy, sigdigits = 5))   d = $(round.(last(c[end].proposalParams), sigdigits = 5))   nsteps = $(c[end].nsteps)", dtchain), tellwidth = false)
    elseif eltype(chain[]) <: NPhotons.SGDState
        Label(fig[4, :][1,1], lift(c -> "$(map(x -> round(x, sigdigits=3), c[end].params))   t = $(round(c[end].t, digits=3))   E = $(round(c[end].energy, sigdigits = 5))   nsteps = $(c[end].nsteps)", dtchain), tellwidth = false)
    end

    fig
end

NPhotons.monitor(chain::AbstractArray, args...; kwargs...) = NPhotons.monitor(Observable(chain), args...; kwargs...)

function NPhotons.monitor(path::String, args...; kwargs...)
    hassave(ls) = any(occursin("Saved", l) for l in ls) || any(occursin("Finished", l) for l in ls)

    if splitext(path)[2] == ".out"        
        if !hassave(readlines(path))
            fig0 = Figure(size = (1200, 1200))
            t = Observable("")
            Label(fig0[1,1], text = t, justification = :left)
            display(fig0)

            d = 1;
            while !hassave(readlines(path))
                if !isopen(fig0.scene)
                    return nothing
                end
                t[] = join(readlines(path), "\n") * "\n" * "."^d
                d = mod1(d + 1, 10)
                sleep(1)
            end
        end

        inpath, outpath = NPhotons.getjobpaths(path)
        chain = loadChainAsArray(outpath, stride = 1)
        object = load(inpath, "reference")

        node = Observable(chain)
        fig = NPhotons.monitor(node, object, args...; kwargs...)
        display(fig)

        ll = length(readlines(path))

        on(liftevery(events(fig).tick, 1)) do _
            if isfile(path) && length(readlines(path)) > ll
                ls = readlines(path)
                println.(ls[ll+1:end])
                if hassave(ls[ll+1:end])
                    c = loadChain(outpath)
                    if length(c) > length(chain)
                        append!(chain, c[length(chain)+1:end])
                    else
                        chain = c[1:end]
                    end
                    node[] = chain
                end
                ll = length(ls)
            end
        end

        return fig
    else
        chain = loadChainAsArray(path, stride = 1)
        node = Observable(chain)
        fig = NPhotons.monitor(node, args...; kwargs...)
        display(fig)

        @async while isopen(fig.scene)
            try
                if watch_file(path, 0.5).changed
                    if isfile(path)
                        c = loadChain(path)
                        if length(c) > length(chain)
                            append!(chain, c[length(chain)+1:end])
                        else
                            chain = c[1:end]
                        end
                        node[] = chain
                    end
                end
            catch e
                if e isa Base.IOError
                    sleep(0.5)
                else
                    rethrow(e)
                end
            end
            ispressed(fig, Keyboard.escape) && break
        end

        return fig
    end
end

end