This repository contains a Julia package for de novo structural ensemble determination from single molecule X-ray scattering using a Bayesian approach. It is a proof-of-principle implementation of the method described at https://arxiv.org/abs/2302.09136 and https://arxiv.org/abs/2403.18391.

While this is not meant for out-of-the-box usage yet, a short demonstration is included (`demo.ipynb`). We also provide Jupyter-notebooks for further example usage; please note, however, that these may contain code specific to our compute cluster and may not be readily usable on other hardware. 

Requirements
=============

* A CUDA-capable gpu (compute capability 3.5 or higher) with support for CUDA 11.0 or higher
* Julia

Installation
=============

Install [Julia](https://julialang.org) and [IJulia](https://github.com/JuliaLang/IJulia.jl) according to the instructions. All our computations were done using Julia 1.8.1 (but newer versions should work as well).
Then clone this repository
```
    git clone https://gitlab.gwdg.de/sschult/xfel.git
```
and install the dependencies by executing
```
  ]add path/to/cloned/repository#master:NPhotons
```
in Julia (this may take a few minutes). You can also do this manually by installing the packages found in `NPhotons/Project.toml`. Additional dependencies may be required to run the notebook files. 

Demo
=============

First install some additional dependencies:
```
  ]add ProgressMeter, StaticArrays, Distributions, StatsBase, Hungarian, GLMakie, CairoMakie, Colors, CUDA
```
To reduce compilation times in the notebook, run `precompile` afterwards.
To run the demo, open `demo.ipynb` in Jupyter, and execute the cells in order. 


[Copyright (c) 2023 Steffen Schultze](https://gitlab.gwdg.de/sschult/xfel/-/blob/master/license.md)